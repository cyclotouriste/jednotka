<?php

/**
 * Markup before the widget
 */
function thematic_before_widget() {
    $content = '<li id="%1$s" class="widgetcontainer %2$s">';
    return apply_filters('thematic_before_widget', $content);
}


/**
 * Markup after the widget
 */
function thematic_after_widget() {
    $content = '</li>';
    return apply_filters('thematic_after_widget', $content);
}



/**
 * Markup before the widget title
 */
function thematic_before_title() {
    $content = "<h3 class=\"widgettitle\">";
    return apply_filters('thematic_before_title', $content);
}


/**
 * Markup after the widget title
 */
function thematic_after_title() {
    $content = "</h3>\n";
    return apply_filters('thematic_after_title', $content);
}

/**
 * Defines the array for creating and displaying the widgetized areas
 * in the WP-Admin and front-end of the site.
 *
 * @uses thematic_before_widget()
 * @uses thematic_after_widget()
 * @uses thematic_before_title()
 * @uses thematic_after_title()
 * @return array
 */
function thematic_widgets_array() {
    $thematic_widgetized_areas = array(
        'Primary Aside' => array(
            'admin_menu_order' => 100,
            'args' => array (
                'name' => __( 'Primary Aside', 'holo' ),
                'id' => 'primary-aside',
                'description' => __('The primary widget area, most often used as a sidebar.', 'thematic'),
                'before_widget' => thematic_before_widget(),
                'after_widget' => thematic_after_widget(),
                'before_title' => thematic_before_title(),
                'after_title' => thematic_after_title(),
            ),
            'action_hook'	=> 'widget_area_primary_aside',
            'function'		=> 'thematic_primary_aside',
            'priority'		=> 10,
        ),
        'Footer Area' => array(
            'admin_menu_order' => 200,
            'args' => array (
                'name' => __( 'Footer Area', 'holo' ),
                'id' => 'footer-area',
                'description' => __('Widget area in the footer.', 'holo'),
                'before_widget' => thematic_before_widget(),
                'after_widget' => thematic_after_widget(),
                'before_title' => thematic_before_title(),
                'after_title' => thematic_after_title(),
            ),
            'action_hook' => 'widget_area_footer',
            'function' => 'holo_footer_area',
            'priority' => 30,
        )
    );

    return $thematic_widgetized_areas;
}

/**
 * Registers Widget Areas(Sidebars) and pre-sets default widgets
 *
 * @uses thematic_presetwidgets
 * @todo consider deprecating the widgets directory search this seems to have never been used
 */
function thematic_widgets_init() {

    $thematic_widgetized_areas = thematic_widgets_array();
    foreach ( $thematic_widgetized_areas as $key => $value ) {
        register_sidebar( $thematic_widgetized_areas[$key]['args'] );
    }

}

add_action( 'widgets_init', 'thematic_widgets_init' );

/**
 * Add wigitized area functions to specific action hooks.
 *
 * @uses thematic_widgets_array to get function names and action hooks.
 */
function thematic_connect_functions() {

    $thematic_widgetized_areas = thematic_widgets_array();

    foreach ( $thematic_widgetized_areas as $key => $value ) {
        if ( !has_action( $thematic_widgetized_areas[$key]['action_hook'], $thematic_widgetized_areas[$key]['function'] ) )
            add_action( $thematic_widgetized_areas[$key]['action_hook'], $thematic_widgetized_areas[$key]['function'], $thematic_widgetized_areas[$key]['priority'] );
    }

}