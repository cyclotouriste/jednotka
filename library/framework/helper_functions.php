<?php

class Holo {

    private $nav_menus = array();

    function Holo() {
        add_action( 'init', 'register_menus' );
    }

    function register_menus() {
        register_nav_menus(
            $this->nav_menus
        );
    }

    function holo_add_nav_menus( $menus ) {
        $this->nav_menus = $menus;
    }

}

///*-----------------------------------------------------------
//Get a theme menu name in slug format based on a
//location parameter.
//-----------------------------------------------------------*/
//function get_menu_from_location($theme_location)
//{
//    if(!$theme_location) return false;
//
//    $theme_locations = get_nav_menu_locations();
//    if(!isset($theme_locations[$theme_location])) return false;
//
//    $menu_obj = get_term($theme_locations[$theme_location],'nav_menu');
//    if(!$menu_obj) $menu_obj = false;
//    if(!isset($menu_obj->name)) return false;
//
//    // Make a slug out of the menu name now we know it exists
//    $menu_name = strtolower($menu_obj->name);
//    $menu_name = str_replace(" ","-", $menu_name);
//
//    return $menu_name;
//}

$holo = new Holo();