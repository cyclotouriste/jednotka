<?php

require_once(dirname(__FILE__) . '/options/options.php');

$sections[] = array(
    'icon' => '',
    'title' => __('General', 'nhp-opts'),
    'desc' => __('Global settings', 'nhp-opts'),
    'fields' => array(
        array(
            'id' => 'favicon',
            'type' => 'upload',
            'title' => __('Custom Favicon', 'nhp-opts'),
            'sub_desc' => 'Icon displayed in a browser tab.',
            'desc' => '',
            'std' => ''
        ),
        array(
            'id' => 'layout-style',
            'type' => 'select',
            'title' => __('Layout Style', 'nhp-opts'),
            'sub_desc' => 'The type of layout you want for your website.',
            'desc' => '',
            'options' => array('wide' => 'Wide', 'boxed' => 'Boxed'),
            'std' => 'wide'
        ),
//        array(
//            'id' => 'show-breadcrumbs',
//            'type' => 'checkbox',
//            'title' => __('Show Breadcrumbs', 'nhp-opts'),
//            'sub_desc' => 'Choose if you want the breadcrumbs to be shown on all your pages',
//            'desc' => '',
//            'std' => ''
//        ),
        array(
            'id' => 'top-image',
            'type' => 'upload',
            'title' => __('Top Image', 'nhp-opts'),
            'sub_desc' => 'The displayed Image for the top area.',
            'desc' => '',
            'std' => ''
        ),
        array(
            'id' => 'background-pattern',
            'type' => 'upload',
            'title' => __('Background Pattern', 'nhp-opts'),
            'sub_desc' => 'The background pattern shown when you have already chosen the boxed layout for your website.',
            'desc' => '',
            'std' => ''
        ),
        array(
            'id' => 'background-image',
            'type' => 'upload',
            'title' => __('404 Background Image', 'nhp-opts'),
            'sub_desc' => 'Background Image for "404" page',
            'desc' => '',
            'std' => ''
        ),
        array(
            'id' => 'analytics',
            'type' => 'textarea',
            'title' => __('Google Analytics', 'nhp-opts'),
            'sub_desc' => 'The Google Analytics code you want to insert into your website. It will be placed in every page.',
            'desc' => '',
            'std' => ''
        ),
        array(
            'id' => 'custom-css',
            'type' => 'textarea',
            'title' => __('Custom Css', 'nhp-opts'),
            'sub_desc' => 'Need some custom styles? This is for you.',
            'desc' => '',
            'std' => ''
        ),
    )
);

$sections[] = array(
    'icon' => '',
    'title' => 'Header',
    'desc' => 'Header Customization',
    'fields' => array(
        array(
            'id' => 'logo',
            'type' => 'upload',
            'title' => __('Logo Image', 'nhp-opts'),
            'sub_desc' => 'The image to be displayed as the website\'s logo.',
            'desc' => '',
            'std' => ''
        ),
        array(
            'id' => 'logo-animate',
            'type' => 'checkbox',
            'title' => __('Logo Animation', 'nhp-opts'),
            'sub_desc' => 'Choose if you want to animate the logo on hover.',
            'desc' => '',
            'std' => ''
        ),
        array(
            'id' => 'logo-width',
            'type' => 'text',
            'title' => __('Logo Width', 'nhp-opts'),
            'sub_desc' => 'The width of your logo image.',
            'desc' => '',
            'std' => 117
        ),
        array(
            'id' => 'logo-height',
            'type' => 'text',
            'title' => __('Logo Height', 'nhp-opts'),
            'sub_desc' => 'The height of your logo image.',
            'desc' => '',
            'std' => 39
        ),
        array(
            'id' => 'logo-text',
            'type' => 'text',
            'title' => __('Text As Logo', 'nhp-opts'),
            'sub_desc' => 'This text will be displayed if there is no logo image uploaded.',
            'desc' => '',
            'std' => 'Jednotka'
        ),
    )
);

$sections[] = array(
    'icon' => '',
    'title' => 'Styles',
    'desc' => 'Colors for your website',
    'fields' => array(
        array(
            'id' => 'color-scheme',
            'type' => 'color',
            'title' => __('Theme Color Scheme', 'nhp-opts'),
            'sub_desc' => 'The color scheme used in your entire website.',
            'desc' => '',
            'std' => '#8DC153'
        ),
    )
);

$sections[] = array(
    'icon' => '',
    'title' => 'Typography',
    'desc' => 'Fonts for your website',
    'fields' => array(
        array(
            'id' => 'google-fonts',
            'type' => 'textarea',
            'title' => 'Google Fonts',
            'sub_desc' => 'The link to a google font.',
            'desc' => 'If you have multiple links, separate them with the "|" pipe character.',
            'std' => 'http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800,300,300italic,400italic,600italic,700italic,800italic
                        |http://fonts.googleapis.com/css?family=Roboto:100,400,300,500'
        ),
        array(
            'id' => 'font-family-heading',
            'type' => 'text',
            'title' => 'Font Family',
            'sub_desc' => 'The font family/families for each headings present throughout the website.',
            'desc' => 'e.g. Roboto',
            'std' => 'Roboto'
        ),
        array(
            'id' => 'font-family-content',
            'type' => 'text',
            'title' => 'Font Family',
            'sub_desc' => 'The font family/families for each normal text present throughout the website.',
            'desc' => 'e.g. "Open Sans", "Helvetica Neue", Arial',
            'std' => '"Open Sans", "Helvetica Neue", Arial'
        ),
    )
);

$sections[] = array(
    'icon' => '',
    'title' => 'Blog',
    'desc' => 'Settings for your blog.',
    'fields' => array(
        array(
            'id' => 'author-bio',
            'type' => 'radio',
            'title' => __('Display The Author Bio', 'nhp-opts'),
            'sub_desc' => 'Check if you want the author bio to be displayed after each post.',
            'desc' => '',
            'options' => array('1' => __('Yes', 'nhp-opts'), '0' => __('No', 'nhp-opts')),
            'std' => 1
        ),
        array(
            'id' => 'posts-number',
            'type' => 'text',
            'title' => __('Number of posts per page', 'nhp-opts'),
            'sub_desc' => 'The number of posts you want to be displayed on each blog page.',
            'desc' => '',
            'std' => '6'
        ),
    )
);

$sections[] = array(
    'icon' => '',
    'title' => 'Portfolio',
    'desc' => 'Settings for your portfolio.',
    'fields' => array(
        array(
            'id' => 'portfolio-link',
            'type' => 'text',
            'title' => __('The link of your portfolio page.', 'nhp-opts'),
            'sub_desc' => 'This link is used to localize your portfolio.',
            'desc' => '',
            'std' => ''
        ),
        array(
            'id' => 'portfolio-number',
            'type' => 'text',
            'title' => __('Number of portfolio items per page', 'nhp-opts'),
            'sub_desc' => 'The number of portfolio items you want to be displayed on each portfolio page.',
            'desc' => '',
            'std' => '14'
        ),
        array(
            'id' => 'show-portfolio-related-work',
            'type' => 'checkbox',
            'title' => __('Show related work on each portfolio page', 'nhp-opts'),
            'sub_desc' => '',
            'desc' => '',
            'std' => '
             '
        ),
    )
);

$sections[] = array(
    'icon' => '',
    'title' => 'Slider',
    'desc' => 'Settings for your slider.',
    'fields' => array(
        array(
            'id' => 'slider-type',
            'type' => 'select',
            'title' => __('Choose the type of slider you want to have on your home page.', 'nhp-opts'),
            'sub_desc' => '',
            'options' => array('default' => 'Default', 'with_background' => 'With Background', 'with_images' => 'With Images'),
            'desc' => '',
            'std' => ''
        ),
        array(
            'id' => 'slider-background',
            'type' => 'upload',
            'title' => __('Default slider background.', 'nhp-opts'),
            'sub_desc' => '',
            'desc' => '',
            'std' => ''
        ),
    )
);

$sections[] = array(
    'icon' => '',
    'title' => 'Social',
    'desc' => 'Social Accounts. (The fields left empty will not be shown into your pages.)',
    'fields' => array(
        array(
            'id' => 'facebook-link',
            'type' => 'text',
            'title' => __('Facebook', 'nhp-opts'),
            'sub_desc' => 'Your Facebook account',
            'desc' => '',
            'std' => ''
        ),
        array(
            'id' => 'twitter-link',
            'type' => 'text',
            'title' => __('Twitter', 'nhp-opts'),
            'sub_desc' => 'Your Twitter account',
            'desc' => '',
            'std' => ''
        ),
        array(
            'id' => 'pinterest-link',
            'type' => 'text',
            'title' => __('Pinterest', 'nhp-opts'),
            'sub_desc' => 'Your Pinterest account',
            'desc' => '',
            'std' => ''
        ),
        array(
            'id' => 'google-plus-link',
            'type' => 'text',
            'title' => __('Google Plus', 'nhp-opts'),
            'sub_desc' => 'Your Google Plus account',
            'desc' => '',
            'std' => ''
        ),
        array(
            'id' => 'tumblr-link',
            'type' => 'text',
            'title' => __('Tumblr', 'nhp-opts'),
            'sub_desc' => 'Your Tumblr account',
            'desc' => '',
            'std' => ''
        ),
        array(
            'id' => 'dribble-link',
            'type' => 'text',
            'title' => __('Dribble', 'nhp-opts'),
            'sub_desc' => 'You Dribble account',
            'desc' => '',
            'std' => ''
        ),array(
            'id' => 'instagram-link',
            'type' => 'text',
            'title' => __('Instagram', 'nhp-opts'),
            'sub_desc' => '',
            'desc' => '',
            'std' => ''
        ),
        array(
            'id' => 'rss-link',
            'type' => 'text',
            'title' => __('RSS', 'nhp-opts'),
            'sub_desc' => 'Your RSS account',
            'desc' => '',
            'std' => ''
        ),

    )
);

$sections[] = array(
    'icon' => '',
    'title' => 'Contact',
    'desc' => 'Your contact info. (You can show these contact info by placing the Contact Widget on the page.)',
    'fields' => array(
        array(
            'id' => 'contact-address',
            'type' => 'text',
            'title' => __('Contact Address', 'nhp-opts'),
            'sub_desc' => '',
            'desc' => 'To insert a new line use "<b>&lt;br /&gt;</b>" character',
            'std' => ''
        ),
        array(
            'id' => 'contact-phone',
            'type' => 'text',
            'title' => __('Contact Phone', 'nhp-opts'),
            'sub_desc' => '',
            'desc' => '',
            'std' => ''
        ),
        array(
            'id' => 'contact-email',
            'type' => 'text',
            'title' => __('Contact Email', 'nhp-opts'),
            'sub_desc' => 'Separate multiple emails by comma',
            'desc' => 'e.g. info@domain.com, support@domain.com',
            'std' => ''
        ),
        array(
            'id' => 'contact-website',
            'type' => 'text',
            'title' => __('Website', 'nhp-opts'),
            'sub_desc' => '',
            'desc' => '',
            'std' => ''
        ),
        array(
            'id' => 'contact-schedule',
            'type' => 'text',
            'title' => __('Contact Schedule', 'nhp-opts'),
            'sub_desc' => '',
            'desc' => 'To insert a new line use "<b>&lt;br /&gt;</b>" character',
            'std' => ''
        ),
        array(
            'id' => 'show-contact-map',
            'type' => 'checkbox',
            'title' => __('Display contact map on your contact page', 'nhp-opts'),
            'sub_desc' => '',
            'desc' => '',
            'std' => ''
        ),
        array(
            'id' => 'contact-map',
            'type' => 'text',
            'title' => __('Contact Map', 'nhp-opts'),
            'sub_desc' => 'The place to be shown on the google map.',
            'desc' => 'Insert the latitude and longitude separated by comma. e.g. 39.777408,-86.137969',
            'std' => ''
        ),
    )
);


$sections[] = array(
    'icon' => '',
    'title' => 'Footer',
    'desc' => 'Footer settings',
    'fields' => array(
        array(
            'id' => 'copyright',
            'type' => 'textarea',
            'title' => __('Copyright Text', 'nhp-opts'),
            'sub_desc' => '',
            'desc' => '',
            'std' => ''
        ),
    )
);

$sections[] = array(
    'icon' => '',
    'title' => 'Coming Soon',
    'desc' => 'Options for "Coming Soon" page.',
    'fields' => array(
        array(
            'id' => 'show-coming-soon',
            'type' => 'checkbox',
            'title' => __('Show "Coming Soon" page', 'nhp-opts'),
            'sub_desc' => 'Choose if you want to show the "Coming Soon" page to normal users.',
            'desc' => '',
            'std' => ''
        ),
        array(
            'id' => 'coming-soon-background-image',
            'type' => 'upload',
            'title' => __('Background Image', 'nhp-opts'),
            'sub_desc' => 'Background Image for the "Coming Soon" page',
            'desc' => '',
            'std' => ''
        ),
        array(
            'id' => 'coming-soon-message',
            'type' => 'text',
            'title' => __('Message', 'nhp-opts'),
            'sub_desc' => 'Message that will be displayed on the "Coming Soon" page.',
            'desc' => '',
            'std' => 'Don\'t worry, we\'re still working on it and we\'re coming soon!'
        ),
        array(
            'id' => 'show-coming-soon-timer',
            'type' => 'checkbox',
            'title' => __('Coming Soon Timer', 'nhp-opts'),
            'sub_desc' => 'Choose if you want to display the timer on the "Coming Soon" page',
            'desc' => '',
            'std' => ''
        ),
        array(
            'id' => 'coming-soon-date',
            'type' => 'text',
            'title' => __('Opening Date', 'nhp-opts'),
            'sub_desc' => 'The date when the website will be on',
            'desc' => 'e.g. 2015/09/17',
            'std' => '2015/09/17'
        ),
        array(
            'id' => 'coming-soon-time',
            'type' => 'text',
            'title' => __('Opening Time', 'nhp-opts'),
            'sub_desc' => 'The time when the website will be on',
            'desc' => 'e.g. 19:50:00',
            'std' => '19:50:00'
        ),
    )
);

$sections[] = array(
    'icon' => '',
    'title' => 'Under Construction',
    'desc' => 'Options for "Under Construction" page.',
    'fields' => array(
        array(
            'id' => 'show-under-construction',
            'type' => 'checkbox',
            'title' => __('Show "Under construction" page', 'nhp-opts'),
            'sub_desc' => 'Choose if you want to show the "Coming Soon" page to normal users.',
            'desc' => '',
            'std' => ''
        ),
        array(
            'id' => 'under-construction-background-image',
            'type' => 'upload',
            'title' => __('Background Image', 'nhp-opts'),
            'sub_desc' => 'Background Image for the "Coming Soon" page',
            'desc' => '',
            'std' => ''
        ),
        array(
            'id' => 'under-construction-message',
            'type' => 'text',
            'title' => __('Message', 'nhp-opts'),
            'sub_desc' => 'Message that will be displayed on the "Under Construction" page.',
            'desc' => '',
            'std' => 'We\'re currently updating this page.'
        ),
        array(
            'id' => 'show-under-construction-timer',
            'type' => 'checkbox',
            'title' => __('Under Construction Timer', 'nhp-opts'),
            'sub_desc' => 'Choose if you want to display the timer on the "Under Construction" page',
            'desc' => '',
            'std' => ''
        ),
        array(
            'id' => 'under-construction-date',
            'type' => 'text',
            'title' => __('Opening Date', 'nhp-opts'),
            'sub_desc' => 'The time when the construction state is done',
            'desc' => 'e.g. 2015/09/17',
            'std' => '2015/09/17'
        ),
        array(
            'id' => 'under-construction-time',
            'type' => 'text',
            'title' => __('Opening Time', 'nhp-opts'),
            'sub_desc' => 'The time when the construction state is done',
            'desc' => 'e.g. 19:50:00',
            'std' => '19:50:00'
        ),
    )
);

$sections[] = array(
    'icon' => '',
    'title' => 'Meta keywords',
    'desc' => 'Page meta options',
    'fields' => array(
        array(
            'id' => 'activate-meta',
            'type' => 'checkbox',
            'title' => __('Activate meta keywords', 'nhp-opts'),
            'sub_desc' => '',
            'desc' => '',
            'std' => ''
        ),
        array(
            'id' => 'meta-author',
            'type' => 'text',
            'title' => __('Meta author', 'nhp-opts'),
            'sub_desc' => '',
            'desc' => '',
            'std' => ''
        ),
        array(
            'id' => 'meta-description',
            'type' => 'textarea',
            'title' => __('Meta description', 'nhp-opts'),
            'sub_desc' => '',
            'desc' => '',
            'std' => ''
        ),
        array(
            'id' => 'meta-keywords',
            'type' => 'textarea',
            'title' => __('Meta Keywords', 'nhp-opts'),
            'sub_desc' => '',
            'desc' => '',
            'std' => ''
        ),
        array(
            'id' => 'robots',
            'type' => 'checkbox',
            'title' => __('Activate robots indexing', 'nhp-opts'),
            'sub_desc' => '',
            'desc' => '',
            'std' => ''
        ),
        array(
            'id' => 'general-bot',
            'type' => 'select',
            'title' => __('General bot indexing type', 'nhp-opts'),
            'sub_desc' => '',
            'desc' => '',
            'std' => '',
            'options' => array('none' => 'none', 'index, follow' => 'index, follow', 'index, nofollow' => 'index, nofollow', 'index, all' => 'index, all', 'index, follow, archive' => 'index, follow, archive', 'noindex, follow' => 'noindex, follow', 'noindex, nofollow' => 'noindex, nofollow'),
        ),
        array(
            'id' => 'google-bot',
            'type' => 'select',
            'title' => __('Google bot indexing type', 'nhp-opts'),
            'sub_desc' => '',
            'desc' => '',
            'std' => '',
            'options' => array('none' => 'none', 'index, follow' => 'index, follow', 'index, nofollow' => 'index, nofollow', 'index, all' => 'index, all', 'index, follow, archive' => 'index, follow, archive', 'noindex, follow' => 'noindex, follow', 'noindex, nofollow' => 'noindex, nofollow'),
        ),
    )
);

$args['opt_name'] = 'jednotka_options';
$args['menu_title'] = 'Jednotka Options';
$args['page_slug'] = 'jednotka_options';
$args['page_title'] = 'Jednotka Options';
$args['page_cap'] = 'manage_options';
$args['dev_mode'] = false;
$args['stylesheet_override'] = false;
$args['page_position'] = 124;

$NHP_Options = new NHP_Options($sections, $args);
