<?php

function register_my_menus() {
    register_nav_menus(
        array(
            'jednotka_header_navigation' => __( 'Jednotka Header Navigation', 'jednotka' )
        )
    );
}
add_action( 'init', 'register_my_menus' );


// Translate, if applicable
define( 'THEME_TEXT_DOMAIN', 'jednotka' );

load_theme_textdomain( THEME_TEXT_DOMAIN, STYLESHEET_DIR . '/languages' );

// Translate, if applicable
load_theme_textdomain( 'thematic', HOLO_LIB . '/languages' );

function custom_excerpt_length($length) {
    return 20;
}

add_filter('excerpt_length', 'custom_excerpt_length', 999);

add_action('after_setup_theme', 'my_theme_setup');
function my_theme_setup(){
    load_theme_textdomain('jednotka', get_template_directory() . '/languages');
}
