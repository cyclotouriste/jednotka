<?php
class Main_Nav_Walker extends Walker_Nav_Menu {

    function start_lvl( &$output, $depth = 0, $args = array() ) {
        $output .= '<ul class="dropdown-menu" role="menu">';
    }

    function end_lvl( &$output, $depth = 0, $args = array() ) {
        $output .= '</ul>';
    }

    /*-----------------------------------------------------------
Get a theme menu name in slug format based on a
location parameter.
-----------------------------------------------------------*/
    function get_menu_from_location($theme_location)
    {
        if(!$theme_location) return false;

        $theme_locations = get_nav_menu_locations();
        if(!isset($theme_locations[$theme_location])) return false;

        $menu_obj = get_term($theme_locations[$theme_location],'nav_menu');
        if(!$menu_obj) $menu_obj = false;
        if(!isset($menu_obj->name)) return false;

        // Make a slug out of the menu name now we know it exists
        $menu_name = strtolower($menu_obj->name);
        $menu_name = str_replace(" ","-", $menu_name);

        return $menu_name;
    }

    function get_menu_children($item_id) {
        $children = array();

        $menu_name = $this->get_menu_from_location('jednotka_header_navigation');

        $items = wp_get_nav_menu_items($menu_name);

        foreach( $items as $menu_item ) {
            if ($menu_item->menu_item_parent == $item_id) {
                $children[] = $menu_item;
            }
        }

        return $children;
    }

    function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {

        $page_id = get_queried_object_id();

        $menu_id = $item->object_id;

        $children = $this->get_menu_children($item->ID);

        $active = false;

        if ((int)$page_id === (int)$menu_id) {
            $active = true;
        }

        foreach($children as $child) {
            if ($child->object_id == $page_id) {
                $active = true;
            }
        }

        $active_class = '';
        if ($active) {
            $active_class = 'active';
        }

        if ( $depth === 0 ) {

            if (!empty($children)) {
                $output .= '<li class="dropdown ' . $active_class .  '">' . esc_attr($item->label);

                $att = '
                <span>
                  ' . $item->title . '
                  <i class="fa-icon-angle-down"></i>
                </span>';
            }
            else {
                $output .= '<li class="' . $active_class .  '">' . esc_attr($item->label);

                $att = '
                <span>
                  ' . $item->title . '
                </span>';
            }
        } else {

            if (!empty($children)) {
                $output .= '<li class="' . $active_class .  ' dropdown-submenu">' . esc_attr($item->label);
                $att = $item->title . '<i class="fa-icon-angle-right"></i>';
            }
            else {
                $output .= '<li class="' . $active_class .  '">' . esc_attr($item->label);
                $att = $item->title;
            }
//            $childrenString = '';
//                foreach($children as $child) {
//                    $childrenString .= $child;
//                }
        }

        $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
        $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
        $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
        $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
        $attributes .= ' class="' . ( $depth === 0 ? 'dropdown-toggle' : '' ) . '"';
        $attributes .= ( $depth === 0 ? ' data-delay="50" data-hover="dropdown"' : '');

        $item_output = $args->before . '<a' . $attributes . '>' . $args->link_before . apply_filters( 'the_title', $att, $item->ID ) .
            $args->link_after . '</a>' . $args->after;

//        $item_output = sprintf( '%1$s<a%2$s>%3$s%4$s%5$s</a>%6$s',
//            $args->before,
//            $attributes,
//            $args->link_before,
//            apply_filters( 'the_title', $att, $item->ID ),
//            $args->link_after,
//            $args->after
//        );

        // build html
        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );

    }

    function end_el( &$output, $item, $depth = 0, $args = array() ) {
        $output .= "</li>\n";
    }

}