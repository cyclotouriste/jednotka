<?php

class Jed_Widget_Tags extends WP_Widget {

    function __construct() {
        parent::__construct('jed_tags', __('Jednotka Tag Cloud', 'jednotka'), array('description' => __('Jednotka style tag Cloud', 'jednotka')));
    }

    function widget($args, $instance) {
        extract($args);

        $tags = get_terms(array('post_tag'), array( 'orderby' => 'count', 'order' => 'DESC' ) ); // Always query top tags

        $title = $instance['title'];

        echo $before_widget;

        echo $before_title . $title . $after_title;

        ?>

        <div class="button-cloud-box">

            <p>
                <?php foreach($tags as $tag) { echo '<a href="' . get_term_link( $tag ) . '" class="btn btn-bordered btn-sm text-medium-light">' . $tag->name . '</a>'; } ?>
            </p>

        </div>

        <?php

        echo $after_widget;
    }

    function form($instance) {
        $instance = wp_parse_args( (array)$instance, array(
            'title' => 'Tag Cloud'
        ));
        ?>

        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', 'clubber'); ?></label>
            <input type="text" class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php
            echo $this->get_field_name('title'); ?>" value="<?php echo $instance['title']; ?>" />
        </p>

        <?php
    }

    function update($new_instance, $old_instance) {
		$instance = $old_instance;
	    $instance['title'] = strip_tags($new_instance['title']);

	    return $instance;
    }

}

class Jed_Widget_Categories extends WP_Widget_Categories {

    function widget($args, $instance) {
        extract($args);

        $title = apply_filters('widget_title', empty( $instance['title'] ) ? __( 'Categories', 'jednotka' ) : $instance['title'], $instance, $this->id_base);

        echo $before_widget;

        echo $before_title . $title . $after_title;

//        wp_list_categories( );

        $args = array(
            'type' => 'post'
        );
        $post_categories = get_categories( $args );

        echo '<div class="list-group">';

        foreach ( $post_categories as $post_category ) {
            ?>

            <a class="list-group-item" href="<?php echo get_home_url() . '?cat=' .  $post_category->cat_ID ?>"><i class='fa-icon-angle-right fa-icon-fixed-width'></i>
                <span><?php echo $post_category->name ?></span>
            </a>

        <?php
        }

        echo '</div>';

        echo $after_widget;
    }

}

class Jed_Widget_Pages extends WP_Widget_Pages {
    function widget( $args, $instance ) {
        extract( $args );

        $title = apply_filters('widget_title', empty( $instance['title'] ) ? __( 'Pages', 'jednotka' ) : $instance['title'], $instance, $this->id_base);
        $sortby = empty( $instance['sortby'] ) ? 'menu_order' : $instance['sortby'];
        $exclude = empty( $instance['exclude'] ) ? '' : $instance['exclude'];

        if ( $sortby == 'menu_order' )
            $sortby = 'menu_order, post_title';

        $pages = get_pages();

        if ( !empty( $pages ) ) {
            echo $before_widget;

            if ( $title)
                echo $before_title . $title . $after_title;
            ?>
            <?php echo '<div class="list-group">'; ?>

                <?php foreach ( $pages as $page ) { ?>

                    <a class="list-group-item" href="<?php echo get_home_url() . '?page_id=' .  $page->ID ?>"><i class='fa-icon-angle-right fa-icon-fixed-width'></i>
                        <?php echo $page->post_title ?>
                    </a>

                <?php } // endforeach ?>

            <?php

            echo '</div>';

            echo $after_widget;
        }
    }
}

class Jed_Widget_Contact extends WP_Widget {

    function __construct() {
        $widget_ops = array('description' => __('Display contact info', 'jednotka'));
        parent::__construct( 'contact-widget', __('Jednotka Contact Widget', 'jednotka'), $widget_ops);
    }

    function form($instance) {
        $instance = wp_parse_args( (array)$instance, array(
            'title' => 'Contact'
        ));
        ?>

        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', 'clubber'); ?></label>
            <input type="text" class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php
                echo $this->get_field_name('title'); ?>" value="<?php echo $instance['title']; ?>" />
        </p>

        <p>
            <label for="<?php echo $this->get_field_id('address'); ?>"><?php _e('Address:', 'clubber'); ?></label>
            <input type="text" class="widefat" id="<?php echo $this->get_field_id('address'); ?>" name="<?php
                echo $this->get_field_name('address'); ?>" value="<?php echo $instance['address']; ?>" />
        </p>

        <p>
            <label for="<?php echo $this->get_field_id('phone'); ?>"><?php _e('Phone:', 'clubber'); ?></label>
            <input type="text" class="widefat" id="<?php echo $this->get_field_id('phone'); ?>" name="<?php
            echo $this->get_field_name('phone'); ?>" value="<?php echo $instance['phone']; ?>" />
        </p>

        <p>
            <label for="<?php echo $this->get_field_id('email'); ?>"><?php _e('Email:', 'clubber'); ?></label>
            <input type="text" class="widefat" id="<?php echo $this->get_field_id('email'); ?>" name="<?php
            echo $this->get_field_name('email'); ?>" value="<?php echo $instance['email']; ?>" />
        </p>

        <p>
            <label for="<?php echo $this->get_field_id('website'); ?>"><?php _e('Website:', 'clubber'); ?></label>
            <input type="text" class="widefat" id="<?php echo $this->get_field_id('website'); ?>" name="<?php
            echo $this->get_field_name('website'); ?>" value="<?php echo $instance['website']; ?>" />
        </p>

        <?php
    }

    function update($new_instance, $old_instance) {
        $instance           = $old_instance;
        $instance['title']  = strip_tags($new_instance['title']);
        $instance['address']  = strip_tags($new_instance['address']);
        $instance['phone']  = strip_tags($new_instance['phone']);
        $instance['email']  = strip_tags($new_instance['email']);
        $instance['website']  = strip_tags($new_instance['website']);

        return $instance;
    }

    function widget($args, $instance) {
        extract($args, EXTR_SKIP);
        $title = apply_filters('widget_title', $instance['title']);
        $address = $instance['address'];
        $phone = $instance['phone'];
        $email = $instance['email'];
        $website = $instance['website'];
        ?>

        <?php

        echo $before_widget;

        if ($title) {
            echo $before_title . $title . $after_title;
        }
        ?>

        <div class="icon-boxes">

        <?php

        if (!empty($address)) {
        ?>
            <div class="icon-box">
                <div class="icon icon-wrap">
                    <i class="fa-icon-map-marker"></i>
                </div>
                <div class="content">
                    <?php echo $address ?>
                </div>
            </div>

        <?php
        }

        if (!empty($phone)) {
            ?>
            <div class="icon-box">
                <div class="icon icon-wrap">
                    <i class="fa-icon-phone"></i>
                </div>
                <div class="content">
                    <a href="tel:<?php echo $phone ?>"><?php echo $phone ?></a>
                </div>
            </div>

        <?php
        }

        if (!empty($email)) {
            ?>
            <div class="icon-box">
                <div class="icon icon-wrap">
                    <i class="fa-icon-envelope"></i>
                </div>
                <div class="content">
                    <a href="mailto:<?php echo $email ?>"><?php echo $email ?></a>
                </div>
            </div>

        <?php
        }

        if (!empty($website)) {
            ?>
            <div class="icon-box">
                <div class="icon icon-wrap">
                    <i class="fa-icon-globe"></i>
                </div>
                <div class="content">
                    <a href="<?php echo $website ?>"><?php echo $website ?></a>
                </div>
            </div>

        <?php
        }

        ?>

        </div>
        <?php

        echo $after_widget;

        ?>

        <?php
    }

}

class Jed_Flickr_Widget extends WP_Widget {

    function __construct() {
        $widget_ops = array('description' => __('Display images from a flickr account', 'jednotka'), 'classname' => 'flickr-widget');

        parent::__construct( 'flickr-widget', __('Jednotka Flickr Widget', 'jednotka'), $widget_ops );
    }

    /* WIDGET ADMIN FORM */
    /* @instance  The array of keys and values for the widget */
    function form($instance) {
        $instance = wp_parse_args((array) $instance, array(
            'title' => 'Photos on flickr',
            'id' => '',
            'number' => '9'
        ));
        ?>

        <p>
            <label for="<?php
            echo $this->get_field_id('title');
            ?>"><?php
                _e('Title:', 'clubber');
                ?></label>
            <input type="text" class="widefat" id="<?php
            echo $this->get_field_id('title');
            ?>" name="<?php
            echo $this->get_field_name('title');
            ?>" value="<?php
            echo $instance['title'];
            ?>" />
        </p>

        <p>
            <label for="<?php
            echo $this->get_field_id('id');
            ?>">Flickr ID (<a href="http://www.idgettr.com" target="_blank">idGettr</a>):</label>
            <input class="widefat" id="<?php
            echo $this->get_field_id('id');
            ?>" name="<?php
            echo $this->get_field_name('id');
            ?>" type="text" value="<?php
            echo $instance['id'];
            ?>" />
        </p>

        <p>
            <label for="<?php
            echo $this->get_field_id('number');
            ?>">Number of photos:</label>
            <input class="widefat" id="<?php
            echo $this->get_field_id('number');
            ?>" name="<?php
            echo $this->get_field_name('number');
            ?>" type="text" value="<?php
            echo $instance['number'];
            ?>" />
        </p>

    <?php
    }

    /* UPDATE THE WIDGET */
    function update($new_instance, $old_instance) {
        $instance           = $old_instance;
        $instance['title']  = strip_tags($new_instance['title']);
        $instance['id']     = strip_tags($new_instance['id']);
        $instance['number'] = (int) $new_instance['number'];
        return $instance;
    }

    /* DISPLAY THE WIDGET */
    /* @args --> The array of form elements */
    function widget($args, $instance) {
        extract($args, EXTR_SKIP);
        $title = apply_filters('widget_title', $instance['title']);
        $id    = $instance['id'];
        if (!$number = (int) $instance['number']) {
            $number = 3;
        } else if ($number < 1) {
            $number = 1;
        } else if ($number > 9) {
            $number = 9;
        }
        /* before widget */
        echo $before_widget;

        /* display the widget */
        ?>
        <div class="flickr-widget widget">
            <?php
            if ($title) {
                echo $before_title . $title . $after_title;
            }
            ?>

            <div class="flickr-images clearfix">
                <script type="text/javascript" src="http://www.flickr.com/badge_code_v2.gne?count=<?php
                echo $number;
                ?>&amp;display=latest&amp;size=s&amp;layout=x&amp;source=user&amp;user=<?php
                echo $id;
                ?>"></script>

                <div class="clear"></div>
            </div><!-- end .flickr -->
        </div><!-- end .widgets-col-flickr -->
        <?php
        /* after widget */
        echo $after_widget;
    }

}

class Jed_Widget_Extra extends WP_Widget {

    function __construct() {
        parent::__construct( false, $name = 'Jednotka Extra Widget', array('description' => '') );
    }

    function form($instance) {
        $extra_description = esc_attr($instance['extra_description']);
    ?>

        <p>
            <label for="<?php echo $this->get_field_id('extra_description') ?>"><?php echo $this->get_field_name('extra_description') ?></label>
            <textarea id="<?php echo $this->get_field_id('extra_description') ?>" name="<?php echo $this->get_field_name('extra_description') ?>"><?php echo $extra_description; ?></textarea>
        </p>

    <?php
    }

    function widget($args, $instance) {
        global $NHP_Options;
        $options = $NHP_Options->options;

        echo $args['before_widget'];
    ?>

        <div class="logo-container">
            <img width="<?php echo $options['logo-width'] ?>" height="<?php echo $options['logo-height'] ?>" src="<?php echo $options['logo'] ?>" />
        </div>

        <p>
            <?php echo $instance['extra_description'] ?>
        </p>

    <?php
        echo $args['after_widget'];
    }

}

class Jed_Widget_Recent_Posts extends WP_Widget_Recent_Posts {

    function widget($args, $instance) {
        $cache = wp_cache_get('widget_recent_posts', 'widget');

        if ( !is_array($cache) )
            $cache = array();

        if ( ! isset( $args['widget_id'] ) )
            $args['widget_id'] = $this->id;

        if ( isset( $cache[ $args['widget_id'] ] ) ) {
            echo $cache[ $args['widget_id'] ];
            return;
        }

        ob_start();
        extract($args);

        $title = apply_filters('widget_title', empty($instance['title']) ? __('Recent Posts', 'jednotka') : $instance['title'], $instance, $this->id_base);
        if ( empty( $instance['number'] ) || ! $number = absint( $instance['number'] ) )
            $number = 10;
        $show_date = isset( $instance['show_date'] ) ? $instance['show_date'] : false;

        $r = new WP_Query( apply_filters( 'widget_posts_args', array( 'posts_per_page' => $number, 'no_found_rows' => true, 'post_status' => 'publish', 'ignore_sticky_posts' => true ) ) );
        if ($r->have_posts()) :
            ?>
            <?php echo $before_widget; ?>
            <?php if ( $title ) echo $before_title . $title . $after_title; ?>
            <div class="list-group">
                <?php while ( $r->have_posts() ) : $r->the_post(); ?>

                    <a class="list-group-item" href="<?php the_permalink() ?>" title="<?php echo esc_attr( get_the_title() ? get_the_title() : get_the_ID() ); ?>"><i class='fa-icon-angle-right fa-icon-fixed-width'></i><span><?php if ( get_the_title() ) the_title(); else the_ID(); ?></span></a>

                <?php endwhile; ?>
            </div>
            <?php echo $after_widget; ?>
            <?php
            // Reset the global $the_post as this query will have stomped on it
            wp_reset_postdata();

        endif;

        $cache[$args['widget_id']] = ob_get_flush();
        wp_cache_set('widget_recent_posts', $cache, 'widget');
    }

}

class Jed_Widget_Recent_Comments extends WP_Widget_Recent_Comments {
    function widget( $args, $instance ) {
        global $comments, $comment;

        $cache = wp_cache_get('widget_recent_comments', 'widget');

        if ( ! is_array( $cache ) )
            $cache = array();

        if ( ! isset( $args['widget_id'] ) )
            $args['widget_id'] = $this->id;

        if ( isset( $cache[ $args['widget_id'] ] ) ) {
            echo $cache[ $args['widget_id'] ];
            return;
        }

        extract($args, EXTR_SKIP);
        $output = '';
        $title = apply_filters( 'widget_title', empty( $instance['title'] ) ? __( 'Recent Comments', 'jednotka' ) : $instance['title'], $instance, $this->id_base );

        if ( empty( $instance['number'] ) || ! $number = absint( $instance['number'] ) )
            $number = 5;

        $comments = get_comments( apply_filters( 'widget_comments_args', array( 'number' => $number, 'status' => 'approve', 'post_status' => 'publish' ) ) );
        $output .= $before_widget;
        if ( $title )
            $output .= $before_title . $title . $after_title;

        $output .= '<div class="list-group">';
        if ( $comments ) {
            // Prime cache for associated posts. (Prime post term cache if we need it for permalinks.)
            $post_ids = array_unique( wp_list_pluck( $comments, 'comment_post_ID' ) );
            _prime_post_caches( $post_ids, strpos( get_option( 'permalink_structure' ), '%category%' ), false );

            foreach ( (array) $comments as $comment) {
                $output .=  '<a class="list-group-item" href="' . esc_url( get_comment_link($comment->comment_ID)) . '"><i class="fa-icon-angle-right fa-icon-fixed-width"></i><span>' .  sprintf('%1$s', get_comment_author_link()) . ' ' . __('on', THEME_TEXT_DOMAIN) . ' ' . get_the_title($comment->comment_post_ID) . '</span></a>';
            }
        }
        $output .= '</div>';
        $output .= $after_widget;

        echo $output;
        $cache[$args['widget_id']] = $output;
        wp_cache_set('widget_recent_comments', $cache, 'widget');
    }
}

class Jed_Widget_Meta extends WP_Widget_Meta {

    function register_filter($content) {
        $content = '<a class="list-group-item" href="' . admin_url() . '"><i class="fa-icon-angle-right fa-icon-fixed-width"></i><span>' . __('Site Admin', 'jednotka') . '</span></a>';

        return $content;
    }

    function loginout_filter($content) {
        if ( ! is_user_logged_in() )
            $content = '<a class="list-group-item" href="' . esc_url( wp_login_url() ) . '"><i class="fa-icon-angle-right fa-icon-fixed-width"></i><span>' . __('Log in', 'jednotka') . '</span></a>';
        else
            $content = '<a class="list-group-item" href="' . esc_url( wp_logout_url() ) . '"><i class="fa-icon-angle-right fa-icon-fixed-width"></i><span>' . __('Log out', 'jednotka') . '</span></a>';

        return $content;
    }

    function widget( $args, $instance ) {
        extract($args);
        $title = apply_filters('widget_title', empty($instance['title']) ? __('Meta', 'jednotka') : $instance['title'], $instance, $this->id_base);

        add_filter('register', array($this, 'register_filter'));
        add_filter('loginout', array($this, 'loginout_filter'));

        echo $before_widget;
        if ( $title )
            echo $before_title . $title . $after_title;
        ?>
        <div class="list-group">
            <?php wp_register(); ?>
            <?php wp_loginout(); ?>
            <a class="list-group-item" href="<?php bloginfo('rss2_url'); ?>" title="<?php echo esc_attr(__('Syndicate this site using RSS 2.0', 'jednotka')); ?>"><i class="fa-icon-angle-right fa-icon-fixed-width"></i><span><?php _e('Entries <abbr title="Really Simple Syndication">RSS</abbr>'); ?></span></a>
            <a class="list-group-item" href="<?php bloginfo('comments_rss2_url'); ?>" title="<?php echo esc_attr(__('The latest comments to all posts in RSS', 'jednotka')); ?>"><i class="fa-icon-angle-right fa-icon-fixed-width"></i><span><?php _e('Comments <abbr title="Really Simple Syndication">RSS</abbr>'); ?></span></a>
            <a class="list-group-item" href="<?php esc_attr_e( 'http://wordpress.org/' ); ?>" title="<?php echo esc_attr(__('Powered by WordPress, state-of-the-art semantic personal publishing platform.', 'jednotka')); ?>"><i class="fa-icon-angle-right fa-icon-fixed-width"></i><span><?php
                    /* translators: meta widget link text */
                    _e( 'WordPress.org', 'jednotka' );
                    ?></span></a>
            <?php wp_meta(); ?>
        </div>
        <?php
        echo $after_widget;
    }

}

class Jed_Widget_Archives extends WP_Widget_Archives {

    function widget( $args, $instance ) {
        extract($args);

        $c = ! empty( $instance['count'] ) ? '1' : '0';
        $d = ! empty( $instance['dropdown'] ) ? '1' : '0';
        $title = apply_filters('widget_title', empty($instance['title']) ? __('Archives', 'jednotka') : $instance['title'], $instance, $this->id_base);

        echo $before_widget;
        if ( $title )
            echo $before_title . $title . $after_title;

        if ( $d ) {
            ?>
            <select name="archive-dropdown" onchange='document.location.href=this.options[this.selectedIndex].value;'> <option value=""><?php echo esc_attr(__('Select Month', 'jednotka')); ?></option> <?php wp_get_archives(apply_filters('widget_archives_dropdown_args', array('type' => 'monthly', 'format' => 'option', 'show_post_count' => $c))); ?> </select>
        <?php
        } else {
            ?>

            <div class="list-group">

            <?php
            global $wpdb, $wp_locale;
            $r = array(
                'type' => 'monthly', 'limit' => '',
                'format' => 'html', 'before' => '',
                'after' => '', 'show_post_count' => false,
                'echo' => 1, 'order' => 'DESC',
            );
            extract( $r, EXTR_SKIP );

            if ( '' != $limit ) {
                $limit = absint($limit);
                $limit = ' LIMIT '.$limit;
            }

            $where = "WHERE post_type = 'post' AND post_status = 'publish'";
            $join = '';
            $output = '';

            $query = "SELECT YEAR(post_date) AS `year`, MONTH(post_date) AS `month`, count(ID) as posts FROM $wpdb->posts $join $where GROUP BY YEAR(post_date), MONTH(post_date) ORDER BY post_date $order $limit";
            $key = md5($query);
            $cache = wp_cache_get( 'wp_get_archives' , 'general');
            if ( !isset( $cache[ $key ] ) ) {
                $arcresults = $wpdb->get_results($query);
                $cache[ $key ] = $arcresults;
                wp_cache_set( 'wp_get_archives', $cache, 'general' );
            } else {
                $arcresults = $cache[ $key ];
            }
            if ( $arcresults ) {
                $afterafter = $after;
                foreach ( (array) $arcresults as $arcresult ) {
                    $url = get_month_link( $arcresult->year, $arcresult->month );
                    /* translators: 1: month name, 2: 4-digit year */
                    $text = sprintf(__('%1$s %2$d', 'jednotka'), $wp_locale->get_month($arcresult->month), $arcresult->year);
                    if ( $show_post_count )
                        $after = '&nbsp;('.$arcresult->posts.')' . $afterafter;

                    $output .= '<a class="list-group-item" href="' . $url . '"><i class="fa-icon-angle-right fa-icon-fixed-width"></i><span>' . $text . '</span></a>';
                }
            }

            echo $output;
            ?>

            </div>

        <?php
        }

        echo $after_widget;
    }
}

add_action('widgets_init', 'register_my_widgets');

function register_my_widgets() {
    register_widget('Jed_Widget_Tags');
    register_widget('Jed_Widget_Categories');
    register_widget('Jed_Widget_Pages');
    register_widget('Jed_Flickr_Widget');
    register_widget('Jed_Widget_Contact');
    register_widget('Jed_Widget_Extra');
    register_widget('Jed_Widget_Recent_Posts');
    register_widget('Jed_Widget_Recent_Comments');
    register_widget('Jed_Widget_Meta');
    register_widget('Jed_Widget_Archives');

//    if ( (float)phpversion() >= 5.3) {
//        register_widget('Jed_Widget_Latest_Tweets');
//    }
}
