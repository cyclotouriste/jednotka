<?php
if ( ! isset( $content_width ) )
    $content_width = 1170;

function mytheme_adjust_content_width() {
    global $content_width;

    if ( is_page_template( 'template-blog-large.php' ) || is_page_template( 'template-blog-small.php' ) || is_single()  ) {
        $content_width = 848;
    }
}

// Path constants
define( 'HOLO_LIB',  get_template_directory() .  '/library' );
define( 'HOLO_STYLESHEET', get_template_directory_uri());
define( 'STYLESHEET_DIR', get_template_directory());

add_theme_support( 'nav-menus' );

include_once( HOLO_LIB . '/theme/init.php' );

include_once( HOLO_LIB . '/theme/Main_Nav_Walker.php');

include_once( HOLO_LIB . '/theme_options/theme_options.php' );

include_once( HOLO_LIB . '/framework/tgm-plugin/recommended-plugins.php');

include_once( HOLO_LIB . '/extensions/widgets_extensions.php');

include_once( HOLO_LIB . '/framework/pagination.php');

include_once( HOLO_LIB . '/framework/functions/hextorgb.php');

global $NHP_Options;
$options = $NHP_Options->options;

function jed_add_website_scripts() {
    wp_enqueue_style('bootstrap-css', HOLO_STYLESHEET . '/styles/bootstrap/bootstrap.min.css');
    wp_enqueue_style('magnific-popup-css', HOLO_STYLESHEET . '/scripts/plugins/magnific-popup/magnific-popup.css');
    wp_enqueue_style('main-css', HOLO_STYLESHEET . '/styles/jednotka_green.css');
    wp_enqueue_style('wp-add', HOLO_STYLESHEET . '/styles/wp_add.css');
    wp_enqueue_style('style', get_stylesheet_uri());

    wp_enqueue_script('jquery');
    wp_enqueue_script('jquery-mobile-js', HOLO_STYLESHEET . '/scripts/jquery/jquery.mobile.custom.min.js', array(), false, true);
    wp_enqueue_script('bootstrap-js', HOLO_STYLESHEET . '/scripts/bootstrap/bootstrap.min.js', array(), false, true);
    wp_enqueue_script('bootstrap-dropdown-hover-js', HOLO_STYLESHEET . '/scripts/plugins/hover_dropdown/twitter-bootstrap-hover-dropdown.min.js', array(), false, true);
    wp_enqueue_script('modernizr-js', HOLO_STYLESHEET . '/scripts/plugins/modernizr/modernizr.custom.min.js', array(), false, true);
    wp_enqueue_script('retina-js', HOLO_STYLESHEET . '/scripts/plugins/retina/retina.min.js', array(), false, true);
    wp_enqueue_script('knob-js', HOLO_STYLESHEET . '/scripts/plugins/knob/jquery.knob.js', array(), false, true);
    wp_enqueue_script('isotope-js', HOLO_STYLESHEET . '/scripts/plugins/isotope/jquery.isotope.min.js', array(), false, true);
    wp_enqueue_script('sloopy-masonry-js', HOLO_STYLESHEET . '/scripts/plugins/isotope/jquery.isotope.sloppy-masonry.min.js', array(), false, true);
    wp_enqueue_script('validate-js', HOLO_STYLESHEET . '/scripts/plugins/validate/jquery.validate.min.js', array(), false, true);
    wp_enqueue_script('flexslider-js', HOLO_STYLESHEET . '/scripts/plugins/flexslider/jquery.flexslider.min.js', array(), false, true);
    wp_enqueue_script('countdown-js', HOLO_STYLESHEET . '/scripts/plugins/countdown/countdown.js', array(), false, true);
    wp_enqueue_script('nivo-lightbox-js', HOLO_STYLESHEET . '/scripts/plugins/nivo_lightbox/nivo-lightbox.min.js', array(), false, true);
    wp_enqueue_script('cycle-all-js', HOLO_STYLESHEET . '/scripts/plugins/cycle/jquery.cycle.all.min.js', array(), false, true);
    wp_enqueue_script('magnific-popup-js', HOLO_STYLESHEET . '/scripts/plugins/magnific-popup/jquery.magnific-popup.min.js', array(), false, true);

    wp_enqueue_script('jednotka-js', HOLO_STYLESHEET . '/scripts/jednotka.js', array(), false, true);
    wp_enqueue_style('font-awesome-css', HOLO_STYLESHEET . '/styles/font-awesome-4.0.3/css/font-awesome.css');

    wp_enqueue_script('google-map-js', "https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false", array(), false, false);
}

function jed_add_admin_scripts() {
    wp_enqueue_style('font-awesome-css', HOLO_STYLESHEET . '/styles/font-awesome-4.0.3/css/font-awesome.css');
    wp_enqueue_style('admin-css', HOLO_STYLESHEET . '/styles/admin.css');

    wp_enqueue_media();

    wp_register_script('custom-meta-scripts', plugins_url() . '/jednotka-custom-meta-boxes/custom-meta-scripts.js', array('jquery'), false, true);
    wp_localize_script( 'custom-meta-scripts', 'meta_image',
        array(
            'title' => 'Choose or Upload an Image',
            'button' => 'Use this image',
        )
    );
    wp_enqueue_script( 'custom-meta-scripts' );

    wp_enqueue_script('media-upload');
    wp_enqueue_style('thickbox');
    wp_enqueue_script('thickbox');

    wp_enqueue_style( 'wp-color-picker' );
    wp_enqueue_script( 'wp-color-picker' );
}

if ( is_singular() ) wp_enqueue_script( "comment-reply" );

add_action('wp_enqueue_scripts', 'jed_add_website_scripts');
add_action('admin_enqueue_scripts', 'jed_add_admin_scripts');

if (function_exists('register_sidebar')) {
    register_sidebar(array(
        'name' => 'Sidebar Widgets',
        'id'   => 'sidebar-widgets',
        'description'   => 'Sidebar Widgets Area',
        'before_widget' => '<div class="box sidebar-widget">',
        'after_widget'  => '</div>',
        'before_title'  => '<h3 class="title">',
        'after_title'   => '</h2>'
    ));
}

if (function_exists('register_sidebar')) {
    register_sidebar(array(
        'name' => 'Footer Widgets',
        'id'   => 'footer-widgets',
        'description'   => 'Footer Widgets Area',
        'before_widget' => '<div class="col-md-3 col-sm-6 info-box footer-widget">',
        'after_widget'  => '</div>',
        'before_title'  => '<h3 class="title">',
        'after_title'   => '</h2>'
    ));
}

add_theme_support('post-thumbnails');
add_image_size('large-blog-image', 848, 364, true);
add_image_size('small-blog-image', 189, 129, true);
add_image_size('portfolio-2columns', 585, 382, true);
add_image_size('portfolio-3columns', 360, 235, true);
add_image_size('portfolio-4columns', 263, 171, true);
add_image_size('portfolio-square-image', 263, 263, true);
add_image_size('portfolio-smaller-square-image', 262, 262, true);
add_image_size('gallery-thumbnail', 165, 165, true);

/**
 * Checks if a particular user has a role.
 * Returns true if a match was found.
 *
 * @param string $role Role name.
 * @param int $user_id (Optional) The ID of a user. Defaults to the current user.
 * @return bool
 */
function holo_check_user_role( $role, $user_id = null ) {

    if ( is_numeric( $user_id ) )
        $user = get_userdata( $user_id );
    else
        $user = wp_get_current_user();

    if ( empty( $user ) )
        return false;

    return in_array( $role, (array) $user->roles );
}

add_filter('get_avatar','jed_change_avatar_css');

function jed_change_avatar_css($class) {
    $class = str_replace("class='avatar", "class='img-circle img-responsive center-block ", $class) ;
    return $class;
}

// redirects the user to the theme options
add_action('after_switch_theme', 'jednotka_activation_function', 10, 2);

function jednotka_activation_function() {
    wp_redirect('admin.php?page=jednotka_options');
}

update_option('posts_per_page', $options['posts-number']);

class Excerpt {

    // Default length (by WordPress)
    public static $length = 55;

    // So you can call: my_excerpt('short');
    public static $types = array(
        'short' => 25,
        'regular' => 55,
        'long' => 100
    );

    /**
     * Sets the length for the excerpt,
     * then it adds the WP filter
     * And automatically calls the_excerpt();
     *
     * @param string $new_length
     * @return void
     * @author Baylor Rae'
     */
    public static function length($new_length = 55) {
        Excerpt::$length = $new_length;

        add_filter('excerpt_length', 'Excerpt::new_length', 999);

        Excerpt::output();
    }

    // Tells WP the new length
    public static function new_length() {
        if( isset(Excerpt::$types[Excerpt::$length]) )
            return Excerpt::$types[Excerpt::$length];
        else
            return Excerpt::$length;
    }

    // Echoes out the excerpt
    public static function output() {
        the_excerpt();
    }

}

// An alias to the class
function custom_excerpt($length = 55) {
    Excerpt::length($length);
}

add_theme_support( 'automatic-feed-links' );
add_theme_support( 'post-formats', array( 'video', 'gallery', 'link' ) );
add_post_type_support('portfolio', 'post-formats');

function thickbox_fields($form_fields, $post){
    unset(
    $form_fields['image-size']
    );

    $size = 'full';
    $css_id = "image-size-{$size}-{$post->ID}";
    $html = "<div style='display: none;' class='image-size-item'><input type='radio' name='attachments[$post->ID][image-size]' id='{$css_id}' value='{$size}'checked='checked' /></div>";
    //set as image-size field
    $form_fields['image-size'] = array(
        'label' => '', //leave with no label
        'input' => 'html', //input type is html
        'html' => $html //the actual html
    );

    return $form_fields;
}

function thickbox_upload_init(){
    add_filter('attachment_fields_to_edit', 'thickbox_fields', 10, 2);
}

add_action('admin_init', 'thickbox_upload_init');

/*
* Function  my_page_template_redirect
*
* To redirect our site url by coming soon page,after redirecting only our coming soon page can display
*
*/
add_action( 'template_redirect', 'jed_template_redirect' );
function jed_template_redirect()
{
    global $NHP_Options;
    $options = $NHP_Options->options;

    if ( isset($options['show-coming-soon']) && $options['show-coming-soon']) {
        //condition matching if status is not disable for coming soon then page is redirect
        if(!is_feed())
        {
            //if user not login page is redirect on coming soon template page
            if ( !is_user_logged_in()  )
            {
                //get path of our coming soon display page and redirecting

                include_once( get_template_directory() . '/coming_soon.php' );

                exit();
            }
        }
        //if user is log-in then we check role.
        if (is_user_logged_in() )
        {
            //get logined in user role
            global $current_user;
            get_currentuserinfo();
            $LoggedInUserID = $current_user->ID;
            $UserData = get_userdata( $LoggedInUserID );
            //if user role not 'administrator' redirect him to message page
            if($UserData->roles[0] != "administrator")
            {
                if(!is_feed())
                {
                    include_once( get_template_directory() . '/coming_soon.php' );

                    exit();
                }
            }
        }
    }

    if ( isset($options['show-under-construction']) && $options['show-under-construction']) {
        //condition matching if status is not disable for coming soon then page is redirect
        if(!is_feed())
        {
            //if user not login page is redirect on coming soon template page
            if ( !is_user_logged_in()  )
            {
                //get path of our coming soon display page and redirecting

                include_once( get_template_directory() . '/under_construction.php' );

                exit();
            }
        }
        //if user is log-in then we check role.
        if (is_user_logged_in() )
        {
            //get logined in user role
            global $current_user;
            get_currentuserinfo();
            $LoggedInUserID = $current_user->ID;
            $UserData = get_userdata( $LoggedInUserID );
            //if user role not 'administrator' redirect him to message page
            if($UserData->roles[0] != "administrator")
            {
                if(!is_feed())
                {
                    include_once( get_template_directory() . '/under_construction.php' );

                    exit();
                }
            }
        }
    }

}

function new_excerpt_more( $more ) {
    return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');

