<?php

global $NHP_Options;
$options = $NHP_Options->options;

$logo_width = 117;
$logo_height = 39;

if ( !empty($options['logo-width']) ) {
    $logo_width = (int)$options['logo-width'];
}

if (!empty($options['logo-height']) ) {
    $logo_height = (int)$options['logo-height'];
}

?>

<!DOCTYPE html>
<!--[if lt IE 9]> <html class="lt-ie9" lang="en"> <![endif]-->
<!--[if !IE] -->
<html lang='en'>
<!-- <![endif] -->
<head>
    <meta content='blog, business, clean, multipurpose template, twitter bootstrap 3, responsive' name='keywords'>
    <meta content='Jednotka is multipurpose Twitter Bootstrap 3 template which is suitable for any type of business or online project. ' name='description'>
    <meta content='BublinaStudio.com' name='author'>
    <meta content='all' name='robots'>
    <meta content='text/html; charset=utf-8' http-equiv='Content-Type'>
    <meta content='width=device-width, initial-scale=1.0' name='viewport'>
    <!--[if IE]> <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'> <![endif]-->
    <link href='assets/images/meta_icons/favicon.ico' rel='shortcut icon' type='image/x-icon'>
    <!-- / required stylesheets -->

    <link href='<?php echo $options['favicon'] ?>' rel='shortcut icon' type='image/x-icon'>

    <title><?php echo get_bloginfo('title') . wp_title( '|', false, 'left' ) ?></title>

    <?php wp_head(); ?>
</head>
<body class='error-page' style="background: url(<?php echo $options['background-image'] ?>) fixed center center / cover no-repeat">

<?php get_template_part('styles/custom_styles') ?>

<div id='wrapper'>
    <div class='container'>
        <div class='row'>
            <div class='col-sm-12'>
                <h1 class='code fadeInDown animated'>
                    <span>4</span>
                    <span class='fa-icon-question-sign'></span>
                    <span>4</span>
                </h1>
            </div>
        </div>
        <div class='row'>
            <div class='col-sm-12'>
                <p class='message fadeInLeft animated'>
                    <?php _e('Sorry, the page you are looking for doesn’t exist.', THEME_TEXT_DOMAIN) ?>
                    <br>
                    <?php _e('Take a run around the block or tap the button below.', THEME_TEXT_DOMAIN) ?>
                </p>
                <a class="btn btn-bordered btn-white btn-lg fadeInRight animated button404" href="<?php echo home_url() ?>"><?php _e('Go to homepage', THEME_TEXT_DOMAIN) ?></a>
            </div>
        </div>
        <div class='row'>
            <div class='col-sm-12'>
                <a class="logo-link" href="<?php echo get_home_url() ?>">
                    <?php if ( !empty($options['logo']) ) { ?>

                        <img class="logo fadeInUp animated" src="<?php echo $options['logo'] ?>" width="<?php echo $logo_width  ?>" height="<?php echo $logo_height ?>" />


                    <?php } else {

                        echo '<span class="logo fadeInUp animated">' . $options['logo-text'] . '</span>';

                    } ?>
                </a>
            </div>
        </div>
    </div>
</div>

<?php wp_footer(); ?>

</body>
</html>
