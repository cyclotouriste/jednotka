(function($) {

    $(document).ready(function() {

        var meta_image_frame;

        $('#slide-background').click(function(e){

//        tb_show('', 'media-upload.php?post_id=<?php  echo $post->ID; ?>&type=image&amp;TB_iframe=true');

            // Prevents the default action from occuring.
            e.preventDefault();

            // If the frame already exists, re-open it.
            if ( meta_image_frame ) {
                meta_image_frame.open();
                return;
            }

            // Sets up the media library frame
            meta_image_frame = wp.media.frames.meta_image_frame = wp.media({
                title: meta_image.title,
                button: { text:  meta_image.button },
                library: { type: 'image' }
            });

            // Runs when an image is selected.
            meta_image_frame.on('select', function(){

                // Grabs the attachment selection and creates a JSON representation of the model.
                var media_attachment = meta_image_frame.state().get('selection').first().toJSON();

                // Sends the attachment URL to our custom image input field.
                $('#element-uploaded-image-container').html('<img src="' + media_attachment.url + '" width="500" />');
                $('#element-uploaded-image-input-container').html('<input type="hidden" value="' + media_attachment.url + '" name="portfolio_image" />');
            });

            // Opens the media library frame.
            meta_image_frame.open();
        });

        $('#slide-image').click(function(e){

//        tb_show('', 'media-upload.php?post_id=<?php  echo $post->ID; ?>&type=image&amp;TB_iframe=true');

            // Prevents the default action from occuring.
            e.preventDefault();

            // If the frame already exists, re-open it.
            if ( meta_image_frame ) {
                meta_image_frame.open();
                return;
            }

            // Sets up the media library frame
            meta_image_frame = wp.media.frames.meta_image_frame = wp.media({
                title: meta_image.title,
                button: { text:  meta_image.button },
                library: { type: 'image' }
            });

            // Runs when an image is selected.
            meta_image_frame.on('select', function(){

                // Grabs the attachment selection and creates a JSON representation of the model.
                var media_attachment = meta_image_frame.state().get('selection').first().toJSON();

                // Sends the attachment URL to our custom image input field.
                $('#element-uploaded-image-container').html('<img src="' + media_attachment.url + '" width="500" />');
                $('#element-uploaded-image-input-container').html('<input type="hidden" value="' + media_attachment.url + '" name="portfolio_image" />');
            });

            // Opens the media library frame.
            meta_image_frame.open();
        });
    })

}(jQuery));