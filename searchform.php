<style>
    #searchform i {
        position: absolute;
        top: 0;
        right: 0;
        line-height: 40px;
        width: 42px;
        color: #fff;
        z-index: 1;
    }

    #searchform .button-wrapper .btn {
        position: relative;
        top: 0;
        right: 0;
        width: 42px;
        height: 40px;
        color: #fff;
        z-index: 2;
        border: none;
        padding: 0;
    }

    #searchform .input-group-btn {
        position: relative;
        text-align: center;
    }

    #searchform .button-wrapper {
        border-radius: 0 5px 5px 0;
    }
</style>

<form id="searchform" action="<?php echo home_url( '/' ); ?>" method="get" role="search" >
    <div class='input-group'>
        <input type="text" class='form-control' name="s" id="s" placeholder="<?php _e('Search', THEME_TEXT_DOMAIN) ?>..." value="" />
          <span class='input-group-btn'>
              <div class="button-wrapper">
                  <input class="btn" type="submit" id="searchsubmit" value="" />
                  <i class='fa-icon-search'></i>
              </div>
          </span>
    </div>
</form>