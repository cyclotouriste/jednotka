<?php
/**
 * Plugin name: Jednotka Custom Post Types
 * Description: The custom post types needed for Jednotka Wordpress Theme
 * Author: Holobest
 * Version: 1.0.0
 * Author URI: http://holobest.com
 */

require_once('jednotka-custom-taxonomies.php');

add_action('init', 'jednotka_register_portfolio');

/**
 *  Registers the Portfolio custom post type
 */
function jednotka_register_portfolio() {

    $labels = array(
        'name'               => _x( 'Portfolio', 'post type general name' ),
        'singular_name'      => _x( 'Portfolio', 'post type singular name' ),
        'add_new'            => _x( 'Add New', 'new portfolio item' ),
        'add_new_item'       => __( 'Add New Portfolio Item' ),
        'edit_item'          => __( 'Edit Portfolio Item' ),
        'new_item'           => __( 'New Portfolio Item' ),
        'all_items'          => __( 'All Portfolio Items' ),
        'view_item'          => __( 'View Portfolio Item' ),
        'search_items'       => __( 'Search Portfolio Items' ),
        'not_found'          => __( 'No Portfolio Items found' ),
        'not_found_in_trash' => __( 'No Portfolio Items found in the Trash' ),
        'parent_item_colon'  => '',
        'menu_name'          => 'Portfolio'
    );

    $args = array(
        'labels' => $labels,
        'description' => 'Holds portfolio specific data',
        'show_ui' => true,
        'public' => true,
        'menu_position' => 5,
        'supports' => array('title', 'editor', 'thumbnail', 'comments'),
        'has_archive' => true,
    );

    register_post_type('portfolio', $args);

}

add_action('init', 'jednotka_register_slides');

function jednotka_register_slides() {
    $labels = array(
        'name'               => _x( 'Slides', 'post type general name' ),
        'singular_name'      => _x( 'Slide', 'post type singular name' ),
        'add_new'            => _x( 'Add New', 'new portfolio item' ),
        'add_new_item'       => __( 'Add New Slide' ),
        'edit_item'          => __( 'Edit Slide' ),
        'new_item'           => __( 'New Slide' ),
        'all_items'          => __( 'All Slides' ),
        'view_item'          => __( 'View Slide' ),
        'search_items'       => __( 'Search Slides' ),
        'not_found'          => __( 'No Slides found' ),
        'not_found_in_trash' => __( 'No Slides found in the Trash' ),
        'parent_item_colon'  => '',
        'menu_name'          => 'Slides'
    );

    $args = array(
        'labels' => $labels,
        'description' => 'Holds slides specific data',
        'show_ui' => true,
        'public' => true,
        'menu_position' => 5,
        'supports' => array('title'),
        'has_archive' => false,
    );

    register_post_type('slides', $args);
}
