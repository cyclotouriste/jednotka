<?php

add_action ('init', 'jednotka_register_portfolio_category_taxonomy');

function jednotka_register_portfolio_category_taxonomy() {

    $args = array(
        'hierarchical' => true,
        'labels' => array(
            'name' => _x( 'Portfolio Categories', 'taxonomy general name' ),
            'singular_name' => _x( 'Category', 'taxonomy singular name' ),
            'search_items' =>  __( 'Search Categories' ),
            'all_items' => __( 'All Categories' ),
            'parent_item' => __( 'Parent Category' ),
            'parent_item_colon' => __( 'Parent Category:' ),
            'edit_item' => __( 'Edit Category' ),
            'update_item' => __( 'Update Category' ),
            'add_new_item' => __( 'Add New Category' ),
            'new_item_name' => __( 'New Category Name' ),
            'menu_name' => __( 'Categories' ),
        ),
        'rewrite' => array(
            'slug' => 'portfolio_category', // This controls the base slug that will display before each term
            'with_front' => true, // Don't display the category base before "/locations/"
            'hierarchical' => true // This will allow URL's like "/locations/boston/cambridge/"
        ),

    );

    register_taxonomy('portfolio_category', array('portfolio'), $args);

}