(function($) {

$(document).ready(function() {

    var original_send_to_editor = window.send_to_editor;

    jQuery(document).on('click', '.image-gallery-upload',
        function()
        {
            parentContainer = jQuery(this).parent('td');

            tb_show('Upload Media', 'media-upload.php?type=image&amp;TB_iframe=true');

            // overrides the send_to_editor() function in media-upload script.
            window.send_to_editor = function(html) {
                imgurl = jQuery('img',html).attr('src');
                parentContainer.find('.gallery-image-holder').html('<img src="' + imgurl + '" width="150" />');
                parentContainer.find('.image-gallery-input').val(imgurl);
                parentContainer.find('button').after('<a class="remove-image" href="javascript:;">Remove Image</a>');
                parentContainer.find('button').remove();
                tb_remove();

                //restore old send to editor function
                window.send_to_editor = original_send_to_editor;
            };

            return false;
        }
    );

    jQuery('#add-new-gallery-image-button').on('click', function() {

        var galleryLength = jQuery(this).attr('data-gallery-length');

        var html =
            '<tr>' +
                '<td><label for="post-gallery">Image</label></td>' +
                '<td>' +
                '<div class="gallery-image-holder"></div>' +

                '<button class="image-gallery-upload">Upload</button>' +
                '<input class="image-gallery-input" type="hidden" name="post_gallery_image[' + parseInt(galleryLength) + ']" />' +
                '</td>' +
            '</tr>';

        jQuery('#post-gallery-container table').append(html);

        jQuery(this).attr('data-gallery-length', parseInt(galleryLength) + 1)

        return false;
    });

    jQuery('#add-new-portfolio-gallery-image-button').on('click', function() {

        var galleryLength = jQuery(this).attr('data-gallery-length');

        var html =
            '<tr>' +
                '<td><label for="post-gallery">Image</label></td>' +
                '<td>' +
                '<div class="gallery-image-holder"></div>' +

                '<button class="image-gallery-upload">Upload</button>' +
                '<input class="image-gallery-input" type="hidden" name="portfolio_gallery_image[' + parseInt(galleryLength) + ']" />' +
                '</td>' +
            '</tr>';

        jQuery('#post-gallery-container table').append(html);

        jQuery(this).attr('data-gallery-length', parseInt(galleryLength) + 1)

        return false;
    });

    jQuery('#post-gallery-container').on('click', '.remove-image', function() {
        var parent = jQuery(this).parents('tr');

        var input = parent.find('input[type="hidden"]');

        input.val('');

        parent.after(input);

        parent.remove();

        return false;
    });

    if (jQuery('#post-format-video').is(':checked')) {
        jQuery('#post_video').css('display', 'block');
        jQuery('#portfolio_video').css('display', 'block');
    }

    if (jQuery('#post-format-gallery').is(':checked')) {
        jQuery('#post_gallery').css('display', 'block');
        jQuery('#portfolio_gallery').css('display', 'block');
    }

    jQuery('.post-format').change(function() {
        jQuery('#post_video').css('display', 'none');
        jQuery('#post_gallery').css('display', 'none');

        jQuery('#portfolio_gallery').css('display', 'none');
        jQuery('#portfolio_video').css('display', 'none');

        if (jQuery(this).val() === 'video') {
            jQuery('#post_video').css('display', 'block');
            jQuery('#portfolio_video').css('display', 'block');
        }
        else if (jQuery(this).val() === 'gallery') {
            jQuery('#post_gallery').css('display', 'block');
            jQuery('#portfolio_gallery').css('display', 'block');
        }
    });

    var meta_image_frame;

    $('#slide-background').click(function(e) {

        tb_show('Upload Media', 'media-upload.php?type=image&amp;TB_iframe=true');

        // overrides the send_to_editor() function in media-upload script.
        window.send_to_editor = function(html) {
            imgurl = jQuery('img',html).attr('src');

            jQuery('#uploaded-slide-background-container').children('img').remove();
            jQuery('#uploaded-slide-background-container').append('<img src="' + imgurl + '" width="500" />');
            jQuery('#uploaded-background-image-input-container').append('<input type="hidden" name="slide_background" value="' + imgurl + '">');

            tb_remove();
        }

    });


    $('#slide-image').click(function(e) {

        tb_show('Upload Media', 'media-upload.php?type=image&amp;TB_iframe=true');

        // overrides the send_to_editor() function in media-upload script.
        window.send_to_editor = function(html) {
            imgurl = jQuery('img',html).attr('src');

            jQuery('#uploaded-slide-image-container').children('img').remove();
            jQuery('#uploaded-slide-image-container').append('<img src="' + imgurl + '" width="500" />');
            jQuery('#uploaded-slide-image-input-container').append('<input type="hidden" name="slide_image" value="' + imgurl + '">');

            tb_remove();
        }

    });
})

}(jQuery))
