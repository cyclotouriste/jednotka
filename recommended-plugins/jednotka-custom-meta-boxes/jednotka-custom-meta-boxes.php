<?php
/**
 * Plugin name: Jednotka custom meta boxes
 * Description: Custom meta boxes needed for Jednotka Wordpress Theme
 * Author: Holobest
 * Version: 1.0.0
 * Author URI: http://holobest.com
 */

// create post video meta box
add_action('admin_init', 'jed_portfolio_sidebar_meta_box');

function jed_portfolio_sidebar_meta_box() {
    add_meta_box(
        'portfolio_sidebar',
        'Portfolio Sidebar',
        'jed_register_portfolio_sidebar',
        'portfolio',
        'normal',
        'high'
    );
}

function jed_register_portfolio_sidebar($post) {
    $custom = get_post_custom($post->ID);
    $portfolio_sidebar = '';
    $portfolio_sidebar_title = '';

    if ( !empty($custom) ) {
        if ( isset ($custom['portfolio_sidebar']) ) {
            $portfolio_sidebar = $custom['portfolio_sidebar'][0];
            $portfolio_sidebar_title = $custom['portfolio_sidebar_title'][0];
        }
    }
    ?>

    <div class="meta-fields-container">
        <p><i></i></p>

        <table>
            <tr>
                <td><label for="portfolio-sidebar-title">Portfolio Sidebar Title</label></td>
                <td>
                    <?php wp_nonce_field(plugin_basename(__FILE__), 'portfolio_sidebar_title_nonce') ?>

                    <input type="text" id="portfolio-sidebar-title" name="portfolio_sidebar_title" value="<?php echo $portfolio_sidebar_title; ?>" />
                </td>
            </tr>

            <tr>
                <td><label for="portfolio-sidebar">Portfolio Sidebar</label></td>
                <td>
                    <?php wp_nonce_field(plugin_basename(__FILE__), 'portfolio_sidebar_nonce') ?>

                    <textarea id="portfolio-sidebar" cols="80" rows="8" name="portfolio_sidebar"><?php echo $portfolio_sidebar; ?></textarea>
                </td>
            </tr>
        </table>
    </div>
    <?php
}

add_action('save_post', 'jed_portfolio_sidebar_save');

function jed_portfolio_sidebar_save($post_id) {

    if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) {
        return;
    }

    if ( isset($_POST['portfolio_sidebar']) ) {
        if ( !wp_verify_nonce($_POST['portfolio_sidebar_nonce'], plugin_basename(__FILE__)) && !wp_verify_nonce($_POST['portfolio_sidebar_title_nonce'], plugin_basename(__FILE__))  ) {
            return;
        }

        $portfolio_sidebar = $_POST['portfolio_sidebar'];
        $portfolio_sidebar_title = $_POST['portfolio_sidebar_title'];

        update_post_meta($post_id, 'portfolio_sidebar', $portfolio_sidebar);
        update_post_meta($post_id, 'portfolio_sidebar_title', $portfolio_sidebar_title);
    }

}



// create post video meta box
add_action('admin_init', 'jed_portfolio_video_meta_box');

function jed_portfolio_video_meta_box() {
    add_meta_box(
        'portfolio_video',
        'Portfolio Video',
        'jed_register_portfolio_video',
        'portfolio',
        'normal',
        'high'
    );
}

function jed_register_portfolio_video($post) {
    $custom = get_post_custom($post->ID);
    $portfolio_video = '';

    if ( !empty($custom) ) {
        if ( isset ($custom['portfolio_video']) ) {
            $portfolio_video = $custom['portfolio_video'][0];
        }
    }
    ?>

    <div class="meta-fields-container">
        <p><i>Insert the video iframe for your portfolio.</i></p>

        <table>
            <tr>
                <td><label for="portfolio-video">Portfolio Video</label></td>
                <td>
                    <?php wp_nonce_field(plugin_basename(__FILE__), 'portfolio_video_nonce') ?>

                    <textarea id="portfolio-video" cols="40" rows="4" name="portfolio_video"><?php echo $portfolio_video; ?></textarea>
                </td>
            </tr>
        </table>
    </div>

<?php
}

add_action('save_post', 'jed_portfolio_video_save');

function jed_portfolio_video_save($post_id) {

    if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) {
        return;
    }

    if ( isset($_POST['portfolio_video']) ) {
        if ( !wp_verify_nonce($_POST['portfolio_video_nonce'], plugin_basename(__FILE__)) ) {
            return;
        }

        $portfolio_video = $_POST['portfolio_video'];

        update_post_meta($post_id, 'portfolio_video', $portfolio_video);
    }

}

// create post gallery meta box
add_action('admin_init', 'jed_portfolio_gallery_meta_box');

function jed_portfolio_gallery_meta_box() {
    add_meta_box(
        'portfolio_gallery',
        'Portfolio Gallery',
        'jed_register_portfolio_gallery',
        'portfolio',
        'normal',
        'high'
    );
}

function jed_register_portfolio_gallery($post) {
    wp_enqueue_script('custom-meta-scripts', plugins_url() . '/stylish-custom-meta-boxes/custom-meta-scripts.js', array('jquery'));

    $custom = get_post_custom($post->ID);
    $portfolio_gallery = '';

    if ( !empty($custom) ) {
        if ( isset ($custom['portfolio_gallery_image']) ) {
            $portfolio_gallery = $custom['portfolio_gallery_image'][0];
            $portfolio_gallery = explode(',', $portfolio_gallery);
        }
    }
    ?>

    <div class="meta-fields-container" id="post-gallery-container">
        <p><i>Choose images for your portfolio gallery.</i></p>

        <?php wp_nonce_field(plugin_basename(__FILE__), 'portfolio_gallery_nonce') ?>

        <table>

            <?php
            for ($i = 0; $i < count($portfolio_gallery); $i++) {
                ?>

                <tr>
                    <td><label for="portfolio-gallery">Image</label></td>
                    <td>
                        <div class="gallery-image-holder">
                            <img src="<?php echo $portfolio_gallery[$i] ?>" width="150" />
                        </div>

                        <?php
                        if ( !empty($portfolio_gallery[$i]) ) {
                            ?>

                            <a class="remove-image" href="javascript:;">Remove Image</a>

                        <?php
                        }
                        else {
                            ?>

                            <button class="image-gallery-upload">Upload</button>

                        <?php
                        }
                        ?>

                        <input class="image-gallery-input" type="hidden" name="portfolio_gallery_image[<?php echo $i ?>]" value="<?php echo $portfolio_gallery[$i] ?>" />
                    </td>
                </tr>

            <?php
            }
            ?>

        </table>

        <div class="admin-stylish-button add-new-button" style="margin: 20px 0 0 0; position: static; float: none">
            <button id="add-new-portfolio-gallery-image-button" data-gallery-length="<?php echo count($portfolio_gallery) ?>">Add new image</button>
        </div>
    </div>

<?php
}

add_action('save_post', 'jed_portfolio_gallery_save');

function jed_portfolio_gallery_save($post_id) {

    if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) {
        return;
    }

    if ( isset($_POST['portfolio_gallery_image']) ) {
        if ( !wp_verify_nonce($_POST['portfolio_gallery_nonce'], plugin_basename(__FILE__)) ) {
            return;
        }

        $images = $_POST['portfolio_gallery_image'];

        $index = 0;
        foreach($images as $image) {
            if ( empty($image) ) {
                unset($images[$index]);
            }

            $index++;
        }

        $portfolio_gallery = implode(',', $images);

        update_post_meta($post_id, 'portfolio_gallery_image', $portfolio_gallery);
    }

}

// create post video meta box
add_action('admin_init', 'jed_post_video_meta_box');

function jed_post_video_meta_box() {
    add_meta_box(
        'post_video',
        'Post Video',
        'jed_register_post_video',
        'post',
        'normal',
        'high'
    );
}

function jed_register_post_video($post) {
    $custom = get_post_custom($post->ID);
    $post_video = '';

    if ( !empty($custom) ) {
        if ( isset ($custom['post_video']) ) {
            $post_video = $custom['post_video'][0];
        }
    }
    ?>

    <div class="meta-fields-container">
        <p><i>Insert the video iframe for your post.</i></p>

        <table>
            <tr>
                <td><label for="post-video">Post Video</label></td>
                <td>
                    <?php wp_nonce_field(plugin_basename(__FILE__), 'post_video_nonce') ?>

                    <textarea id="post-video" cols="40" rows="4" name="post_video"><?php echo $post_video; ?></textarea>
                </td>
            </tr>
        </table>
    </div>

<?php
}

add_action('save_post', 'jed_post_video_save');

function jed_post_video_save($post_id) {

    if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) {
        return;
    }

    if ( isset($_POST['post_video']) ) {
        if ( !wp_verify_nonce($_POST['post_video_nonce'], plugin_basename(__FILE__)) ) {
            return;
        }

        $post_video = $_POST['post_video'];

        update_post_meta($post_id, 'post_video', $post_video);
    }

}

// create post gallery meta box
add_action('admin_init', 'jed_post_gallery_meta_box');

function jed_post_gallery_meta_box() {
    add_meta_box(
        'post_gallery',
        'Post Gallery',
        'jed_register_post_gallery',
        'post',
        'normal',
        'high'
    );
}

function jed_register_post_gallery($post) {
    wp_enqueue_script('custom-meta-scripts', plugins_url() . '/jednotka-custom-meta-boxes/custom-meta-scripts.js', array('jquery'));

    $custom = get_post_custom($post->ID);
    $post_gallery = '';

    if ( !empty($custom) ) {
        if ( isset ($custom['post_gallery_image']) ) {
            $post_gallery = $custom['post_gallery_image'][0];
            $post_gallery = explode(',', $post_gallery);
        }
    }
    ?>

    <div class="meta-fields-container" id="post-gallery-container">
        <p><i>Choose images for your post gallery.</i></p>

        <?php wp_nonce_field(plugin_basename(__FILE__), 'post_gallery_nonce') ?>

        <table>

            <?php
            for ($i = 0; $i < count($post_gallery); $i++) {
                ?>

                <tr>
                    <td><label for="post-gallery">Image</label></td>
                    <td>
                        <div class="gallery-image-holder">
                            <img src="<?php echo $post_gallery[$i] ?>" width="150" />
                        </div>

                        <?php
                        if ( !empty($post_gallery[$i]) ) {
                            ?>

                            <a class="remove-image" href="javascript:;">Remove Image</a>

                        <?php
                        }
                        else {
                            ?>

                            <button class="image-gallery-upload">Upload</button>

                        <?php
                        }
                        ?>

                        <input class="image-gallery-input" type="hidden" name="post_gallery_image[<?php echo $i ?>]" value="<?php echo $post_gallery[$i] ?>" />
                    </td>
                </tr>

            <?php
            }
            ?>

        </table>

        <div class="admin-stylish-button add-new-button" style="margin: 20px 0 0 0; position: static; float: none">
            <button id="add-new-gallery-image-button" data-gallery-length="<?php echo count($post_gallery) ?>">Add new image</button>
        </div>
    </div>

<?php
}

add_action('save_post', 'jed_post_gallery_save');

function jed_post_gallery_save($post_id) {

    if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) {
        return;
    }

    if ( isset($_POST['post_gallery_image']) ) {
        if ( !wp_verify_nonce($_POST['post_gallery_nonce'], plugin_basename(__FILE__)) ) {
            return;
        }

        $images = $_POST['post_gallery_image'];

        $index = 0;
        foreach($images as $image) {
            if ( empty($image) ) {
                unset($images[$index]);
            }

            $index++;
        }

        $post_video = implode(',', $images);

        update_post_meta($post_id, 'post_gallery_image', $post_video);
    }

}

/* Add Page Description in page*/
add_action('admin_init', 'jednotka_create_page_description_meta_box');

function jednotka_create_page_description_meta_box() {
    add_meta_box(
        'page-description',
        'Page Settings',
        'jednotka_register_page_settings',
        'page',
        'normal',
        'high'
    );
}

function jednotka_register_page_settings($post) {
    if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) {
        return;
    }

    $page_description = '';

    $custom = get_post_custom($post->ID);

    if (!empty($custom)) {
        if ( isset($custom['page_description']) ) {
            $page_description = $custom['page_description'][0];
        }
    }

    ?>

    <div class="meta-fields-container">
        <table>
            <tr>
                <td><label for="page-description">Page Description</label></td>
                <td>
                    <?php wp_nonce_field( plugin_basename(__FILE__), 'page_description_nonce'); ?>

                    <input type="text" id="page-description" name="page_description" value="<?php echo $page_description ?>" />

                </td>
            </tr>
        </table>
    </div>

    <?php
}

add_action('save_post', 'jednotka_save_page_description');

function jednotka_save_page_description($post_id) {

    if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) {
        return;
    }

    if ( isset($_POST['post_type']) ) {
        if ($_POST['post_type'] == 'page') {
            if ( !current_user_can('edit_page', $post_id) ) {
                return;
            }
            else {
                if ( !wp_verify_nonce($_POST['page_description_nonce'], plugin_basename(__FILE__)) ) {
                    return;
                }
            }
        }

        $page_description = $_POST['page_description'];

        update_post_meta($post_id, 'page_description', $page_description);
    }

}

/* Add Slide Captions */
add_action('admin_init', 'jednotka_create_slide_captions');

function jednotka_create_slide_captions() {
    add_meta_box(
        'slide-captions',
        'Slide Captions',
        'jednotka_register_slide_captions',
        'slides',
        'normal',
        'high'
    );
}

function jednotka_register_slide_captions($post) {

    $first_caption = '';
    $second_caption = '';
    $first_caption_animation = '';
    $second_caption_animation = '';
    $button_animation = '';

    $custom = get_post_custom($post->ID);

    if ( !empty($custom) ) {
        $first_caption = $custom['first_caption'][0];
        $second_caption = $custom['second_caption'][0];
        $first_caption_animation = $custom['first_caption_animation'][0];
        $second_caption_animation = $custom['second_caption_animation'][0];
        $button_animation = $custom['button_animation'][0];
    }

    ?>

    <?php wp_nonce_field(plugin_basename(__FILE__), 'slide_captions_nonce'); ?>

    <div class="meta-fields-container">
        <table>
            <tr>
                <td><label for="first-caption">First Caption</label></td>
                <td>
                    <input type="text" id="first-caption" name="first_caption" value="<?php echo $first_caption ?>" />
                </td>
            </tr>

            <tr>
                <td><label for="first-caption-animation">First Caption Animation</label></td>
                <td>
                    <select id="first-caption-animation" name="first_caption_animation">
                        <option value="">No Animation</option>
                        <option value="fadeInUpBig" <?php echo ($first_caption_animation == 'fadeInUpBig' ? 'selected=selected' : ''); ?>>Slide From Bottom</option>
                        <option value="fadeInDownBig" <?php echo ($first_caption_animation == 'fadeInDownBig' ? 'selected=selected' : ''); ?>>Slide From Top</option>
                        <option value="fadeInLeftBig" <?php echo ($first_caption_animation == 'fadeInLeftBig' ? 'selected=selected' : ''); ?>>Slide From Left</option>
                        <option value="fadeInRightBig" <?php echo ($first_caption_animation == 'fadeInRightBig' ? 'selected=selected' : ''); ?>>Slide From Right</option>
                        <option value="fadeInUp" <?php echo ($first_caption_animation == 'fadeInUp' ? 'selected=selected' : ''); ?>>Fade In From Bottom</option>
                        <option value="fadeInDown" <?php echo ($first_caption_animation == 'fadeInDown' ? 'selected=selected' : ''); ?>>Fade In From Top</option>
                        <option value="fadeInLeft" <?php echo ($first_caption_animation == 'fadeInLeft' ? 'selected=selected' : ''); ?>>Fade In From Left</option>
                        <option value="fadeInRight" <?php echo ($first_caption_animation == 'fadeInRight' ? 'selected=selected' : ''); ?>>Fade In From Right</option>
                    </select>
                </td>
            </tr>

            <tr>
                <td><label for="second-caption">Second Caption</label></td>
                <td>
                    <input type="text" id="second-caption" name="second-caption" value="<?php echo $second_caption ?>" />
                </td>
            </tr>

            <tr>
                <td><label for="second-caption-animation">Second Caption Animation</label></td>
                <td>
                    <select id="second-caption-animation" name="second_caption_animation">
                        <option value="">No Animation</option>
                        <option value="fadeInUpBig" <?php echo ($second_caption_animation == 'fadeInUpBig' ? 'selected=selected' : ''); ?>>Slide From Bottom</option>
                        <option value="fadeInDownBig" <?php echo ($second_caption_animation == 'fadeInDownBig' ? 'selected=selected' : ''); ?>>Slide From Top</option>
                        <option value="fadeInLeftBig" <?php echo ($second_caption_animation == 'fadeInLeftBig' ? 'selected=selected' : ''); ?>>Slide From Left</option>
                        <option value="fadeInRightBig" <?php echo ($second_caption_animation == 'fadeInRightBig' ? 'selected=selected' : ''); ?>>Slide From Right</option>
                        <option value="fadeInUp" <?php echo ($second_caption_animation == 'fadeInUp' ? 'selected=selected' : ''); ?>>Fade In From Bottom</option>
                        <option value="fadeInDown" <?php echo ($second_caption_animation == 'fadeInDown' ? 'selected=selected' : ''); ?>>Fade In From Top</option>
                        <option value="fadeInLeft" <?php echo ($second_caption_animation == 'fadeInLeft' ? 'selected=selected' : ''); ?>>Fade In From Left</option>
                        <option value="fadeInRight" <?php echo ($second_caption_animation == 'fadeInRight' ? 'selected=selected' : ''); ?>>Fade In From Right</option>
                    </select>
                </td>
            </tr>

            <tr>
                <td><label for="button-animation">Button Animation</label></td>
                <td>
                    <select id="button-animation" name="button_animation">
                        <option value="">No Animation</option>
                        <option value="fadeInUpBig" <?php echo ($button_animation == 'fadeInUpBig' ? 'selected=selected' : ''); ?>>Slide From Bottom</option>
                        <option value="fadeInDownBig" <?php echo ($button_animation == 'fadeInDownBig' ? 'selected=selected' : ''); ?>>Slide From Top</option>
                        <option value="fadeInLeftBig" <?php echo ($button_animation == 'fadeInLeftBig' ? 'selected=selected' : ''); ?>>Slide From Left</option>
                        <option value="fadeInRightBig" <?php echo ($button_animation == 'fadeInRightBig' ? 'selected=selected' : ''); ?>>Slide From Right</option>
                        <option value="fadeInUp" <?php echo ($button_animation == 'fadeInUp' ? 'selected=selected' : ''); ?>>Fade In From Bottom</option>
                        <option value="fadeInDown" <?php echo ($button_animation == 'fadeInDown' ? 'selected=selected' : ''); ?>>Fade In From Top</option>
                        <option value="fadeInLeft" <?php echo ($button_animation == 'fadeInLeft' ? 'selected=selected' : ''); ?>>Fade In From Left</option>
                        <option value="fadeInRight" <?php echo ($button_animation == 'fadeInRight' ? 'selected=selected' : ''); ?>>Fade In From Right</option>
                    </select>
                </td>
            </tr>
        </table>
    </div>

    <?php
}

add_action('save_post', 'jednotka_save_slide_captions');

function jednotka_save_slide_captions($post_id) {

    if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) {
        return;
    }

    if ( isset($_POST['post_type']) ) {
        if ( $_POST['post_type'] == 'slides' ) {
            if ( !current_user_can('edit_page', $post_id)) {
                return;
            }
            elseif ( !wp_verify_nonce($_POST['slide_captions_nonce'], plugin_basename(__FILE__)) ) {
                return;
            }
        }

        $first_caption = $_POST['first_caption'];
        $second_caption = $_POST['second-caption'];

        $first_caption_animation = $_POST['first_caption_animation'];
        $second_caption_animation = $_POST['second_caption_animation'];
        $button_animation = $_POST['button_animation'];

        update_post_meta($post_id, 'first_caption', $first_caption);
        update_post_meta($post_id, 'second_caption', $second_caption);
        update_post_meta($post_id, 'first_caption_animation', $first_caption_animation);
        update_post_meta($post_id, 'second_caption_animation', $second_caption_animation);
        update_post_meta($post_id, 'button_animation', $button_animation);
    }

}

/* Create Slide Button */
add_action('admin_init', 'jednotka_create_slide_button');

function jednotka_create_slide_button() {
    add_meta_box(
        'slide_button',
        'Slide Button',
        'jednotka_register_slide_button',
        'slides',
        'normal',
        'high'
    );
}

function jednotka_register_slide_button($post) {

    $show_slide_button = '';
    $button_name = '';
    $button_link = '';
    $slide_background = '';
    $slide_image = '';

    $custom = get_post_custom($post->ID);

    if ( !empty($custom) ) {
        if ( isset($custom['show_slide_button']) ) {
            $show_slide_button = $custom['show_slide_button'][0];
        }

        $button_name = $custom['button_name'][0];
        $button_link = $custom['button_link'][0];
        $slide_background = $custom['slide_background'][0];
        $slide_image = $custom['slide_image'][0];
    }

    ?>

    <?php wp_nonce_field(plugin_basename(__FILE__), 'slide_button_nonce') ?>

    <div class="meta-fields-container">
        <table>
            <tr>
                <td><label for="show-slide-button">Show Slide Button</label></td>
                <td>
                    <input type="checkbox" id="show-slide-button" name="show_slide_button" value="show_button" <?php echo ($show_slide_button == 'show_button' ? 'checked=checked' : '') ?> />
                </td>
            </tr>

            <tr>
                <td><label for="button-name">Button Name</label></td>
                <td>
                    <input type="text" id="button-name" name="button_name" value="<?php echo $button_name ?>" />
                </td>
            </tr>
            <tr>
                <td><label for="button-link">Button Link</label></td>
                <td>
                    <input type="text" id="button-link" name="button_link" value="<?php echo $button_link ?>" />
                </td>
            </tr>
            <tr>
                <td><label for="slide-background">Slide Background</label></td>
                <td>
                    <input type="button" id="slide-background" class="button" value="Choose or Upload an Image" />

                    <div id="uploaded-slide-background-container" class="uploaded-image-container" style="margin: 20px 0">
                        <?php if ( isset($slide_background) ) : ?>

                            <img src="<?php echo $slide_background ?>" width="500" />

                        <?php endif; ?>
                    </div>

                    <div id="uploaded-background-image-input-container" style="display: none">
                        <input type="hidden" value="<?php echo $slide_background ?>" name="slide_background" />
                    </div>
                </td>
            </tr>
            <tr>
                <td><label for="slide-image">Slide Image</label></td>

                <td>
                    <input type="button" id="slide-image" class="button" value="Choose or Upload an Image" />

                    <div id="uploaded-slide-image-container" class="uploaded-image-container" style="margin: 20px 0">
                        <?php if ( isset($slide_image) ) : ?>

                            <img src="<?php echo $slide_image ?>" width="500" />

                        <?php endif; ?>
                    </div>

                    <div id="uploaded-slide-image-input-container" style="display: none">
                        <input type="hidden" value="<?php echo $slide_image ?>" name="slide_image" />
                    </div>
                </td>
            </tr>
        </table>
    </div>

    <?php

}

add_action('save_post', 'jednotka_save_slide_button');

function jednotka_save_slide_button($post_id) {

    if ( isset($_POST['post_type']) ) {
        if ( $_POST['post_type'] == 'slides' ) {
            if ( !current_user_can('edit_page', $post_id) ) {
                return;
            }
            elseif (!wp_verify_nonce($_POST['slide_button_nonce'], plugin_basename(__FILE__)) ) {
                return;
            }
        }



        if ( isset($_POST['show_slide_button']) ) {
            $show_slide_button = $_POST['show_slide_button'];
        }
        else {
            $show_slide_button = '';
        }

        $button_name = $_POST['button_name'];
        $button_link = $_POST['button_link'];
        $slide_background = $_POST['slide_background'];
        $slide_image = $_POST['slide_image'];

        update_post_meta($post_id, 'show_slide_button', $show_slide_button);
        update_post_meta($post_id, 'button_name', $button_name);
        update_post_meta($post_id, 'button_link', $button_link);
        update_post_meta($post_id, 'slide_background', $slide_background);
        update_post_meta($post_id, 'slide_image', $slide_image);

    }

}