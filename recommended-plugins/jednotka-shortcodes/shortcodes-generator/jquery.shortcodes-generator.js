/*
 * jQuery ShortCodesGenerator v1.1.0
 * Copyright 2013 Holobest
 * Contributing Author: Adrian Holobut
 */

(function($) {

    var form = '';
//    var shortcodesSelectOptions = '';

    var shortcodesMarkup = '';
    var shortcodes = [];
    var shortcodesValues = [];
    var options = {};
    var finalOptions = [];
    var colorFields = [];
    var imageFields = [];
    var iconFields = [];

    $.fn.shortcodes = function() {

        generateFormMarkup();

        init();

    };

    /*
    *   Adds shortcode information
    */
    $.fn.shortcodes.add = function(settings) {

        var content = $.extend( {
            name: '',
            id: '',
            itemName: '',
            itemId: null,
            type: 'single',
            childrenTemplate: '',
            options: {/* name of option: { option info }, ... */},
            generatedCode: function() {}
        }, settings);

        shortcodes.push(content);

        shortcodesValues.push(content.id);

        console.log('shortcode: ', content);
    };

    /*
    *
    * Initialize the generator
    *
    * */
    function init() {

        console.log('shortcodes: ', shortcodes);

        var selectOptions = '';

        $.each(shortcodes, function(index, shortcode) {

            selectOptions += '<option value="' + shortcode.id + '">' + shortcode.name + '</option>';

        });

        form = jQuery('\
            <div id="shortcodes-form" class="clearfix">\
		        <button class="mfp-close" type="button" title="Close (Esc)">×</button>\
		        \
		        <div class="shortcodes-select-wrapper">\
                    <select name="shortcodes" id="shortcodes">\
                        <option></option>\
                        ' + selectOptions + '\
                    </select>\
                </div>\
                ' + shortcodesMarkup + '\
                \
                <div class="admin-stylish-button">\
			        <input type="button" id="shortcodes-submit" value="Generate Shortcode" name="submit" />\
		        </div>\
            </div>');

        createForm();

        handleShortcodeSubmit();
    }

    function refresh() {
        $('#shortcodes-form').remove();

        init();
    }

    function generateFormMarkup() {

        $.each(shortcodes, function(key, shortcode) {

            var generalOptions = '';
            var childrenOptions = '';
            var childrenMarkup = '';
            var generalIcons = '';
            var childrenIcons = '';

            $.each(shortcode.options, function(optionKey, option) {

                var optionInput = '';
                var iconInput = '';

                switch (option.type) {
                    case 'text' :
                        optionInput = '<input type="text" class="' + shortcode.id + '-' + optionKey + '" value="" />';
                        break;
                    case 'checkbox' :
                        optionInput = '<input type="checkbox" class="' + shortcode.id + '-' + optionKey + '" value="1" />';
                        break;
                    case 'select' :
                        var options = '';

                        $.each(option.options, function(optionValue, optionLabel) {
                            options += '<option value="' + optionValue + '">' + optionLabel + '</option>';
                        });

                        optionInput =
                            '<select class="' + shortcode.id + '-' + optionKey + '">' +
                                options +
                            '</select>';
                        break;
                    case 'image' :
                        optionInput =
                            '<button class="' + shortcode.id + '-' + optionKey + '-upload">Upload image</button><br />' +
                            '<div class="' + shortcode.id + '-' + optionKey + '-holder"></div>' +
                            '<input type="hidden" class="' + shortcode.id + '-' + optionKey + '" value="" />';

                        var imageFieldObject = {};

                        imageFieldObject.id = '#' + shortcode.id;
                        imageFieldObject.button = '.' + shortcode.id + '-' + optionKey + '-upload';
                        imageFieldObject.holder = '.' + shortcode.id + '-' + optionKey + '-holder';

                        if ( option.for == 'all') {
                            imageFieldObject.single = 'true';
                        }
                        else {
                            imageFieldObject.single = 'false';
                        }

                        imageFields.push(imageFieldObject);
                        break;
                    case 'color' :
                        colorFields.push('.' + shortcode.id + '-' + optionKey);
                        optionInput = '<input type="text" class="' + shortcode.id + '-' + optionKey + '" value="" />';
                        break;
                    case 'icon' :
                        iconInput =
                            '<input type="text" class="' + shortcode.id + '-icon" value="" />';

                        iconFields.push('.' + shortcode.id + '-icon');
                        break;
                }

                if (option.for == 'all') {
                    generalOptions +=
                        '<tr>' +
                            '<th><label>' + option.label +  '</label></th>' +
                            '<td>' + optionInput + '</td>' +
                            '</tr>';

                    generalIcons = iconInput;
                }
                else {
                    childrenOptions +=
                        '<tr>' +
                            '<th><label>' + option.label +  '</label></th>' +
                            '<td>' + optionInput + '</td>' +
                            '</tr>';

                    childrenIcons = iconInput;
                }
            });

            if (shortcode.type == 'multiple') {
                var childMarkup =
                    '<h3>' + shortcode.itemName + '</h3>' +
                    '<div>' +
                        '<table>' +
                        childrenOptions +
                        '</table>' +

                        childrenIcons +
                    '</div>';

                childrenMarkup =
                    '<div id="' + shortcode.id + '">' +
                        childMarkup +
                    '</div>' +

                    '<div class="admin-stylish-button add-new-button">' +
                        '<button id="add-new-' + shortcode.itemId + '">New ' + shortcode.itemName + '</button>' +
                    '</div>';

                shortcode.childrenTemplate = childMarkup;
            }
            else {
                childrenMarkup = '';
            }

            shortcodesMarkup +=
                '<div class="shortcode ' + shortcode.id + '-shortcode">' +
                    '<table>' +
                        generalOptions +
                    '</table>' +

                    generalIcons +

                    childrenMarkup +

                '</div>';
        });
    }

    /*
    *
    * Adds the form markup and its handlers to the dom
    *
    * */
    function createForm() {
        form.appendTo('body');

        jQuery('#shortcodes-form').css('display', 'none');
        jQuery('.shortcode').css('display', 'none');

        console.log('color fields: ', colorFields);
        console.log('image fields: ', imageFields);
        console.log('icon fields: ', iconFields);

        $.each(colorFields, function(index, fieldClass) {
            jQuery(fieldClass).wpColorPicker();
        });

        var parentContainer = '';

        //store old send to editor function
        var original_send_to_editor = window.send_to_editor;

        $.each(imageFields, function(index, field) {
            if ( field.single == 'true' ) {
                $(field.button).on('click', function() {

                    tb_show('Upload Media', 'media-upload.php?type=image&amp;TB_iframe=true');

                    // overrides the send_to_editor() function in media-upload script.
                    window.send_to_editor = function(html) {
                        imgurl = jQuery('img',html).attr('src');
                        $(field.holder).html('<img src="' + imgurl + '" width="150" />');
                        $(field.holder).next('input').val(imgurl);
                        tb_remove();

                        //restore old send to editor function
                        window.send_to_editor = original_send_to_editor;
                    };

                    return false;
                });
            }
            else {
                $(field.id).on('click', field.button,
                    function() {
                        parentContainer = $(this).parent('td');

                        tb_show('Upload Media', 'media-upload.php?type=image&amp;TB_iframe=true');

                        // overrides the send_to_editor() function in media-upload script.
                        window.send_to_editor = function(html) {
                            imgurl = jQuery('img',html).attr('src');
                            parentContainer.find(field.holder).html('<img src="' + imgurl + '" width="150" />');
                            parentContainer.find(field.holder).next('input').val(imgurl);
                            tb_remove();

                            //restore old send to editor function
                            window.send_to_editor = original_send_to_editor;
                        };

                        return false;
                    }
                );
            }
        });

        $.each(iconFields, function(index, field) {
            $(field).iconsform();
        });

        $.each(shortcodes, function(index, shortcode) {
            $('#' + shortcode.id).accordion({
                heightStyle: "content",
                collapsible: true,
                animate: 400
            });

            var html = shortcode.childrenTemplate;

            $('#add-new-' + shortcode.itemId).click(function() {
                jQuery('#' + shortcode.id).append(html);
                jQuery( "#" + shortcode.id ).accordion('destroy');
                jQuery( "#" + shortcode.id ).accordion({
                    heightStyle: "content",
                    collapsible: true,
                    animate: 400,
                    active: (jQuery('#' + shortcode.id).children('h3').length) - 1
                });

                $('.' + shortcode.id + '-icon').iconsform('refresh');
                $.each(colorFields, function(index, fieldClass) {
                    jQuery(fieldClass).wpColorPicker();
                });
            });
        });

        jQuery("#shortcodes").select2({
            placeholder: 'Select a Shortcode',
            width: 200
        });

        jQuery('#shortcodes').change(function() {
            var shortcode = jQuery(this).val();

            jQuery('.shortcode').css('display', 'none');
            jQuery('.' + shortcode + '-shortcode').css('display', 'block');
        });

    }

    /*
    * handles the click event of the submit button
    */
    function handleShortcodeSubmit() {
        var shortcode = '';

        var shortcodeBody = 'no shortcode';

        form.find('#shortcodes-submit').click(function(){

            var submittedShortCode = jQuery('#shortcodes').val();

            // iterate through all the existing shortcodes
            $.each(shortcodes, function(key, shortcode) {

                // check for our submitted shortcode
                if (shortcode.id === submittedShortCode) {

                    // an array containing an object with all the options for each shortcode item
                    // [ Object { option=value, option=value... }... ]
                    var options = [];

                    if (shortcode.type === 'multiple') {
                        // iterate through all shortcode items existing in the form
                        $('#' + shortcode.id).children('div').each(function(index, val) {

                            // an object containing all the options of the shortcode items
                            var subItemOptions = {};

                            // iterate through all registered options and for each one capture all the values present for all shortcode items
                            $.each(shortcode.options, function(optionName, optionInfo) {

                                console.log('option name: ', optionName);

                                // array containing all the options for the same field from the shortcode form
                                // ex. for color; [green, red, black...]
                                var attrValue = [];

                                // get all the values of the present option from the shortcode form
                                $('.' + shortcode.id + '-shortcode').find('.' + shortcode.id + '-' + optionName).each(function() {

                                    var option = $(this);

                                    console.log('option value: ', '.' + shortcode.id + '-shortcode');

                                    var inputType = option.attr('type');

                                    switch (inputType) {
                                        case 'checkbox':
                                            if (option.attr('checked') == 'checked') {
                                                attrValue.push(jQuery(this).val());
                                            }
                                            else {
                                                attrValue.push('');
                                            }
                                            break;
                                        default:
                                            attrValue.push($(this).val());
                                            break;
                                    }

                                });

                                // push the relevant option value into a new object property with the name of the present option
                                subItemOptions[optionName] = attrValue[index];

                            });

                            options.push(subItemOptions);

                        });
                    }
                    else if (shortcode.type === 'single') {

                        var subItemOptions = {};

                        // iterate through all registered options and for each one capture all the values present for all shortcode items
                        $.each(shortcode.options, function(optionName, optionInfo) {

                            var option = $('.' + shortcode.id + '-shortcode').find('.' + shortcode.id + '-' + optionName);

                            var inputType = option.attr('type');

                            switch (inputType) {
                                case 'checkbox':
                                    if (option.attr('checked') == 'checked') {
                                        subItemOptions[optionName] = option.val();
                                    }
                                    else {
                                        subItemOptions[optionName] = '';
                                    }
                                    break;
                                default:
                                    // push the relevant option value into a new object property with the name of the present option
                                    subItemOptions[optionName] = option.val();
                                    break;
                            }

                        });

                        options.push(subItemOptions);
                    }

//                    console.log('shortcodes', shortcodes);
                    console.log('options', options);

                    // get the data from the shortCode function
                    shortcodeBody = shortcode.generatedCode.call(this, options);
                }

            });

            insertShortcode(shortcodeBody);
        });
    }

    function insertShortcode(shortcode) {
        // inserts the shortcode into the active editor
        tinyMCE.activeEditor.execCommand('mceInsertContent', 0, shortcode);

        // closes Thickbox
        jQuery.magnificPopup.close();

        refresh();
    }

}(jQuery));
