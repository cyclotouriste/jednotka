(function($) {
    var themeColor = '';

    $.fn.shortcodes.add({
        name: 'Gap',
        id: 'gap',
        type: 'single',
        options: {
            height: {
                for: 'all',
                type: 'text',
                label: 'Gap Height'
            }
        },
        generatedCode: function(options) {
            var option = options[0];

            return '[jed_gap height="' + option.height + '"]'
        }
    });

    $.fn.shortcodes.add({
        name: 'Divider',
        id: 'divider',
        type: 'single',
        options: {
            type: {
                for: 'all',
                type: 'select',
                label: 'Divider Type',
                options: {
                    line: 'Solid Line',
                    custom: 'Custom'
                }
            },
            title: {
                for: 'all',
                type: 'text',
                label: 'Title',
                default: ''
            },
            icon: {
                for: 'all',
                type: 'icon',
                label: 'Icon'
            }
        },
        generatedCode: function(options) {

            var option = options[0];

            if (option.type != 'custom') {
                return '[jed_divider type="' + option.type + '"][/jed_divider]';
            }
            else {
                return '[jed_divider type="' + option.type + '" title="' + option.title + '" icon="' + option.icon + '"] Your content goes here [/jed_divider]';
            }
        }
    });

    $.fn.shortcodes.add({
        name: 'Jumbotron',
        id: 'jumbotron',
        type: 'single',
        generatedCode: function(options) {

            return '[jed_jumbotron] Your content goes here [/jed_jumbotron]'

        }
    });

    $.fn.shortcodes.add({
        name: 'Well',
        id: 'well',
        type: 'single',
        options: {
            size: {
                for: 'all',
                type: 'select',
                label: 'Size',
                default: 'medium',
                options: {
                    small: 'Small',
                    medium: 'Medium',
                    large: 'Large'
                }
            }
        },
        generatedCode: function(options) {
            var option = options[0];

            return '[jed_well size="' + option.size + '"] Your content goes here [/jed_well]';
        }
    });

    $.fn.shortcodes.add({
        name: 'Panel',
        id: 'panel',
        type: 'single',
        options: {
            heading: {
                for: 'all',
                type: 'select',
                label: 'Heading',
                options: {
                    no_heading: 'No Heading',
                    above: 'Above',
                    below: 'Below'
                }
            },
            heading_msg: {
                for: 'all',
                type: 'text',
                label: 'Heading Text',
                default: 'Heading Text'
            },
            border_color: {
                for: 'all',
                type: 'color',
                label: 'Border Color',
                default: themeColor
            }
        },
        generatedCode: function(options) {
            var option = options[0];

            var shortcodeHeading = '';
            if (option.heading != 'no_heading') {
                shortcodeHeading = 'heading="' + option.heading + '" heading_msg="' + option.heading_msg + '"';
            }

            return '[jed_panel ' + shortcodeHeading + ' border_color="' + option.border_color + '"] Your content goes here [/jed_panel]';
        }
    });

    $.fn.shortcodes.add({
        name: 'Social Icon',
        id: 'social-icon',
        type: 'single',
        options: {
            type: {
                for: 'all',
                type: 'select',
                label: 'Type',
                options: {
                    facebook: 'Facebook',
                    twitter: 'Twitter',
                    flickr: 'Flickr',
                    github: 'GitHub',
                    google_plus: 'Google Plus',
                    pinterest: 'Pinterest',
                    tumblr: 'Tumblr',
                    linkedin: 'Linkedin',
                    dribble: 'Dribble',
                    instagram: 'Instagram',
                    skype: 'Skype'
                }
            },
            link: {
                for: 'all',
                type: 'text',
                label: 'Link'
            },
            target: {
                for: 'all',
                type: 'checkbox',
                label: 'Open In New Tab'
            }
        },
        generatedCode: function(options) {
            var option = options[0];

            var target = '';
            if (option.target == 1) {
                target = 'blank';
            }

            return '[jed_social_icon type="' + option.type + '" link="' + option.link + '" target="' + target + '"]';
        }
    });

    $.fn.shortcodes.add({
        name: 'Blurb Text',
        id: 'blurb-text',
        type: 'single',
        options: {
            align_center: {
                for: 'all',
                type: 'checkbox',
                label: 'Align to center'
            }
        },
        generatedCode: function(options) {
            var option = options[0];
            var alignCenter = 'true';

            if ( option.align_center !== '1' ) {
                alignCenter = 'false';
            }

            return '[jed_blurb_text align_center="' + alignCenter + '"][/jed_blurb_text]';
        }
    });

    $.fn.shortcodes.add({
        name: 'Tabs',
        id: 'tabs',
        itemName: 'Tab',
        itemId: 'tab',
        type: 'multiple',
        options: {
            tabs_position: {
                for: 'all',
                type: 'select',
                label: 'Tabs Position',
                options: {
                    top: 'Top',
                    below: 'Below',
                    left: 'Left',
                    right: 'Right'
                }
            },
            title: {
                for: 'children',
                type: 'text',
                label: 'Title',
                default: 'Tab Title'
            }
        },
        generatedCode: function(options) {
            var option = options[0];
            var tabs_position = option.tabs_position;
            var children = '';

            $.each(options, function(key, option) {
                children += '[jed_tab title="' + option.title + '"] Your content goes here [/jed_tab]<br />';
            });

            return '[jed_tabs tabs_position="' + tabs_position + '"]<br />' + children + '[/jed_tabs]';
        }
    });

    $.fn.shortcodes.add({
        name: 'Image Gallery',
        id: 'image-gallery',
        itemName: 'image',
        itemId: 'image',
        type: 'multiple',
        options: {
            image: {
                for: 'children',
                type: 'image',
                label: 'Image'
            }
        },
        generatedCode: function(options) {
            var children = '';

            $.each(options, function(key, option) {
                children += '[jed_image]' + option.image + '[/jed_image]<br />'
            });

            return '[jed_image_gallery]<br />' + children + '[/jed_image_gallery]';
        }
    });

    $.fn.shortcodes.add({
        name: 'Accordion',
        id: 'accordion',
        itemName: 'Accordion Item',
        itemId: 'accordion-item',
        type: 'multiple',
        options: {
            style: {
                for: 'all',
                type: 'select',
                label: 'Accordion Style',
                options: {
                    plain: 'Plain',
                    bordered: 'Bordered'
                }
            },
            type: {
                for: 'all',
                type: 'select',
                label: 'Accordion Type',
                options: {
                    default: 'Default',
                    multiaccordions: 'Multiaccordions'
                }
            },
            title: {
                for: 'children',
                type: 'text',
                label: 'Accordion Item Title'
            }
        },
        generatedCode: function(options) {
            var children = '';
            var style = options[0].style;
            var type = options[0].type;

            $.each(options, function(key, option) {
                children += '[jed_accordion_item title="' + option.title + '"] Your content goes here[/jed_accordion_item]<br />'
            });

            return '[jed_accordion style="' + style + '" type="' + type + '"]<br />' + children + '[/jed_accordion]';
        }
    });

    $.fn.shortcodes.add({
        name: 'Services',
        id: 'services',
        itemName: 'service',
        itemId: 'service',
        type: 'multiple',
        options: {
            style: {
                for: 'all',
                type: 'select',
                label: 'Services Style',
                options: {
                    imaged: 'Imaged',
                    bordered: 'Bordered',
                    list: 'List'
                }
            },
            icon_type: {
                for: 'all',
                type: 'select',
                label: 'Services Icon Size',
                options: {
                    default: 'Default',
                    large_filled: 'Large and Filled',
                    small_filled: 'Small and Filled'
                }
            },
            img: {
                for: 'children',
                type: 'image',
                label: 'Service Image'
            },
            title: {
                for: 'children',
                type: 'text',
                label: 'Service Title',
                default: 'Title'
            },
            link: {
                for: 'children',
                type: 'text',
                label: 'Service Link',
                default: 'Link'
            },
            icon: {
                for: 'children',
                type: 'icon',
                label: 'Service Icon'
            }
        },
        generatedCode: function(options) {
            var option = options[0];
            var style = option.style;
            var children = '';
            var icon_type = options[0].icon_type;

            $.each(options, function(key, option) {
                children += '[jed_service_item img="' + option.img + '" icon="' + option.icon + '" title="' + option.title + '" link="' + option.link + '"] Your content goes here [/jed_service_item]<br/>';
            });

            return '[jed_services style="' + style + '" icon_type="' + icon_type + '"]<br/>' + children + '[/jed_services]';
        }
    });

    $.fn.shortcodes.add({
        name: 'Progress Bars',
        id: 'progress-bars',
        itemName: 'Progress Bar',
        itemId: 'progress-bar',
        type: 'multiple',
        options: {
            type: {
                for: 'all',
                type: 'select',
                label: 'Bars Type',
                options: {
                    default: 'Default',
                    striped: 'Striped',
                    animated: 'Animated',
                    stacked: 'Stacked'
                }
            },
            size: {
                for: 'all',
                type: 'select',
                label: 'Bars Size',
                options: {
                    small: 'Small',
                    medium: 'Medium',
                    large: 'Large'
                }
            },
            fill: {
                for: 'children',
                type: 'text',
                label: 'Progress Bar Fill',
                default: 50
            },
            color: {
                for: 'children',
                type: 'color',
                label: 'Progress Bar Color',
                default: themeColor
            }
        },
        generatedCode: function(options) {
            var option = options[0];
            var type = option.type;
            var size = option.size;
            var children = '';

            $.each(options, function(key, option) {
                children += '[jed_progress_bar fill="' + option.fill + '" color="' + option.color + '"]<br />';
            });

            return '[jed_progress_bars type="' + type + '" size="' + size + '"]<br />' + children + '[/jed_progress_bars]';
        }
    });

    $.fn.shortcodes.add({
        name: 'Message Box',
        id: 'message-box',
        type: 'single',
        options: {
            type: {
                for: 'all',
                type: 'select',
                label: 'Message Box Type',
                options: {
                    error: 'Error',
                    info: 'Info',
                    warning: 'Warning',
                    success: 'Success'
                }
            }
        },
        generatedCode: function(options) {
            var option = options[0];

            return '[jed_message type="' + option.type + '"] Your content goes here [/jed_message]';
        }
    });

    $.fn.shortcodes.add({
        name: 'Button',
        id: 'button',
        type: 'single',
        options: {
            text: {
                for: 'all',
                type: 'text',
                label: 'Button Text'
            },
            type: {
                for: 'all',
                type: 'select',
                label: 'Button Type',
                options: {
                    filled: 'Filled',
                    bordered: 'Bordered'
                }
            },
            size: {
                for: 'all',
                type: 'select',
                label: 'Button Size',
                options: {
                    mini: 'Mini',
                    small: 'Small',
                    medium: 'Medium',
                    large: 'Large'
                }
            },
            link: {
                for: 'all',
                type: 'text',
                label: 'Button Link',
                default: 'link'
            },
            target: {
                for: 'all',
                type: 'checkbox',
                label: 'Open In New Tab'
            },
            color: {
                for: 'all',
                type: 'color',
                label: 'Button Color',
                default: themeColor
            }
            /*animation: {
                for: 'all',
                type: 'select',
                label: 'Button Animation',
                options: {
                    no_animation: 'No animation',
                    flash: 'Flash',
                    bounce: 'Bounce',
                    shake: 'Shake',
                    tada: 'Tada',
                    swing: 'Swing',
                    wobble: 'Wobble',
                    pulse: 'Pulse',
                    flipOutX: 'Flip Out X',
                    flipOutY: 'Flip Out Y',
                    fadeOut: 'Fade Out',
                    fadeOutUp: 'Fade Out Up',
                    fadeOutDown: 'Fade Out Down',
                    fadeOutLeft: 'Fade Out Left',
                    fadeOutRight: 'Fade Out Right',
                    fadeOutUpBig: 'Fade Out Up Big',
                    fadeOutDownBig: 'Fade Out Down Big',
                    fadeOutLeftBig: 'Fade Out Left Big',
                    slideOutUp: 'Slide Out Up',
                    slideOutLeft: 'Slide Out Left',
                    slideOutRight: 'Slide Out Right',
                    bounceOut: 'Bounce Out',
                    bounceOutDown: 'Bounce Out Down',
                    bounceOutUp: 'Bounce Out Up',
                    bounceOutLeft: 'Bounce Out Left',
                    bounceOutRight: 'Bounce Out Right',
                    rotateOut: 'Rotate Out',
                    rotateOutDownLeft: 'Rotate Out Down Left',
                    rotateOutDownRight: 'Rotate Out Down Right',
                    rotateOutUpLeft: 'Rotate Out Up Left',
                    rotateOutUpRight: 'Roate Out Up Right',
                    lightSpeedOut: 'Light Speed Out',
                    hinge: 'Hinge',
                    rollOut: 'Roll Out'
                }
            }*/
        },
        generatedCode: function(options) {
            var option = options[0];

            return '[jed_button type="' + option.type + '" size="' + option.size + '" link="' + option.link + '" ' +
                'target="' + option.target + '" color="' + option.color + '"]' + option.text + '[/jed_button]';
        }
    });

    $.fn.shortcodes.add({
        name: 'Members',
        id: 'members',
        itemName: 'Member',
        itemId: 'member',
        type: 'multiple',
        options: {
            photo: {
                for: 'children',
                type: 'image',
                label: 'Member Photo'
            },
            name: {
                for: 'children',
                type: 'text',
                label: 'Member Name',
                default: 'name'
            },
            role: {
                for: 'children',
                type: 'text',
                label: 'Member Role'
            },
            info: {
                for: 'children',
                type: 'text',
                label: 'Member Info'
            }
        },
        generatedCode: function(options) {
            var children = '';

            $.each(options, function(key, option) {
                children += '[jed_member photo="' + option.photo + '" name="' + option.name + '" role="' + option.role + '" info="' + option.info + '"] Social icons for the member goes here [/jed_member]<br />';
            });

            return '[jed_members]<br />' + children + '[/jed_members]';
        }
    });

    $.fn.shortcodes.add({
        name: 'Pricing Tables',
        id: 'pricing-tables',
        itemName: 'Pricing Table',
        itemId: 'pricing-table',
        type: 'multiple',
        options: {
            rows : {
                for: 'all',
                type: 'text',
                label: 'Number Of Rows'
            },
            name: {
                for: 'children',
                type: 'text',
                label: 'Pricing Table Name'
            },
            price: {
                for: 'children',
                type: 'text',
                label: 'Price'
            },
            currency: {
                for: 'children',
                type: 'text',
                label: 'Currency'
            },
            payment_interval: {
                for: 'children',
                type: 'text',
                label: 'Payment Interval'
            },
            style: {
                for: 'children',
                type: 'select',
                label: 'Pricing Table Style',
                options: {
                    bordered: 'Bordered',
                    filled: 'Filled'
                }
            },
            link: {
                for: 'children',
                type: 'text',
                label: 'Pricing Table Link'
            },
            highlighted: {
                for: 'children',
                type: 'checkbox',
                label: 'Highlighted Table'
            },
            button_text: {
                for: 'children',
                type: 'text',
                label: 'Button Text'
            },
            icon: {
                for: 'children',
                type: 'icon',
                label: 'Icon'
            }
        },
        generatedCode: function(options) {
            var option = options[0];
            var rows = option.rows;
            var children = '';
            var tableRows = '';

            for (var i = 0; i < rows; i++) {
                tableRows += '[jed_pricing_table_row] Your content goes here [/jed_pricing_table_row]<br />';
            }

            var highlighted = 'off';
            if (option.highlighted) {
                highlighted = 'on';
            }

            $.each(options, function(key, option) {
                children += '[jed_pricing_table name="' + option.name + '" price="' + option.price + '" currency="' + option.currency + '" payment_interval="' + option.payment_interval + '" ' +
                    'icon="' + option.icon + '" style="' + option.style + '" link="' + option.link + '" highlighted="' + highlighted + '" button_text="' + option.button_text + '"]<br />' + tableRows + '[/jed_pricing_table]<br />';
            });

            return '[jed_pricing_tables]<br />' + children + '[/jed_pricing_tables]';
        }
    });

    $.fn.shortcodes.add({
        name: 'Table',
        id: 'table',
        itemName: 'Table Column',
        itemId: 'table-column',
        type: 'multiple',
        options: {
            type: {
                for: 'all',
                type: 'select',
                label: 'Table type',
                options: {
                    basic: 'Basic Table',
                    stripped: 'Stripped Table',
                    bordered: 'Bordered Table',
                    condensed: 'Condensed Table'
                }
            },
            hover: {
                for: 'all',
                type: 'checkbox',
                label: 'Hover Rows'
            },
            rows: {
                for: 'all',
                type: 'text',
                label: 'Number of rows'
            },
            name: {
                for: 'children',
                type: 'text',
                label: 'Column Name'
            },
            icon: {
                for: 'children',
                type: 'icon',
                label: 'Column icons'
            }
        },
        generatedCode: function(options) {

            var rowsNumber = options[0].rows;
            var tableType = options[0].type;

            var hoverRows = 'off';
            if (options[0].hover) {
                var hoverRows = 'on';
            }

            var rows = '';
            var columns = '';

            for (var i = 0; i < rowsNumber; i++) {
                rows += '[jed_table_row]Content goes here[/jed_table_row]<br />';
            }

            $.each(options, function(key, option) {
                columns += '[jed_table_column icon="' + option.icon + '" name="' + option.name + '"]<br />' + rows + '[/jed_table_column]<br />';
            });

            return '[jed_table type="' + tableType + '" rows="' + rowsNumber + '" hover_rows="' + hoverRows + '"]<br />' + columns + '[/jed_table]';

        }
    });

    $.fn.shortcodes.add({
        name: 'Latest Posts',
        id: 'latest-posts',
        type: 'single',
        generatedCode: function(options) {
            return '[jed_latest_posts]';
        }
    });

    $.fn.shortcodes.add({
        name: 'Latest Work',
        id: 'latest-work',
        type: 'single',
        generatedCode: function(options) {
            return '[jed_latest_work]';
        }
    });

    $.fn.shortcodes.add({
        name: 'Testimonials',
        id: 'testimonials',
        itemName: 'Testimonial',
        itemId: 'testimonial',
        type: 'multiple',
        options: {
            author: {
                for: 'children',
                type: 'text',
                label: 'Testimonial Author'
            },
            company: {
                for: 'children',
                type: 'text',
                label: 'Testimonial Company'
            }
        },
        generatedCode: function(options) {
            var children = '';

            $.each(options, function(key, option) {
                children += '[jed_testimonial author="' + option.author + '" company="' + option.company + '"] Your content goes here [/jed_testimonial]<br />';
            });

            return '[jed_testimonials]<br />' + children + '[/jed_testimonials]';
        }
    });

    $.fn.shortcodes.add({
        name: 'Circle Stats',
        id: 'circle-stats',
        itemName: 'Circle Stat',
        itemId: 'circle-stat',
        type: 'multiple',
        options: {
            title: {
                for: 'children',
                type: 'text',
                label: 'Stat Title'
            },
            percent: {
                for: 'children',
                type: 'text',
                label: 'Stat Fill'
            },
            size: {
                for: 'children',
                type: 'select',
                label: 'Stat Size',
                options: {
                    small: 'Small',
                    medium: 'Medium'
                }
            }
        },
        generatedCode: function(options) {
            var children = '';

            $.each(options, function(key, option) {
                children += '[jed_circle_stat title="' + option.title + '" percent="' + option.percent + '" size="' + option.size + '"] Your content goes here [/jed_circle_stat]<br />';
            });

            return '[jed_circle_stats]<br />' + children + '[/jed_circle_stats]';
        }
    });

    $.fn.shortcodes.add({
        name: 'Top Clients',
        id: 'top-clients',
        itemName: 'Client',
        itemId: 'client',
        type: 'multiple',
        options: {
            img: {
                for: 'children',
                type: 'image',
                label: 'Client Logo'
            }
        },
        generatedCode: function(options) {
            var children = '';

            $.each(options, function(key, option) {
                children += '[jed_client img="' + option.img + '"]<br />'
            });

            return '[jed_clients]<br />' + children + '[/jed_clients]';
        }
    });

    $.fn.shortcodes.add({
        name: 'Timeline',
        id: 'timeline',
        itemName: 'Timeline Item',
        itemId: 'timeline-item',
        type: 'multiple',
        options: {
            img: {
                for: 'children',
                type: 'image',
                label: 'Image'
            },
            time: {
                for: 'children',
                type: 'text',
                label: 'Time'
            },
            title: {
                for: 'children',
                type: 'text',
                label: 'Title'
            }
        },
        generatedCode: function(options) {
            var children = '';

            $.each(options, function(key, option) {
                children += '[jed_timeline_item img="' + option.img + '" time="' + option.time + '" title="' + option.title + '"] Your content goes here [/jed_timeline_item]<br />'
            });

            return '[jed_timeline]<br />' + children + '[/jed_timeline]';
        }
    });

    $.fn.shortcodes.add({
        name: 'List',
        id: 'list',
        itemName: 'List Item',
        itemId: 'list-item',
        type: 'multiple',
        options: {
            icons_style: {
                for: 'all',
                type: 'select',
                label: 'Icon Style',
                options: {
                    bordered: 'Bordered',
                    filled: 'Filled'
                }
            },
            icons_size: {
                for: 'all',
                type: 'select',
                label: 'Icons Size',
                options: {
                    tiny: 'Tiny',
                    small: 'Small',
                    medium: 'Medium',
                    large: 'Large'
                }
            },
            icons_shape: {
                for: 'all',
                type: 'select',
                label: 'Icons Shape',
                options: {
                    rounded: 'Rounded',
                    squared: 'Squared'
                }
            },
            color: {
                for: 'children',
                type: 'color',
                label: 'Icons Color'
            },
            icon: {
                for: 'children',
                type: 'icon',
                label: 'List Item Icon'
            }
        },
        generatedCode: function(options) {
            var children = '';
            var icons_style = options[0].icons_style;
            var icons_size = options[0].icons_size;
            var icons_shape = options[0].icons_shape;

            $.each(options, function(key, option) {
                children += '[jed_list_row color="' + option.color + '" icon="' + option.icon + '"] Your content goes here [/jed_list_row]<br />'
            });

            return '[jed_list icons_style="' + icons_style + '" icons_size="' + icons_size + '" icons_shape="' + icons_shape + '"]<br />' + children + '[/jed_list]';
        }
    });

    $.fn.shortcodes.add({
        name: 'Dropcap',
        id: 'dropcap',
        type: 'single',
        options: {
            style: {
                for: 'all',
                type: 'select',
                label: 'Dropcap Style',
                options: {
                    filled: 'Filled',
                    normal: 'Normal'
                }
            }
        },
        generatedCode: function(options) {
            var option = options[0];

            return '[jed_dropcap style="' + option.style + '"] Your content goes here [/jed_dropcap]';
        }
    });

    $.fn.shortcodes.add({
        name: 'Browser Wrapper',
        id: 'browser-wrapper',
        type: 'single',
        options: {
            link: {
                for: 'all',
                type: 'text',
                label: 'Link'
            },
            image: {
                for: 'all',
                type: 'image',
                label: 'Image'
            }
        },
        generatedCode: function(options) {
            var option = options[0];

            return '[jed_browser_wrapper link="' + option.link + '"]<img src="' + option.image + '" />[/jed_browser_wrapper]';
        }
    });

    $.fn.shortcodes.add({
        name: 'Blockquote',
        id: 'blockquote',
        type: 'single',
        options: {
            author: {
                for: 'all',
                type: 'text',
                label: 'Blockquote Author'
            }
        },
        generatedCode: function(options) {
            var option = options[0];

            return '[jed_blockquote author="' + option.author + '"] Your content goes here [/jed_blockquote]';
        }
    });

    $.fn.shortcodes.add({
        name: 'Columns Wrapper',
        id: 'columns-wrapper',
        type: 'single',
        generatedCode: function(options) {

            return '[jed_columns_wrapper][/jed_columns_wrapper]';
        }
    });

    $.fn.shortcodes.add({
        name: 'Fullwidth Section',
        id: 'fullwidth-section',
        type: 'single',
        generatedCode: function(options) {

            return '[jed_full_width_section][/jed_full_width_section]';
        }
    });

    $.fn.shortcodes.add({
        name: 'One Half',
        id: 'one-half',
        type: 'single',
        generatedCode: function(options) {

            return '[jed_one_half_column][/jed_one_half_column]';
        }
    });

    $.fn.shortcodes.add({
        name: 'One Third',
        id: 'one-third',
        type: 'single',
        generatedCode: function(options) {

            return '[jed_one_third_column][/jed_one_third_column]';
        }
    });

    $.fn.shortcodes.add({
        name: 'One Fourth',
        id: 'one-fourth',
        type: 'single',
        generatedCode: function(options) {

            return '[jed_one_fourth_column][/jed_one_fourth_column]';
        }
    });

    $.fn.shortcodes.add({
        name: 'Two Thirds',
        id: 'two-thirds',
        type: 'single',
        generatedCode: function(options) {

            return '[jed_two_thirds_column][/jed_two_thirds_column]';
        }
    });

    $.fn.shortcodes.add({
        name: 'Two Fourths',
        id: 'Two-fourths',
        type: 'single',
        generatedCode: function(options) {

            return '[jed_two_fourths_column][/jed_two_fourths_column]';
        }
    });

    $.fn.shortcodes();

}(jQuery));