/*
 * Creates a new button in the Tiny MCE Editor
 */
(function ($) {

    tinymce.PluginManager.add( 'holo_shortcodes', function( editor, url ) {

        // Add a button that opens a window
        editor.addButton( 'holo_shortcodes_button', {

            title : 'Shortcodes Generator', // title of the button
            image : '../wp-content/plugins/jednotka-shortcodes/editor-addon/shortcodes-image.png',  // path to the button's image
            onclick: function() {

                jQuery.magnificPopup.open({
                    items: {
                        src: '#shortcodes-form',
                        type: 'inline'
                    }
                });

                jQuery('#shortcodes-form').css('display', 'block');
            }

        } );

    } );

//    tinymce.create('tinymce.plugins.holo_shortcodes', {
//        // creates control instances based on the control's id.
//        // our button's id is "shortcodes_button"
//        createControl : function(id, controlManager) {
//            if (id == 'holo_shortcodes_button') {
//                // creates the button
//                var button = controlManager.createButton('holo_shortcodes_button', {
//                    title : 'Shortcodes Generator', // title of the button
//                    image : 'shortcodes-image.png',  // path to the button's image
//                    onclick : function() {
//                        jQuery.magnificPopup.open({
//                            items: {
//                                src: '#shortcodes-form',
//                                type: 'inline'
//                            }
//                        });
//
//                        jQuery('#shortcodes-form').css('display', 'block');
//                    }
//                });
//                return button;
//            }
//
//            return null;
//        }
//    });
//
//    // registers the plugin.
//    tinymce.PluginManager.add('holo_shortcodes', tinymce.plugins.holo_shortcodes);
})(jQuery);