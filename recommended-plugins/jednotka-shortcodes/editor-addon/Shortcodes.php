<?php
/*
Plugin Name: mygallery
Plugin URI: http://wphardcore.com
Description: A simple user interface for Gallery shortcode
Version: 0.1
Author: Gary Cao
Author URI: http://garyc40.com
*/

if ( ! defined( 'ABSPATH' ) )
	die( "Can't load this file directly" );

class Shortcodes
{
	function __construct() {
		add_action( 'admin_init', array( $this, 'action_admin_init' ) );
        add_action( 'admin_enqueue_scripts', array($this, 'action_admin_scripts') );
	}

    function action_admin_scripts() {

        /* jquery Libraries */
        //wp_enqueue_script('jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js', array(), false, true);
        //wp_enqueue_script('jqueryui', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js', array(), '1.10.3', true);
		wp_enqueue_script('jquery-ui-accordion');
		wp_enqueue_script('jquery-ui-autocomplete');

		wp_enqueue_style( 'wp-color-picker' );

        wp_enqueue_script('select-2-js', plugins_url() . '/jednotka-shortcodes/shortcodes-generator/select2-3.4.3/select2.min.js', array('jquery'), false, true);
        wp_enqueue_style('select-2-css', plugins_url() . '/jednotka-shortcodes/shortcodes-generator/select2-3.4.3/select2.css');

        wp_enqueue_style('magnific-popup-css', plugins_url() . '/jednotka-shortcodes/shortcodes-generator/magnific-popup/magnific-popup.css');
        wp_enqueue_script('magnific-popup-js', plugins_url() . '/jednotka-shortcodes/shortcodes-generator/magnific-popup/jquery.magnific-popup.min.js', array('jquery'), false, true);

        wp_enqueue_script('iconsform-js', plugins_url() . '/jednotka-shortcodes/editor-addon/jquery.iconsform.js', array('jquery'), false, true);
        wp_enqueue_script('add-shortcodes-js', plugins_url() . '/jednotka-shortcodes/shortcodes-generator/jquery.shortcodes-generator.js', array('jquery','wp-color-picker'), false, true);
        wp_enqueue_script('shortcodes-js', plugins_url() . '/jednotka-shortcodes/editor-addon/shortcodes.js', array('jquery'), false, true);

        wp_enqueue_style('shortcodes-admin-panel-style', plugins_url() . '/jednotka-shortcodes/editor-addon/style.css');

    }

	function action_admin_init() {

		// only hook up these filters if we're in the admin panel, and the current user has permission
		// to edit posts and pages
		if ( current_user_can( 'edit_posts' ) && current_user_can( 'edit_pages' ) ) {
			add_filter( 'mce_buttons', array( $this, 'filter_mce_button' ) );
			add_filter( 'mce_external_plugins', array( $this, 'filter_mce_plugin' ) );
		}
	}

	function filter_mce_button( $buttons ) {
		// add a separation before our button, here our button's id is "mygallery_button"
		array_push( $buttons, '|', 'holo_shortcodes_button' );
		return $buttons;
	}

	function filter_mce_plugin( $plugins ) {
		// this plugin file will work the magic of our button
		$plugins['holo_shortcodes'] = plugins_url() . '/jednotka-shortcodes/editor-addon/tinymce-sc-button.js';
		return $plugins;
	}
}

$mygallery = new Shortcodes();
