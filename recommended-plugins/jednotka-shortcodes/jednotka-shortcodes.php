<?php
/**
 * Plugin Name: Jednotka Shortcodes
 * Plugin URI:
 * Description: The Shortcodes needed for Jednotka Wordpress Theme
 * Author: Holobest
 * Version: 1.1.3
 * Author URI: http://holobest.com
 */

require_once('editor-addon/Shortcodes.php');

add_filter('excerpt_length', 'shortcodes_custom_excerpt_length', 999);
function shortcodes_custom_excerpt_length($length) {
    return 25;
}

/*
 * code
 * gap
 * divider
 * social icons
 * blurb text
 * jumbotron
 * wells
 * panels
 * tabs
 * accordions
 * services
 * progress bars
 * message boxes
 * buttons
 * members
 * image gallery
 * pricing tables
 * latest posts
 * latest work
 * testimonials
 * circle stats
 * top clients
 * timeline
 * tables
 * lists
 * dropcaps
 * browser wrapper
 * columns wrapper
 * fullwidth section
 */


/**********************************************************************************************************************/
/****************************************************** Code **********************************************************/
/**********************************************************************************************************************/
add_shortcode('jed_code', 'jed_code_shortcode');

function jed_code_shortcode($atts, $content) {

    $returnContent =
        '<pre>
            ' . $content . '
        </pre>
    ';

    return $returnContent;

}


/**********************************************************************************************************************/
/******************************************************* Gap **********************************************************/
/**********************************************************************************************************************/
add_shortcode('jed_gap', 'jed_gap_shortcode');

function jed_gap_shortcode($atts, $content) {
    extract(shortcode_atts(array(
        'height' => ''
    ), $atts));

    $returnContent =
        '<div style="width: 100%; clear: both; height: ' . (int)$height . 'px"></div>';

    return $returnContent;
}


/**********************************************************************************************************************/
/***************************************************** Divider ********************************************************/
/**********************************************************************************************************************/
add_shortcode('jed_divider', 'jed_divider_shortcode');

function jed_divider_shortcode($atts, $content) {
    extract(shortcode_atts(array(
        'type' => 'line',
        'icon' => '',
        'title' => ''
    ), $atts));

    switch ($type) {
        case 'line' :
            $return = '<hr />';
            break;
        case 'custom' :
            $return = '
            <div class="page-header page-header-with-icon">
                <i class="' . $icon . '"></i>
                <h2>' . $title . '<small>' . $content . '</small></h2>
            </div>';
            break;
    }

    return $return;
}

/**********************************************************************************************************************/
/************************************************** Social Icons ******************************************************/
/**********************************************************************************************************************/
add_shortcode('jed_social_icon', 'jed_social_icon_shortcode');

function jed_social_icon_shortcode($atts) {
    extract(shortcode_atts(array(
        'target' => '',
        'align' => '',
        'type' => '',
        'link' => ''
    ), $atts));

    $social_type = '';
    $icon = '';

    switch ($type) {
        case 'facebook':
            $icon = 'fa-icon-facebook';
            break;
        case 'twitter':
            $icon = 'fa-icon-twitter';
            break;
        case 'flickr':
            $icon = 'fa-icon-flickr';
            break;
        case 'github':
            $icon = 'fa-icon-github';
            break;
        case 'google_plus':
            $icon = 'fa-icon-google-plus';
            break;
        case 'pinterest':
            $icon = 'fa-icon-pinterest';
            break;
        case 'tumblr':
            $icon = 'fa-icon-tumblr';
            break;
        case 'linkedin':
            $icon = 'fa-icon-linkedin';
            break;
        case 'dribble':
            $icon = 'fa-icon-dribbble';
            break;
        case 'instagram':
            $icon = 'fa-icon-instagram';
            break;
        case 'skype':
            $icon = 'fa-icon-skype';
            break;
        case 'rss':
            $icon = 'fa-icon-rss';
            break;

    }

    $target = ($target == 'blank') ? ' target="_blank"' : '';

    $returnContent =
        '<a href="' . $link . '" class="btn btn-sm btn-circle social-icon" ' . $target . '>
          <i class="' . $icon . '"></i>
        </a>';

    return $returnContent;
}

/**********************************************************************************************************************/
/**************************************************** Blurb Text ******************************************************/
/**********************************************************************************************************************/
add_shortcode('jed_blurb_text', 'jed_blurb_text_shortcode');

function jed_blurb_text_shortcode($atts, $content) {
    extract(shortcode_atts(array(
        'align_center' => 'true'
    ), $atts));

    if ( $align_center == 'false' ) {
        $returnData = '
            <p class="lead no-mg-b">' . $content . '</p>
        ';
    }
    else {
        $returnData = '
            <p class="lead text-center no-mg-b">' . $content . '</p>
        ';
    }

    return $returnData;
}

/**********************************************************************************************************************/
/**************************************************** Blurb Text ******************************************************/
/**********************************************************************************************************************/
add_shortcode('jed_jumbotron', 'jed_jumbotron_shortcode');

function jed_jumbotron_shortcode($atts, $content) {

    $returnData = '
        <div class="jumbotron">
            ' . do_shortcode($content) . '
        </div>
    ';

    return $returnData;
}

/**********************************************************************************************************************/
/****************************************************** Wells *********************************************************/
/**********************************************************************************************************************/
add_shortcode('jed_well', 'jed_well_shortcode');

function jed_well_shortcode($atts, $content) {
    extract(shortcode_atts(array(
        'size' => ''
    ), $atts));

    switch($size) {
        case 'small' :
            $well_size = 'well-sm';
            break;
        case 'large' :
            $well_size = 'well-lg';
            break;
        default :
            $well_size = '';
            break;
    }

    $returnData = '<div class="well ' . $well_size . '">' . $content . '</div>';

    return $returnData;
}

/**********************************************************************************************************************/
/****************************************************** Panels ********************************************************/
/**********************************************************************************************************************/
add_shortcode('jed_panel', 'jed_panel_shortcode');

function jed_panel_shortcode($atts, $content) {
    extract(shortcode_atts(array(
        'heading' => '',
        'heading_msg' => '',
        'border_color' => ''
    ), $atts));

    $panel_border_color = $border_color !== '' ? 'border-color: ' . $border_color : '';

    switch($heading) {
        case 'above' :
            $panel_heading = '
                <div class="panel-heading" style="background-color: ' . $border_color . '; border-color: ' . $border_color . '">' . $heading_msg . '</div>
                <div class="panel-body">
                    ' . $content . '
                </div>
            ';
            break;
        case 'below' :
            $panel_heading = '
                <div class="panel-body">
                    ' . $content . '
                </div>
                <div class="panel-footer" style="background-color: ' . $border_color . '; border-color: ' . $border_color . '">' . $heading_msg . '</div>
            ';
            break;
        default :
            $panel_heading = '
                <div class="panel-body">
                    ' . $content . '
                </div>
            ';
            break;
    }

    $returnData = '
        <div class="panel panel-bordered mg-b" style="' . $panel_border_color . '">
            ' . $panel_heading . '
        </div>
    ';

    return $returnData;
}

/**********************************************************************************************************************/
/******************************************************* Tabs *********************************************************/
/**********************************************************************************************************************/
add_shortcode('jed_tabs', 'jed_tabs_shortcode');

function jed_tabs_shortcode($atts, $content) {
    extract(shortcode_atts(array(
        'tabs_position' => 'top'
    ), $atts));

    $GLOBALS['tabs'] = array();
    $uniqueId = rand(1000, 9999);

    switch($tabs_position) {
        case 'top' :
            $position = '';
            break;
        case 'below' :
            $position = 'tabs-below';
            break;
        case 'left' :
            $position = 'tabs-left';
            break;
        case 'right' :
            $position = 'tabs-right';
            break;
    }

    do_shortcode($content);

    if (is_array($GLOBALS['tabs'])) {
        $int = 1;

        foreach( $GLOBALS['tabs'] as $tab) {
            $active = $int == 1 ? 'active' : '';

            $tabs[] = '
                <li class="' . $active . '"><a href="#tab' . $uniqueId . '-content-' . $int . '" data-toggle="tab">' . $tab['title'] . '</a></li>
            ';

            $panes[] = '
            <div id="tab' . $uniqueId . '-content-' . $int . '" class="tab-pane fade in ' . $active . '">
                <p class="no-mg-b">' . $tab['content'] . '<p>
            </div>
            ';

            $int++;
        }

        if ($tabs_position == 'below') {
            $return ='
            <div class="tabbable ' . $position . '">
                <div id="tab' . $uniqueId . '-content" class="tab-content">' . implode('', $panes) . '</div>
                <ul id="tab' . $uniqueId . '" class="nav nav-tabs">' . implode('', $tabs) . '</ul>
            </div>';
        } else {
            $return ='
            <div class="tabbable ' . $position . '">
                <ul id="tab' . $uniqueId . '" class="nav nav-tabs">' . implode('', $tabs) . '</ul>
                <div id="tab' . $uniqueId . '-content" class="tab-content">' . implode('', $panes) . '</div>
            </div>';
        }
    }

    return $return;
}

add_shortcode('jed_tab', 'jed_tab_shortcode');

function jed_tab_shortcode($atts, $content) {
    extract(shortcode_atts(array(
        'title' => ''
    ), $atts));

    $GLOBALS['tabs'][] = array('title' => sprintf($title), 'content' => $content);
}


/**********************************************************************************************************************/
/**************************************************** Accordions ******************************************************/
/**********************************************************************************************************************/
add_shortcode('jed_accordion', 'jed_accordion_shortcode');

function jed_accordion_shortcode($atts, $content) {
    extract(shortcode_atts(array(
        'style' => 'plain',
        'type' => 'default'
    ), $atts));

    $GLOBALS['accordion_items'] = array();
    $uniqueId = rand(1000, 9999);

    do_shortcode($content);

    $accordionStyle = ($style == 'bordered' ? 'accordion-bordered' : '');
    $accordionType = ($type == 'multiaccordions' ? '' : 'data-parent="#accordion' . $uniqueId . '"');

    if (is_array($GLOBALS['accordion_items'])) {
        $accordion = array();

        $index = 1;
        foreach ($GLOBALS['accordion_items'] as $item) {
            $active = ($index === 1 ? 'in' : '');
            $collapsed = ($index === 1 ? '' : 'collapsed');

            $accordion[] = '
                <div class="panel panel-default">
                    <div class="panel-heading">
                      <h4 class="panel-title">
                        <a class="accordion-toggle ' . $collapsed . '" ' . $accordionType . ' data-toggle="collapse" href="#accordion' . $uniqueId . '-item-' . $index . '">
                          ' . $item['title'] . '
                        </a>
                      </h4>
                    </div>
                    <div class="panel-collapse collapse ' . $active . '" id="accordion' . $uniqueId . '-item-' . $index . '">
                      <div class="panel-body">
                        ' . $item['content'] . '
                      </div>
                    </div>
                </div>
            ';

            $index++;
        }
    }

    $returnContent = '
        <div class="panel-group accordion ' . $accordionStyle . '" id="accordion' . $uniqueId . '">' . implode('', $accordion) . '</div>
    ';

    return $returnContent;
}

add_shortcode('jed_accordion_item', 'jed_accordion_item_shortcode');

function jed_accordion_item_shortcode($atts, $content) {
    extract(shortcode_atts(array(
        'title' => ''
    ), $atts));

    $GLOBALS['accordion_items'][] = array('title' => $title, 'content' => $content);
}


/**********************************************************************************************************************/
/**************************************************** Services ********************************************************/
/**********************************************************************************************************************/
add_shortcode('jed_services', 'jed_services_shortcode');

function jed_services_shortcode($atts, $content) {

    extract(shortcode_atts(array(
        'style' => 'imaged',
        'icon_type' => ''
    ), $atts));

    $GLOBALS['service_items'] = array();

    do_shortcode($content);

    $returnContent = '';

    if (is_array($GLOBALS['service_items'])) {
        $services = array();

        if ($style == 'imaged' ) {
            foreach ( $GLOBALS['service_items'] as $service ) {
                $services[] = '
                    <div class="col-sm-4 panel-item">
                      <a href="' . $service['link'] . '" class="panel panel-image">
                        <div class="panel-icon">
                          <i class="' . $service['icon'] . ' icon"></i>
                        </div>
                        <div class="panel-heading"><img width="768" height="300" src="' . $service['img'] . '" class="img-responsive-sm"></div>
                        <div class="panel-body">
                          <h3 class="panel-title">' . $service['title'] . '</h3>
                          <p>' . $service['content'] . '</p>
                        </div>
                      </a>
                    </div>
                ';
            }

            $returnContent = '
            <div class="row panels">' . implode('', $services) . '</div>
        ';
        }
        elseif ($style == 'bordered') {
            $icon_size_class = '';
            if ( $icon_type == 'large_filled' ) {
                $icon_size_class = 'panel-circle-big-contrast';
            }
            elseif ( $icon_type == 'small_filled') {
                $icon_size_class = 'panel-circle-contrast';
            }
            else {
                $icon_size_class = '';
            }

            foreach ( $GLOBALS['service_items'] as $service ) {
                $services[] = '
                    <div class="col-sm-4 panel-item">
                      <a href="' . $service['link'] . '" class="panel panel-big ' . $icon_size_class . '">
                        <div class="panel-icon">
                          <i class="' . $service['icon'] . ' icon"></i>
                        </div>
                        <div class="panel-body">
                          <h3 class="panel-title">' . $service['title'] . '</h3>
                          <p>' . $service['content'] . '</p>
                        </div>
                      </a>
                    </div>
                ';
            }

            $returnContent = '
                <div class="row panels">
                    ' . implode('', $services) . '
                </div>
            ';
        }
        else {
            foreach ( $GLOBALS['service_items'] as $service ) {
                $services[] = '
                    <div class="col-sm-6 icon-box">
                      <div class="icon icon-wrap icon-circle icon-lg contrast-bg">
                        <i class="' . $service['icon'] . ' text-white"></i>
                      </div>
                      <div class="content">
                        <h3 class="title">' . $service['title'] . '</h3>
                        <p>' . $service['content'] . '</p>
                      </div>
                    </div>
                ';
            }

            $returnContent = '
                <div class="icon-boxes icon-boxes-lg clearfix">
                    ' . implode('', $services) . '
                </div>
            ';
        }
    }

    return $returnContent;
}

add_shortcode('jed_service_item', 'jed_service_shortcode');

function jed_service_shortcode($atts, $content) {
    extract(shortcode_atts(array(
        'img' => '',
        'icon' => '',
        'title' => '',
        'link' => ''
    ), $atts));

    $GLOBALS['service_items'][] = array('icon' => $icon, 'title' => $title, 'link' => $link, 'img' => $img, 'content' => $content);
}


/**********************************************************************************************************************/
/************************************************* Progress Bars ******************************************************/
/**********************************************************************************************************************/
// Create Skill Bars shortcodes
add_shortcode('jed_progress_bars', 'jed_progress_bars_shortcode');

function jed_progress_bars_shortcode($atts, $content) {
    extract(shortcode_atts(array(
        'type' => '',
        'size' => '',
    ), $atts));

    $GLOBALS['progress_bars'] = array();

    switch ($type) {
        case 'striped' :
            $barType = 'progress-striped';
            break;
        case 'animated' :
            $barType = 'progress-striped active';
            break;
        default :
            $barType = '';
            break;
    }

    switch ($size) {
        case 'medium' :
            $barSize = 'progress-md';
            break;
        case 'large' :
            $barSize = 'progress-lg';
            break;
        default :
            $barSize = '';
            break;
    }

    do_shortcode($content);

    if ($type == 'stacked') {
        if ( is_array($GLOBALS['progress_bars']) ) {
            $skill_bars = array();

            foreach ( $GLOBALS['progress_bars'] as $skill_bar) {
                $skill_bars[] = $skill_bar;
            }
        }

        $returnContent = '<div class="progress ' . $barType . ' ' . $barSize . '">' . implode('', $skill_bars) . '</div>';
    } else {

        if ( is_array($GLOBALS['progress_bars']) ) {
            $skill_bars = array();

            foreach ( $GLOBALS['progress_bars'] as $skill_bar) {
                $skill_bars[] = '<div class="progress ' . $barType . ' ' . $barSize . '">' . $skill_bar . '</div>';
            }
        }

        $returnContent = implode('', $skill_bars);
    }



    return $returnContent;
}

add_shortcode('jed_progress_bar', 'holo_progress_bar_shortcode');

function holo_progress_bar_shortcode($atts, $content) {
    extract(shortcode_atts(array(
        'fill' => '',
        'color' => ''
    ), $atts));

    $GLOBALS['progress_bars'][] = '
        <div class="progress-bar" data-width="' . $fill . '" style="width: ' . $fill . '%; background-color: ' . $color . '">
            <span class="sr-only"></span>
        </div>
    ';
}

/**********************************************************************************************************************/
/************************************************** Message Boxes *****************************************************/
/**********************************************************************************************************************/
// Create Message Boxes shortcode
add_shortcode('jed_message', 'jed_message_shortcode');

function jed_message_shortcode($atts, $content) {
    extract(shortcode_atts(array(
        'type' => '' // ok box, question box, info box, idea box
    ), $atts));

    $message_type = '';
    $message = '';

    switch ($type) {
        case 'error':
            $message_type = 'alert-danger';
            $message = 'Error Message!';
            break;
        case 'info' :
            $message_type = 'alert-info';
            $message = 'Heads up!';
            break;
        case 'warning':
            $message_type = 'alert-warning';
            $message = 'Warning!';
            break;
        case 'success':
            $message_type = 'alert-success';
            $message = 'Success Message!';
            break;
    }

    $returnContent = '
        <div class="alert fade in ' . $message_type . '">
            <strong>' . $message . '</strong>
            ' . $content . '
            <button class="close" type="button" aria-hidden="true" data-dismiss="alert">×</button>
        </div>
    ';

    return $returnContent;
}

/**********************************************************************************************************************/
/***************************************************** Buttons ********************************************************/
/**********************************************************************************************************************/
// Create Button shortcode
add_shortcode('jed_button', 'jed_button_shortcode');

function jed_button_shortcode($atts, $content) {
    extract(shortcode_atts(array(
        'type' => 'filled',
        'link' => '#',
        'target' => '',
        'color' => '',
        'size' => 'medium',
        'align' => '',
        'icon' => '',
        'animation' => ''
    ), $atts));

    $background = '';
    $textColor = '';

    switch ($type) {
        case 'filled' :
            $background = ($color !== '' ? 'background-color: ' . $color . '' : '');
            break;
        case 'bordered' :
            $background = ($color !== '' ? 'background-color: transparent; border: 2px solid ' . $color : '');
            $textColor = ($color !== '' ? 'color: ' . $color : '');
            break;
    }

    $class = ($type == 'filled' ? 'button-wrapper' : 'button-wrapper btn-bordered');

    switch ($size) {
        case 'mini' :
            $size = 'btn-xs';
            break;
        case 'small' :
            $size = 'btn-sm';
            break;
        case 'large' :
            $size = 'btn-lg';
            break;
        default :
            $size = '';
            break;
    }

    $style = 'style="' . $background . '"';
    $target = ($target == 'blank') ? ' target="_blank"' : '';
    $align = ($align) ? ' ' . $align : '';
    $iconTag = $icon !== '' ? '<i class="' . $icon . '"></i>' : '';

    $returnContent = '
        <div class="' . $class . '" ' . $style . ' data-color="' . $color . '" data-animation="' . $animation . '">
            <a class="btn ' . $size . '" href="' . $link . '" ' . $target . ' style="' . $textColor . '">' . $content . '</a>
        </div>
    ';

    return $returnContent;
}

/**********************************************************************************************************************/
/************************************************** Members ******************************************************/
/**********************************************************************************************************************/
// Create Team Members shortcodes
add_shortcode('jed_members', 'jed_members_shortcode');

function jed_members_shortcode($atts, $content) {
    $GLOBALS['members'] = array();

    do_shortcode($content);

    if ( is_array($GLOBALS['members']) ) {
        $members = array();

        $index = 1;
        foreach ( $GLOBALS['members'] as $member ) {

            if ( $index % 2 == 0) {
                $members[] = $member . '</div>';
            }
            else {
                $members[] = '<div class="row">' . $member;
            }

            $index++;
        }
    }

    $returnContent = '
        <div class="profile-boxes">
            ' . implode('', $members) . '
        </div>
    ';

    return $returnContent;
}

add_shortcode('jed_member', 'jed_member_shortcode');

function jed_member_shortcode($atts, $content) {
    extract(shortcode_atts(array(
        'photo' => '',
        'name' => '',
        'role' => '',
        'info' => ''
    ), $atts));

    $GLOBALS['members'][] = '
        <div class="col-sm-6 profile-box">
          <div class="row">
            <div class="col-sm-4">
              <a class="image-link" data-lightbox data-lightbox-gallery="team" href="' . $photo . '" title="' . $name . '">
                <i class="fa-icon-envelope-alt"></i>
                <img alt="David Doe" width="165" height="165" src="' . $photo . '" />
              </a>
            </div>
            <div class="col-sm-8">
              <h3 class="name">' . $name . '</h3>
              <h4 class="position">' . $role . '</h4>
              <p>' . $info . '</p>
              <div class="links">
                ' . do_shortcode($content) . '
              </div>
            </div>
          </div>
        </div>
    ';
}


/**********************************************************************************************************************/
/************************************************* Image Gallery ******************************************************/
/**********************************************************************************************************************/
// Create Team Members shortcodes
add_shortcode('jed_image_gallery', 'jed_image_gallery_shortcode');

function jed_image_gallery_shortcode($atts, $content) {
    $GLOBALS['gallery_images'] = array();

    do_shortcode($content);

    $images = array();
    if ( is_array($GLOBALS['gallery_images']) ) {
        foreach ( $GLOBALS['gallery_images'] as $gallery_image ) {
            $images[] = $gallery_image;
        }
    }
    $returnContent = '
        <div class="row text-center portfolio-boxes">
              ' . implode('', $images) . '
        </div>
    ';

    return $returnContent;

}


add_shortcode('jed_image', 'jed_image_shortcode');

function jed_image_shortcode($atts, $content) {

    $fullImage = $content;

    $thumbnail = '-150x150';

    $image = explode('.', $content);

    $imageExtension = $image[count($image) - 1];

    unset($image[count($image) - 1]);

    $image = implode('.', $image);

    $imageName = $image . $thumbnail . '.' . $imageExtension;

    $GLOBALS['gallery_images'][] = '
            <div class="col-sm-3 mg-b">
                    <a href="' . $content . '" data-lightbox-gallery="lightbox-test" data-lightbox="">
                      <img width="165" height="165" src="' . $imageName . '">
                    </a>
          </div>
    ';
}


/**********************************************************************************************************************/
/************************************************* Pricing tables *****************************************************/
/**********************************************************************************************************************/
// Create Pricing Tables shortcodes
add_shortcode('jed_pricing_tables', 'jed_pricing_tables_shortcode');

function jed_pricing_tables_shortcode($atts, $content) {
    $GLOBALS['pricing_tables'] = array();

    do_shortcode($content);

    if ( is_array($GLOBALS['pricing_tables']) ) {
        $pricing_tables = array();

        $tablesNumber = count($GLOBALS['pricing_tables']);

        foreach ( $GLOBALS['pricing_tables'] as $pricing_table ) {

            switch ($tablesNumber) {
                case 4 :
                    $pricing_tables[] = '<div class="col-sm-3 panel-item">' . $pricing_table . '</div>';
                    break;
                default :
                    $pricing_tables[] = '<div class="col-sm-4 panel-item">' . $pricing_table . '</div>';
                    break;
            }
        }
    }

    $returnContent = '
        <div class="row panels">
            ' . implode('', $pricing_tables) . '
        </div>
    ';

    return $returnContent;
}

add_shortcode('jed_pricing_table', 'jed_pricing_table_shortcode');

function jed_pricing_table_shortcode($atts, $content) {
    extract(shortcode_atts(array(
        'highlighted' => 'off',
        'name' => '',
        'price' => '',
        'currency' => '',
        'payment_interval' => '',
        'icon' => '',
        'style' => 'bordered',
        'link' => '',
        'button_text' => 'Buy now'
    ), $atts));

    $GLOBALS['pricing_table_rows'] = array();

    do_shortcode($content);

    $isHighlighted = $highlighted === 'on' ? 'panel-contrast' : 'panel-light';

    if ( is_array($GLOBALS['pricing_table_rows']) ) {
        $pricing_table_rows = array();

        foreach ( $GLOBALS['pricing_table_rows'] as $pricing_table_row) {
            $pricing_table_rows[] = $pricing_table_row;
        }
    }

    if ( $style == 'bordered' ) {
        if ( !empty($link) ) {
            if ( $highlighted === 'on' ) {
                $table_link = '
                <div class="button-wrapper button-bordered table-button">
                    <a class="btn btn-block btn-light " href="' . $link . '">' . $button_text . '</a>
                </div>
            ';
            }
            else {
                $table_link = '
                <div class="button-wrapper button-bordered table-button table-button-normal">
                    <a class="btn btn-block btn-light " href="' . $link . '">' . $button_text . '</a>
                </div>
            ';
            }
        }
        else {
            $table_link ='';
        }

        $GLOBALS['pricing_tables'][] = '
        <div class="panel panel-circle-contrast ' . $isHighlighted . ' pricing-table">
          <div class="panel-icon">
            <i class="' . $icon . ' icon"></i>
          </div>
          <div class="panel-body">
            <h3 class="panel-title">' . $name . '</h3>
            <h4 class="price">
              <span class="currency">
                ' . $currency . '
              </span>
              ' . $price . '
            </h4>
            <p class="period">/ ' . $payment_interval . '</p>
            <ul class="list-unstyled">
              ' . implode('', $pricing_table_rows) . '
            </ul>
            ' . $table_link . '
            </div>
        </div>
    ';
    }
    else {
        if ( !empty($link) ) {
            if ( $highlighted === 'on' ) {
                $table_link = '<a class="btn btn-block btn-light btn-bordered" href="' . $link . '">' . $button_text . '</a>';
            }
            else {
                $table_link = '<a class="btn btn-block btn-light btn-bordered btn-table" href="' . $link . '">' . $button_text . '</a>';
            }
        }
        else {
            $table_link = '';
        }

        $GLOBALS['pricing_tables'][] = '
            <div class="panel panel-bordered ' . $isHighlighted . ' pricing-table pricing-table-solid">
              <div class="panel-heading">
                <h3 class="panel-title">' . $name . '</h3>
              </div>
              <div class="panel-body">
                <h4 class="price">
                  <span class="currency">
                    ' . $currency . '
                  </span>
                  ' . $price . '
                </h4>
                <p class="period">/ ' . $payment_interval . '</p>
                <ul class="list-unstyled">
                  ' . implode('', $pricing_table_rows) . '
                </ul>

                ' . $table_link . '
              </div>
            </div>
        ';
    }
}

add_shortcode('jed_pricing_table_row', 'jed_pricing_table_row_shortcode');

function jed_pricing_table_row_shortcode($atts, $content) {
    $GLOBALS['pricing_table_rows'][] = '
        <li>' . $content . '</li>
    ';
}

/**********************************************************************************************************************/
/************************************************** Latest posts ******************************************************/
/**********************************************************************************************************************/

add_shortcode('jed_latest_posts', 'jed_latest_posts_shortcode');

function jed_latest_posts_shortcode() {

    $posts = '';

    $args = array(
        'post_type' => 'post',
        'posts_per_page' => 2,
        'ignore_sticky_posts' => 1
    );

    global $post;

    $new_posts = new WP_Query($args);

    while ($new_posts->have_posts()) {
        $new_posts->the_post();

        $blog_image_url = wp_get_attachment_url(get_post_thumbnail_id($post->ID), 'post-thumbnail');
        $full_blog_image_url = wp_get_attachment_url(get_post_thumbnail_id($post->ID), 'full');

        $posts .= '
            <div class="col-sm-6 text-box">
                <div class="row">
                  <div class="col-sm-4 portfolio-box portfolio-filter-photography portfolio-item">
	                  <a class="thumbnail-hover" href="' . $full_blog_image_url . '">
	                        <div class="image-link">
	                            <i class="fa-icon-search"></i>

	                            <img width="340" height="188" src="' . $blog_image_url . '" class="img-responsive center-block img-rounded-half">

	                        </div>
	                    </a>
                  </div>
                  <div class="col-sm-8">
                    <h3 class="title"><a href="' . get_permalink($post->ID) . '">' . get_the_title() . '</a></h3>
                    <div class="toolbar">
                      <a href="' . get_day_link( get_the_time('Y'), get_the_time('m'), get_the_time('d') ) . '" class="btn btn-link">
                        <i class="fa-icon-calendar-empty"></i>
                        <span>' . get_the_date(get_option( 'date_format' )) . '</span>
                      </a>
                    </div>
                    <p>' . get_the_excerpt() . '</p>
                    <a href="' . get_permalink($post->ID) . '" class="btn btn-contrast btn-bordered btn-xs">' . __('Read more', THEME_TEXT_DOMAIN) . '</a>
                  </div>
                </div>
              </div>
        ';

    }

    $returnContent = '
        <div class="row text-boxes portfolio-boxes">

            ' . $posts . '

        </div>';

    return $returnContent;

}

/**********************************************************************************************************************/
/*************************************************** Latest Work ******************************************************/
/**********************************************************************************************************************/
add_shortcode('jed_latest_work', 'jed_latest_work_shortcode');

function jed_latest_work_shortcode($atts, $content) {

    $portfolio_items = '';

    $args = array(
        'post_type' => 'portfolio',
        'posts_per_page' => 4
    );

    global $post;

    $portfolio = new WP_Query($args);

    while ($portfolio->have_posts()) {
        $portfolio->the_post();

        $portfolio_image_src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'portfolio-smaller-square-image');

        $portfolio_category = wp_get_post_terms($post->ID, 'portfolio_category');

        $portfolio_categories = array();

        foreach ( $portfolio_category  as $portfolio_category ) {
            $portfolio_categories[] = $portfolio_category->name;
        }

        $portfolio_items .= '
            <div class="col-sm-3 col-xs-6 no-mb-t-xs portfolio-box">
                <a href="' . get_permalink($post->ID) . '" class="image-link">
                  <i class="fa-icon-search"></i>
                  <img width="262" height="262" src="' . $portfolio_image_src[0] . '" class="img-responsive img-rounded center-block">
                </a>
                <h3 class="title">' . get_the_title() . '</h3>
                <p class="category">' . implode(', ', $portfolio_categories) . '</p>
              </div>
        ';

    }

    $return ='
        <div class="row portfolio-boxes">

            ' . $portfolio_items . '

        </div>'
    ;

    return $return;
}

/**********************************************************************************************************************/
/**************************************************** Testimonials ****************************************************/
/**********************************************************************************************************************/
add_shortcode('jed_testimonials', 'jed_testimonials_shortcode');

function jed_testimonials_shortcode($atts, $content) {
    $GLOBALS['testimonials'] = array();

    do_shortcode($content);

    $testimonials = array();
    $testimonialsNumber = 0;

    if ( is_array($GLOBALS['testimonials']) ) {
        $testimonialsNumber = count($GLOBALS['testimonials']);

        $index = 1;
        foreach ($GLOBALS['testimonials'] as $testimonial) {
            $active = ($index == 1 ? 'active' : '');

            $testimonials[] = '<div class="item quote ' . $active . '">' . $testimonial . '</div>';

            $index++;
        }
    }

    $carouselIndicators = '';

    for ( $i = 0; $i < $testimonialsNumber; $i++ ) {
        $active = ($i == 0 ? 'active' : '');
        $carouselIndicators .= '<li data-target="#carousel-testimonials" data-slide-to="' . $i . '" class="' . $active . '"></li>';
    }

    $returnContent = '
        <div class="row quotes">
            <div id="carousel-testimonials" class="carousel carousel-default slide carousel-auto">
                <div class="carousel-inner">
                    ' . implode('', $testimonials) . '
                </div>

                <ol class="carousel-indicators">
                  ' . $carouselIndicators . '
                </ol>
            </div>
        </div>
    ';

    return $returnContent;

}

add_shortcode('jed_testimonial', 'jed_testimonial_shortcode');

function jed_testimonial_shortcode($atts, $content) {

    extract(shortcode_atts(array(
        'author' => '',
        'company' => ''
    ), $atts));

    $authorCompany = !empty($company) ? ', ' . $company : '';

    $GLOBALS['testimonials'][] = '
        <div class="col-sm-12 text-center">
          <p class="lead">' . $content . '</p>
          <div class="author-wrapper">
            <p class="author">
              <strong>' . $author . '</strong>
              ' . $authorCompany . '
            </p>
          </div>
        </div>
    ';

}

/**********************************************************************************************************************/
/**************************************************** Circle Stats ****************************************************/
/**********************************************************************************************************************/
add_shortcode('jed_circle_stats', 'jed_circle_stats_shortcode');

function jed_circle_stats_shortcode($atts, $content) {
    $GLOBALS['circle_stats'] = array();

    do_shortcode($content);

    $circle_stats = array();

    if ( is_array($GLOBALS['circle_stats']) ) {
        foreach ($GLOBALS['circle_stats'] as $circle_stat) {
            $circle_stats[] = $circle_stat;
        }
    }

    $returnContent = '
        <ul class="circle-stats">
            ' . implode('', $circle_stats) . '
        </ul>
    ';

    return $returnContent;
}

add_shortcode('jed_circle_stat', 'jed_circle_stat_shortcode');

function jed_circle_stat_shortcode($atts, $content) {

    extract(shortcode_atts(array(
        'title' => '',
        'percent' => '',
        'size' => 'medium'
    ), $atts));

    switch ($size) {
        case 'small' :
            $sizeClass = 'circle-stat-small';
            break;
        default :
            $sizeClass = '';
            break;
    }

    $circleContent = !empty($content) ? '<p class="description">' . $content . '</p>' : '';

    $GLOBALS['circle_stats'][] = '
        <li class="' . $sizeClass . '">
            <div class="circle-stat-wrapper">
              <div style="display:inline;width:200px;height:200px;">
                <input class="hidden" value="' . (int)$percent . '" data-fgColor="#8dc153" data-readOnly="true" data-stat="circle" data-thickness="0.13" type="text">
                </div>
              <p class="percent">' . (int)$percent . '%</p>
            </div>
            <h3 class="title">' . $title . '</h3>
            ' . $circleContent . '
          </li>
    ';

}

/**********************************************************************************************************************/
/**************************************************** Top Clients *****************************************************/
/**********************************************************************************************************************/
add_shortcode('jed_clients', 'jed_clients_shortcode');

function jed_clients_shortcode($atts, $content) {
    $GLOBALS['clients'] = array();

    do_shortcode($content);

    $clients = array();

    if ( is_array($GLOBALS['clients']) ) {
        foreach ($GLOBALS['clients'] as $client) {
            $clients[] = $client;
        }
    }

    $returnContent = '
        <div class="client-slideshow cycle-slideshow" data-cycle-carousel-fluid="true" data-cycle-fx="carousel" data-cycle-timeout="2000">
            ' . implode('', $clients) . '
        </div>
    ';

    return $returnContent;
}

add_shortcode('jed_client', 'jed_client_shortcode');

function jed_client_shortcode($atts, $content) {
    extract(shortcode_atts(array(
        'img' => ''
    ), $atts));

    $GLOBALS['clients'][] = '<img alt="" src="' . $img . '" />';

}

/**********************************************************************************************************************/
/***************************************************** Timeline *******************************************************/
/**********************************************************************************************************************/

add_shortcode('jed_timeline', 'jed_timeline_shortcode');

function jed_timeline_shortcode($atts, $content) {
    $GLOBALS['timeline_items'] = array();

    do_shortcode($content);

    $returnContent = '';
    $timeline_items = '';

    if ( is_array($GLOBALS['timeline_items']) ) {
        foreach ( $GLOBALS['timeline_items'] as $timeline_item) {
            $timeline_items[] = $timeline_item;
        }
    }

    $returnContent = '
        <ol class="timeline">
            ' . implode('', $timeline_items) . '
        </ol>
    ';

    return $returnContent;
}

add_shortcode('jed_timeline_item', 'jed_timeline_item_shortcode');

function jed_timeline_item_shortcode($atts, $content) {
    extract(shortcode_atts(array(
        'img' => '',
        'time' => '',
        'title' => ''
    ), $atts));

    $GLOBALS['timeline_items'][] = '
        <li>
            <div class="image"><img width="120" height="120" src="' . $img . '"></div>
            <p class="time">' . $time . '</p>
            <h3 class="title">' . $title . '</h3>
            <p>' . $content . '</p>
          </li>
    ';
}

/**********************************************************************************************************************/
/****************************************************** Tables ********************************************************/
/**********************************************************************************************************************/
add_shortcode('jed_table', 'jed_table_shortcode');

function jed_table_shortcode($atts, $content) {
    extract(shortcode_atts(array(
        'type' => 'basic',
        'rows' => '',
        'hover_rows' => ''
    ), $atts));

    $GLOBALS['table_columns'] = array();

    do_shortcode($content);

    $columns = array();
    $tableHead = '';

    $rowsArray = array();
    $rowsMarkup = array();

    $index = 1;
    foreach( $GLOBALS['table_columns'] as $table_column ) {
        $tableHead[] = '
            <th>
                <i class="' . $table_column['icon'] . ' text-contrast"></i>
                ' . $table_column['name'] . '
            </th>
        ';

        for ( $i = 0; $i < $rows; $i++ ) {
            $rowsArray[$i][] = $table_column['rows'][$i];
        }

        $index++;
    }

    foreach ( $rowsArray as $row ) {
        $rowsMarkup[] = '
            <tr>' . implode('', $row) . '</tr>
        ';
    }

    $typeClass = '';
    switch ($type) {
        case 'stripped':
            $typeClass = 'table-striped';
            break;
        case 'bordered':
            $typeClass = 'table-bordered';
            break;
        case 'condensed':
            $typeClass = 'table-condensed';
            break;
        default:
            $typeClass = '';
    }

    $hoverClass = '';
    if ( $hover_rows == 'on') {
        $hoverClass = 'table-hover';
    }

    $returnContent = '
        <div class="table-responsive">
          <table class="table ' . $hoverClass . ' ' . $typeClass . '">
            <thead>
                  <tr>
                    ' . implode('', $tableHead ) . '
                  </tr>
            </thead>
            <tbody>
                ' . implode('', $rowsMarkup) . '
            </tbody>
          </table>
        </div>
    ';


    return $returnContent;
}

add_shortcode('jed_table_column', 'jed_table_column_shortcode');

function jed_table_column_shortcode($atts, $content) {
    $GLOBALS['table_rows'] = array();

    extract(shortcode_atts(array(
        'icon' => '',
        'name' => '',
    ), $atts));

    do_shortcode($content);

    $table_rows = array();
    foreach ($GLOBALS['table_rows'] as $table_row) {
        $table_rows[] = '<td>' . $table_row . '</td>';
    }

    $GLOBALS['table_columns'][] = array('rows' => $table_rows, 'name' => $name, 'icon' => $icon);


}

add_shortcode('jed_table_row', 'jed_table_row_shortcode');

function jed_table_row_shortcode($atts, $content) {

    $GLOBALS['table_rows'][] =  $content;
}

/**********************************************************************************************************************/
/****************************************************** Lists *********************************************************/
/**********************************************************************************************************************/
add_shortcode('jed_list', 'jed_list_shortcode');

function jed_list_shortcode($atts, $content) {
    extract(shortcode_atts(array(
        'icons_style' => 'bordered',
        'icons_size' => 'tiny',
        'icons_shape' => ''
    ), $atts));

    $GLOBALS['list_rows'] = array();

    do_shortcode($content);

    $rows = array();
    $returnContent = '';

    if ( is_array($GLOBALS['list_rows'])) {
        foreach ( $GLOBALS['list_rows'] as $row) {

            $tiny_icon = false;
            $size_class = '';
            $style_class = '';
            $shape_class = '';
            $color = '';
            $background_color = '';

            switch ($icons_size) {
                case 'small':
                    $size_class = '';
                    break;
                case 'medium':
                    $size_class = 'icon-md';
                    break;
                case 'large':
                    $size_class = 'icon-lg';
                    break;
                default:
                    $tiny_icon = true;
                    break;
            }

            if ( $icons_style == 'bordered' ) {
                $style_class = 'icon-bordered';
                $color = $row['color'];
                $background_color = '#fff';
            }
            else {
                $style_class = '';
                $color = '#fff';
                $background_color = $row['color'];
            }

            if ( $icons_shape == 'squared' ) {
                $shape_class = 'icon-rounded-square';
            }
            else {
                $shape_class = 'icon-circle';
            }

            if ($tiny_icon) {
                $icon = '<span style="color: ' . $row['color'] . '">' . $row['icon'] . '</span>';
            }
            else {
                $icon = '<div class="icon-wrap ' . $shape_class . ' ' . $size_class . ' ' . $style_class . '" style="background-color: ' . $background_color . '; color: ' . $color . '; border-color: ' . $color . ';">
                            ' . $row['icon'] . '
                        </div>';
            }


            $rows[] = '<li>' . $icon . $row['content'] . '</li>';

        }
    }

    $returnContent = '
        <ul class="list-unstyled">
            ' . implode('', $rows) . '
        </ul>
    ';

    return $returnContent;
}

add_shortcode('jed_list_row', 'jed_list_row_shortcode');

function jed_list_row_shortcode($atts, $content) {
    extract(shortcode_atts(array(
        'icon' => '',
        'color' => '',
    ), $atts));

    $GLOBALS['list_rows'][] = array(
        'icon' => '<i class="' . $icon . '"></i>',
        'color' => $color,
        'content' =>$content
    );
}

/**********************************************************************************************************************/
/***************************************************** Dropcaps *******************************************************/
/**********************************************************************************************************************/
add_shortcode('jed_dropcap', 'jed_dropcap_shortcode');

function jed_dropcap_shortcode($atts, $content) {
    extract(shortcode_atts(array(
        'style' => 'filled'
    ), $atts));

    $cap = substr($content, 0, 1);
    // delete the first letter
    $content = substr_replace($content, '', 0, 1);

    $styleClass = ($style == 'normal' ? 'dropcap' : 'dropcap-contrast');

    $returnContent = '
        <p class="' . $styleClass . ' text-justify">
          <span class="first-letter">' . $cap . '</span>
          ' . $content . '
        </p>
    ';

    return $returnContent;
}

/**********************************************************************************************************************/
/************************************************* Browser Wrapper ****************************************************/
/**********************************************************************************************************************/
add_shortcode('jed_browser_wrapper', 'jed_browser_wrapper_shortcode');

function jed_browser_wrapper_shortcode($atts, $content) {
    extract(shortcode_atts(array(
        'link' => '',
    ), $atts));

    $returnContent = '
        <div class="browser-window">
          <div class="browser-header">
            <div class="browser-header-controls">
              <span class="red"></span>
              <span class="orange"></span>
              <span class="green"></span>
            </div>
            <div class="browser-address-bar">
              <div class="browser-title">
                <a href="' . $link . '">' . $link . '</a>
              </div>
            </div>
          </div>
          <div class="browser-content">
            ' . $content . '
          </div>
        </div>
    ';

    return $returnContent;
}


/**********************************************************************************************************************/
/**************************************************** Blockquote ******************************************************/
/**********************************************************************************************************************/
add_shortcode('jed_blockquote', 'jed_blockquote_shortcode');

function jed_blockquote_shortcode($atts, $content) {
    extract(shortcode_atts(array(
        'author' => ''
    ), $atts));

    $returnContent = '
        <blockquote>
          <p>' . $content . '</p>
          <small>
            ' . $author . '
          </small>
        </blockquote>
    ';

    return $returnContent;
}


/**********************************************************************************************************************/
/************************************************** Columns Wrapper ***************************************************/
/**********************************************************************************************************************/
add_shortcode('jed_columns_wrapper', 'jed_columns_wrapper_shortcode');

function jed_columns_wrapper_shortcode($atts, $content) {
    $GLOBALS['columns'] = array();

    do_shortcode($content);

    if ( is_array($GLOBALS['columns'])) {
        $columns = array();

        foreach ( $GLOBALS['columns'] as $column) {
            $columns[] = $column;
        }
    }

    $returnContent = '
        <div class="row">
            ' . implode('', $columns) . '
        </div>
    ';

    return $returnContent;
}


/**********************************************************************************************************************/
/************************************************ One fourths column **************************************************/
/**********************************************************************************************************************/
add_shortcode('jed_one_fourth_column', 'jed_one_fourth_column_shortcode');

function jed_one_fourth_column_shortcode($atts, $content) {
    $returnContent = '
        <div class="col-md-3">' . do_shortcode( $content ) . '</div>
    ';

    $GLOBALS['columns'][] = $returnContent;
}


/**********************************************************************************************************************/
/************************************************ One thirds column ***************************************************/
/**********************************************************************************************************************/
add_shortcode('jed_one_third_column', 'jed_one_third_column_shortcode');

function jed_one_third_column_shortcode($atts, $content) {
    $returnContent = '
        <div class="col-md-4">' . do_shortcode( $content ) . '</div>
    ';

    $GLOBALS['columns'][] = $returnContent;
}


/**********************************************************************************************************************/
/************************************************ Two thirds column ***************************************************/
/**********************************************************************************************************************/
add_shortcode('jed_two_thirds_column', 'jed_two_thirds_column_shortcode');

function jed_two_thirds_column_shortcode($atts, $content) {
    $returnContent = '
        <div class="col-md-8">' . do_shortcode( $content ) . '</div>
    ';

    $GLOBALS['columns'][] = $returnContent;
}


/**********************************************************************************************************************/
/*********************************************** Two fourths column *************************************************/
/**********************************************************************************************************************/
add_shortcode('jed_two_fourths_column', 'jed_two_fourths_column_shortcode');

function jed_two_fourths_column_shortcode($atts, $content) {
    $returnContent = '
        <div class="col-md-9">' . do_shortcode( $content ) . '</div>
    ';

    $GLOBALS['columns'][] = $returnContent;
}


/**********************************************************************************************************************/
/************************************************* One half column ****************************************************/
/**********************************************************************************************************************/
add_shortcode('jed_one_half_column', 'jed_one_half_column_shortcode');

function jed_one_half_column_shortcode($atts, $content) {
    $returnContent = '
        <div class="col-md-6">' . do_shortcode( $content ) . '</div>
    ';

    $GLOBALS['columns'][] = $returnContent;
}

/**********************************************************************************************************************/
/*********************************************** Full width section ***************************************************/
/**********************************************************************************************************************/
add_shortcode('jed_full_width_section', 'jed_full_width_section_shortcode');

function jed_full_width_section_shortcode($atts, $content) {

    $returnContent = '
        <div class="container">
            ' . do_shortcode( $content ) . '
        </div>
    ';

    return $returnContent;
}

