<?php
// Template Name: Slider 2 (Demo Only)

get_template_part('templates/header') ?>

<div id='main' role='main'>

    <div class='carousel-contrast flexslider' id='carousel-example-2'>
        <ul class='slides'>

            <?php

            $args = array(
                'post_type' => 'slides'
            );

            $slides = new WP_Query($args);

            if ( $slides->have_posts()) :
                while ( $slides->have_posts()) :
                    $slides->the_post();

                    $first_caption = get_post_meta($post->ID, 'first_caption', true);
                    $second_caption = get_post_meta($post->ID, 'second_caption', true);
                    $show_slide_button = get_post_meta($post->ID, 'show_slide_button', true);
                    $button_name = get_post_meta($post->ID, 'button_name', true);
                    $button_link = get_post_meta($post->ID, 'button_link', true);
                    $slide_background = get_post_meta($post->ID, 'slide_background', true);

                    $first_caption_animation = get_post_meta($post->ID, 'first_caption_animation', true);
                    $second_caption_animation = get_post_meta($post->ID, 'second_caption_animation', true);
                    $button_animation = get_post_meta($post->ID, 'button_animation', true);

                    ?>

                    <li class='item forest' style="background: transparent url(<?php echo $slide_background ?>) center center">
                        <div class='container'>
                            <div class='row'>
                                <div class='col-lg-12 animate'>
                                    <h1 class='big <?php echo $first_caption_animation ?> animated'>
                                        <span><?php echo $first_caption ?></span>
                                    </h1>

                                    <p class='normal <?php echo $second_caption_animation ?> animated'>
                                        <span><?php echo $second_caption ?></span>
                                    </p>

                                    <p class='fadeInRight animated'>
                                        <?php if ($show_slide_button == 'show_button') : ?>
                                    <div class="button-wrapper squared-button-wrapper <?php echo $button_animation ?> animated">
                                        <a class='btn btn-squared btn-contrast btn-lg' href='<?php echo $button_link ?>'><?php echo $button_name ?></a>
                                    </div>
                                    <?php endif; ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </li>

                <?php

                endwhile;
            endif;

            ?>
        </ul>
    </div>

    <div id='main-content'>
        <?php
        if ( have_posts() ) {
            while ( have_posts() ) {
                the_post();

                the_content();
            }
        }
        ?>
    </div>

<?php get_template_part('templates/footer') ?>