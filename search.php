<?php

global $NHP_Options;
$options = $NHP_Options->options;

custom_excerpt(50);

?>

<?php get_template_part('templates/header') ?>

<div id='main' role='main'>

<?php get_template_part('templates/top_section') ?>

    <div id='main-content'>
        <div class='container'>
            <div class='row'>
                <div class='col-md-push-9 col-sm-push-8 col-sm-4 col-md-3'>

                    <?php get_template_part('templates/sidebar') ?>

                </div>

                <div class='col-md-pull-3 col-sm-pull-4 col-sm-8 col-md-9'>
                    <div class='text-boxes portfolio-boxes'>

                        <?php

                        if ( have_posts() ) {
                            while ( have_posts() ) {
                                the_post();

                                $blog_image_src = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'small-blog-image');
                                $big_blog_image_src = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');
                                $comments_number = get_comments_number();

                                ?>

                                <div class='row text-box text-box-title-above'>
                                    <div class='col-sm-12'>
                                        <h2 class='title'><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h2>

                                        <div class='toolbar'>
                                            <a class='btn btn-link' href='<?php echo get_day_link( get_the_time('Y'), get_the_time('m'), get_the_time('d') ) ?>'>
                                                <i class='fa-icon-calendar-empty'></i>
                                                <span><?php echo get_the_date('M d, Y') ?></span>
                                            </a>
                                            <a class='btn btn-link' href='<?php echo home_url() . '/?author=' ?><?php the_author_meta('ID'); ?>'>
                                                <i class='fa-icon-user'></i>
                                                <span><?php the_author() ?></span>
                                            </a>
                                            <a class='btn btn-link' href='<?php the_permalink() ?>#comments'>
                                                <i class='fa-icon-comments'></i>
                                                <span><?php echo $comments_number ?> <?php _e('comments', THEME_TEXT_DOMAIN) ?></span>
                                            </a>
                                        </div>

                                        <div class='row'>

	                                        <?php if ( !empty($blog_image_src[0]) ) :  ?>

                                            <div class='col-sm-3 portfolio-box portfolio-filter-photography portfolio-item'>
                                                <a class="thumbnail-hover" href="<?php echo $big_blog_image_src[0] ?>">
                                                    <div class='image-link'>
                                                        <i class='fa-icon-search'></i>

                                                        <img class="img-responsive center-block img-rounded-half" width="189" height="129" src="<?php echo $blog_image_src[0] ?>" alt="">

                                                    </div>
                                                </a>
                                            </div>

		                                    <?php endif; ?>

                                            <div class='col-sm-9'>
                                                <p><?php the_excerpt() ?></p>
                                                <a class='btn btn-contrast btn-bordered btn-xs' href='<?php the_permalink() ?>'><?php _e('Read more', THEME_TEXT_DOMAIN) ?></a>
                                            </div>
                                        </div>

                                        <hr class='hr-half'>
                                    </div>
                                </div>

                            <?php

                            }
                        }

                        ?>

                    </div>

                    <?php get_template_part('templates/pagination') ?>

                </div>
            </div>
        </div>
    </div>

<?php get_template_part('templates/footer') ?>