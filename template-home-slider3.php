<?php
// Template Name: Slider 3 (Demo Only)

get_template_part('templates/header') ?>

<div id='main' role='main'>

    <div class='carousel-image flexslider' id='carousel-example-2'>
        <ul class='slides'>

            <?php

            $args = array(
                'post_type' => 'slides'
            );

            $slides = new WP_Query($args);

            if ( $slides->have_posts()) :
                while ( $slides->have_posts()) :
                    $slides->the_post();

                    $first_caption = get_post_meta($post->ID, 'first_caption', true);
                    $second_caption = get_post_meta($post->ID, 'second_caption', true);
                    $show_slide_button = get_post_meta($post->ID, 'show_slide_button', true);
                    $button_name = get_post_meta($post->ID, 'button_name', true);
                    $button_link = get_post_meta($post->ID, 'button_link', true);
                    $slide_image = get_post_meta($post->ID, 'slide_image', true);

                    $first_caption_animation = get_post_meta($post->ID, 'first_caption_animation', true);
                    $second_caption_animation = get_post_meta($post->ID, 'second_caption_animation', true);
                    $button_animation = get_post_meta($post->ID, 'button_animation', true);

                    ?>

                    <li class='item'>
                        <div class='container'>
                            <div class='row pos-rel'>
                                <div class='col-sm-12 col-md-8 animate'>
                                    <h1 class='big <?php echo $first_caption_animation ?> animated'><?php echo $first_caption ?></h1>
                                    <p class='normal <?php echo $second_caption_animation ?> animated'><?php echo $second_caption ?></p>

                                    <?php if ($show_slide_button == 'show_button') : ?>
                                        <a class='btn btn-bordered btn-white btn-lg <?php echo $button_animation ?> animated' href='<?php echo $button_link ?>'><?php echo $button_name ?></a>
                                    <?php endif; ?>
                                </div>
                                <div class='col-sm-4 animate pos-sta hidden-xs hidden-sm animated'>
                                    <img class="img-responsive fadeInUpBig animated" style="position:absolute;bottom:0;right:15px;" alt="iPhone" width="331" height="370" src="<?php echo $slide_image ?>" />
                                </div>
                            </div>
                        </div>
                    </li>

                <?php

                endwhile;
            endif;

            ?>
        </ul>
    </div>

    <div id='main-content'>
        <?php
        if ( have_posts() ) {
            while ( have_posts() ) {
                the_post();

                the_content();
            }
        }
        ?>
    </div>

<?php get_template_part('templates/footer') ?>