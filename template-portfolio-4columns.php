<?php

// Template Name: Portfolio 4 Columns

global $NHP_Options;
$options = $NHP_Options->options;

?>

<?php get_template_part('templates/header') ?>

<div id='main' role='main'>

    <?php get_template_part('templates/top_section') ?>

    <div id='main-content'>
        <div class='container'>

            <?php get_template_part('templates/portfolio_filter') ?>

            <div class='row portfolio-boxes' id='portfolio-container'>

                <?php

                $args = array(
                    'post_type' => 'portfolio',
                    'posts_per_page' => $options['portfolio-number'],
                    'paged' => get_query_var('paged')
                );

                query_posts($args);

                if ( have_posts()) :
                    while ( have_posts()) :
                    the_post();

                    $portfolio_image = wp_get_attachment_image( get_post_thumbnail_id($post->ID), 'portfolio-4columns');
                    $portfolio_image_src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );

                    $portfolio_category = wp_get_post_terms($post->ID, 'portfolio_category');

                    $portfolio_categories = array();
                    $portfolio_categories_slugs = array();

                        foreach ( $portfolio_category  as $portfolio_category ) {
                            $portfolio_category_slug = str_replace(' ', '_', $portfolio_category->name);

                            $portfolio_categories_slugs[] = $portfolio_category_slug;
                            $portfolio_categories[] = $portfolio_category->name;
                        }

                ?>

                <div class='col-sm-3 portfolio-box portfolio-item <?php echo implode(' ', $portfolio_categories_slugs) ?>'>
                    <a class="thumbnail-hover" href='<?php echo $portfolio_image_src[0] ?>'>
                        <div class='image-link'>
                            <i class='fa-icon-search'></i>

                            <?php echo $portfolio_image ?>

                        </div>
                    </a>

                    <a href="<?php the_permalink() ?>"><h3 class='title'><?php the_title() ?></h3></a>
                    <p class='category'><?php echo implode(', ', $portfolio_categories) ?></p>
                </div>

                <?php

                    endwhile;
                endif;

                ?>

            </div>

            <?php get_template_part('templates/pagination') ?>
        </div>
    </div>

<?php get_template_part('templates/footer') ?>