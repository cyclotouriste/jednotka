<?php get_template_part('templates/header') ?>

<div id="main" role="main">

    <?php get_template_part('templates/top_section') ?>

    <?php

    $portfolio_category = array();

    $GLOBALS['portfolio_category'] = $portfolio_category;

    ?>

    <div id='main-content'>
        <div class='container'>
            <div class='row'>

                <?php

                if ( have_posts() ) {
                    while ( have_posts() ) {
                        the_post();

                        $portfolio_image_src = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');
                        $portfolio_category = wp_get_post_terms($post->ID, 'portfolio_category');

                        $portfolio_categories = array();

                        foreach ( $portfolio_category  as $portfolio_category ) {
                            $portfolio_categories[] = $portfolio_category->name;
                        }

                        $post_format = get_post_format($post->ID);

                        $gallery = get_post_meta($post->ID, 'portfolio_gallery_image', true);
                        $sidebar = get_post_meta($post->ID, 'portfolio_sidebar', true);
                        $sidebar_title = get_post_meta($post->ID, 'portfolio_sidebar_title', true);

                        $gallery = explode(',', $gallery);

                        if ($post_format === 'video') {
                            // get post video
                            $video = '';
                            $video_custom_meta = get_post_meta($post->ID, 'portfolio_video', true);

                            if ( !empty($video_custom_meta) ) {
                                $video = $video_custom_meta;
                            }

                            $post_media =
                                '<div style="clear: both"></div>
                                    <div class="portfolio-media">' . $video . '</div>';
                        }
                        elseif ($post_format === 'gallery') {

                            $images = array();

                            $uniqueId = rand(1000, 9999);

                            $index = 0;
                            foreach($gallery as $image) {

                                $fullImage = $image;

                                $active = $index === 0 ? ' active' : '';

                                $post_image = '-848x364';

                                $image = explode('.', $image);

                                $imageExtension = $image[count($image) - 1];

                                unset($image[count($image) - 1]);

                                $image = implode('.', $image);

                                $imageName = $image . $post_image . '.' . $imageExtension;

                                $images[] = '<div class="item' . $active . '"><a class="stylish-image-gallery" href="' . $fullImage . '"><img src="' . $imageName . '" /></a></div>';
                                $index++;
                            }

//                                    var_dump($images);

                            $post_media = '
                                <div class="portfolio-media">
                                    <div id="carousel-' . $uniqueId . '" class="post-carousel carousel slide" data-ride="carousel">
                                      <!-- Indicators -->


                                      <!-- Wrapper for slides -->
                                      <div class="carousel-inner">
                                        ' . implode('', $images) . '
                                      </div>

                                      <!-- Controls -->
                                      <a class="left carousel-control" href="#carousel-' . $uniqueId . '" data-slide="prev">
                                        <span><i class="fa-icon-chevron-left"></i></span>
                                      </a>
                                      <a class="right carousel-control" href="#carousel-' . $uniqueId . '" data-slide="next">
                                        <span><i class="fa-icon-chevron-right"></i></span>
                                      </a>
                                    </div>
                                </div>
                                ';


                        }
                        elseif ($post_format == 'link') {
                            $post_media = '';
                        }
                        else {
                            $post_media =
                                '<div class="portfolio-media">
                                    <img class="img-responsive center-block img-rounded-half" width="848" height="364" src="' . $portfolio_image_src[0] . '">
                                </div>';
                        }
                ?>

                <div class='col-md-push-9 col-sm-push-8 col-sm-4 col-md-3'>
                    <nav class='sidebar'>
                        <button class='btn btn-block btn-contrast sidebar-toggle' data-target='.sidebar-collapse' data-toggle='collapse' type='button'>
                            <span class='sr-only'><?php _e('Toggle navigation', THEME_TEXT_DOMAIN) ?></span>
                            <span class='icon-bar'></span>
                            <span class='icon-bar'></span>
                            <span class='icon-bar'></span>
                        </button>
                        <div class='sidebar-collapse collapse'>
                            <div class='box'>
                                <h3 class='title'><?php _e('Info', THEME_TEXT_DOMAIN) ?></h3>
                                <ul class='list-unstyled list-padded'>
                                    <li>
                                        <i class='fa-icon-calendar-empty text-contrast fa-icon-fixed-width'></i>
                                        <?php the_date('M d, Y') ?>
                                    </li>
                                    <li>
                                        <i class="fa-icon-pencil text-contrast fa-icon-fixed-width"></i>
                                        <?php echo implode(', ', $portfolio_categories) ?>
                                    </li>
                                </ul>

                                <h3><?php echo $sidebar_title ?></h3>

                                <div>
                                    <?php echo do_shortcode(nl2br($sidebar)); ?>
                                </div>
                            </div>
                        </div>
                    </nav>
                </div>

                <div class='col-md-pull-3 col-sm-pull-4 col-sm-8 col-md-9'>
                    <div class='row'>
                        <div class='col-sm-12'>
                            <?php echo $post_media ?>

                            <?php the_content() ?>
                        </div>
                    </div>

                </div>

                <?php
                        }
                    }
                ?>
            </div>
        </div>

        <div class='container'>
            <div class='row'>

                <div class='col-md-9'>

                    <?php get_template_part('templates/related_work') ?>

                </div>

            </div>
        </div>
    </div>

<?php get_template_part('templates/footer') ?>