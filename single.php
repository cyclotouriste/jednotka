<?php

?>

<?php get_template_part('templates/header') ?>

<div id='main' role='main'>

    <?php get_template_part('templates/top_section') ?>

    <div id='main-content'>
        <div class='container'>
            <div class='row'>

                <div class='col-md-push-9 col-sm-push-8 col-sm-4 col-md-3'>

                <?php get_template_part('templates/sidebar') ?>

                </div>

                <div class='col-md-pull-3 col-sm-pull-4 col-sm-8 col-md-9'>

                    <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                        <?php

                        if ( have_posts() ) {
                            while ( have_posts() ) {
                                the_post();

                                $post_format = get_post_format($post->ID);

                                $blog_image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'large-blog-image');
                                $full_blog_image_src = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');

                                $gallery = get_post_meta($post->ID, 'post_gallery_image', true);

                                $gallery = explode(',', $gallery);

                                if ($post_format === 'video') {
                                    // get post video
                                    $video = '';
                                    $video_custom_meta = get_post_meta($post->ID, 'post_video', true);

                                    if ( !empty($video_custom_meta) ) {
                                        $video = $video_custom_meta;
                                    }

                                    $post_media =
                                        '<div style="clear: both"></div>
                                            <div class="portfolio-media">' . $video . '</div>';
                                }
                                elseif ($post_format === 'gallery') {

                                    $images = array();

                                    $uniqueId = rand(1000, 9999);

                                    $index = 0;
                                    foreach($gallery as $image) {

                                        $fullImage = $image;

                                        $active = $index === 0 ? ' active' : '';

                                        $post_image = '-848x364';

                                        $image = explode('.', $image);

                                        $imageExtension = $image[count($image) - 1];

                                        unset($image[count($image) - 1]);

                                        $image = implode('.', $image);

                                        $imageName = $image . $post_image . '.' . $imageExtension;

                                        $images[] = '<div class="item' . $active . '"><a class="stylish-image-gallery" href="' . $fullImage . '"><img src="' . $imageName . '" /></a></div>';
                                        $index++;
                                    }

//                                    var_dump($images);

                                    $post_media = '
                                    <div class="portfolio-media">
                                        <div id="carousel-' . $uniqueId . '" class="post-carousel carousel slide" data-ride="carousel">
                                          <!-- Indicators -->


                                          <!-- Wrapper for slides -->
                                          <div class="carousel-inner">
                                            ' . implode('', $images) . '
                                          </div>

                                          <!-- Controls -->
                                          <a class="left carousel-control" href="#carousel-' . $uniqueId . '" data-slide="prev">
                                            <span><i class="fa-icon-chevron-left"></i></span>
                                          </a>
                                          <a class="right carousel-control" href="#carousel-' . $uniqueId . '" data-slide="next">
                                            <span><i class="fa-icon-chevron-right"></i></span>
                                          </a>
                                        </div>
                                    </div>
                                    ';

                                }
                                else {
                                    $post_media =
                                        '<div class="portfolio-box portfolio-filter-photography portfolio-item">
                                            <a class="thumbnail-hover" href="' . $full_blog_image_src[0] . '">
                                            <div class="image-link">
                                                <i class="fa-icon-search"></i>

                                                <img class="img-responsive center-block img-rounded-half" width="848" height="364" src="' . $blog_image[0] . '">

                                            </div>
                                        </a>
                                    </div>';
                                }
                                ?>

                                <div class='text-boxes portfolio-boxes'>
                                    <div class='row text-box text-box-title-above text-box-big-image'>
                                        <div class='col-sm-12'>
                                            <h2 class='title'><?php the_title() ?></h2>
                                            <div class='toolbar'>
                                                <a class='btn btn-link' href='<?php echo get_day_link( get_the_time('Y'), get_the_time('m'), get_the_time('d') ) ?>'>
                                                    <i class='fa-icon-calendar-empty'></i>
                                                    <span><?php the_date('M d, Y') ?></span>
                                                </a>
                                                <a class='btn btn-link' href='<?php echo home_url() . '/?author=' ?><?php the_author_meta('ID'); ?>'>
                                                    <i class='fa-icon-user'></i>
                                                    <span><?php the_author() ?></span>
                                                </a>
                                                <a class='btn btn-link' href='#comments'>
                                                    <i class='fa-icon-comments'></i>
                                                    <span><?php echo get_comments_number() ?> <?php _e('comments', THEME_TEXT_DOMAIN) ?></span>
                                                </a>
                                            </div>
                                            <div class='row'>
                                                <div class='col-sm-12'>

                                                    <?php echo $post_media ?>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class='row text-box'>
                                        <div class='col-sm-12'>
                                            <?php the_content() ?>
                                        </div>
                                    </div>

                                
                                </div>

                            <?php
                            }
                        }
                        ?>
                    </div>

                </div>
            </div>
        </div>
    </div>


<?php get_template_part('templates/footer') ?>