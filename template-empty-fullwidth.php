<?php

// Template Name: Empty Fullwidth

?>

<?php get_template_part('templates/header') ?>

    <div id='main' role='main'>

        <?php get_template_part('templates/top_section') ?>

        <div id='main-content' class="full-width-main-content">
            <?php
            if ( have_posts() ) {
                while ( have_posts() ) {
                    the_post();

                    the_content();
                }
            }
            ?>
        </div>

<?php get_template_part('templates/footer') ?>