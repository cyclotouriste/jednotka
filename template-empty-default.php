<?php
// Template name: Empty Default

get_template_part('templates/header') ?>

<div id='main' role='main'>

<?php get_template_part('templates/top_section') ?>

    <div id='main-content'>
        <div class='container'>
            <div class='row'>
                <div class='col-md-push-9 col-sm-push-8 col-sm-4 col-md-3'>

                    <?php get_template_part('templates/sidebar') ?>

                </div>

                <div class='col-md-pull-3 col-sm-pull-4 col-sm-8 col-md-9'>

                    <?php

                    if ( have_posts() ) {
                        while ( have_posts() ) {
                            the_post();

                            the_content();
                        }
                    }

                    ?>

                </div>
            </div>
        </div>
    </div>

<?php get_template_part('templates/footer') ?>