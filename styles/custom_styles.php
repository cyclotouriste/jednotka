<?php

global $NHP_Options;
$options = $NHP_Options->options;

$google_fonts_array = $options['google-fonts'];

$google_fonts_array = explode('|', $google_fonts_array);

$google_fonts = '';

foreach ($google_fonts_array as $google_font) {
    $google_fonts .= '@import url(' . $google_font . ');';
}

$color_scheme = $options['color-scheme'];
$transparent_color_scheme = get_transparent_color($color_scheme, 0.1);

?>

<style type="text/css">

    <?php echo $google_fonts ?>

    ::selection {
        background-color: <?php echo $color_scheme ?>;
        color: #fff;
    }

    ::-moz-selection {
        background-color: <?php echo $color_scheme ?>;
        color: #fff;
    }

    code {
        color: <?php echo $color_scheme ?>;
        background-color: <?php echo $transparent_color_scheme ?>;
    }

    /*------------------------------------*\
    $LINKS
    \*------------------------------------*/
    a, a:active, a:link, a:visited, .logo-link {
        color: <?php echo $options['color-scheme'] ?>;
    }

    a:hover, a:focus, a.logo-link:hover, a.logo-link:active {
        color: <?php echo $options['color-scheme'] ?>;
        text-decoration: underline;
    }

    a.link-contrast {
        color: <?php echo $options['color-scheme'] ?>;
    }

    a.link-contrast:hover {
        color: <?php echo $options['color-scheme'] ?>;
    }

    /*------------------------------------*\
    $QUOTES
    \*------------------------------------*/
    body, .lead.lead-md, .lead.lead-sm, .lead.lead-xs, h1 small, h2 small, h3 small, h4 small, h5 small, h6 small, #main-content-header .title small, .quotes .quote .lead,
    .panel.pricing-table .panel-body .price, .text-boxes .text-box .toolbar .btn-link, .profile-boxes .profile-box .position,
    .accordion .accordion-toggle, .tabbable .nav-tabs > li > a {
        font-family: <?php echo $options['font-family-content'] ?>;
    }

    .lead, h1, h2, h3, h4, h5, h6, .panel.pricing-table .panel-body .price .currency, .carousel-blur .big, .carousel-blur .normal, .carousel-contrast .big,
    .carousel-contrast .carousel-blur .normal, .carousel-blur .carousel-contrast .normal, .carousel-contrast .carousel-image .normal,
    .carousel-image .carousel-contrast .normal, .carousel-contrast .normal, .carousel-image .big, .carousel-image .normal, ol.timeline li .time,
    ul.circle-stats li .percent, body.error-page, body.countdown-page {
        font-family: <?php echo $options['font-family-heading'] ?>;
    }

    #wrapper {
        background-image: url(<?php echo $options['top-image'] ?>);
    }

    #wrapper.homepage-wrapper {
        background-image: url(<?php echo $options['slider-background'] ?>);
    }

    body.boxed {
        background-image: url(<?php echo $options['background-pattern'] ?>);
    }

    #header .navbar-default .navbar-brand {
        height: 90px;
    }

    .text-contrast {
        color: <?php echo $options['color-scheme'] ?> !important; }

    .contrast-bg {
        background-color: <?php echo $options['color-scheme'] ?> !important; }

    .contrast-border {
        border-color: <?php echo $options['color-scheme'] ?> !important; }

    .label-contrast {
        background-color: <?php echo $options['color-scheme'] ?> !important; }

    .badge-contrast {
        background-color: <?php echo $options['color-scheme'] ?> !important; }

    .progress-bar-contrast {
        background: <?php echo $options['color-scheme'] ?>; }

    #scroll-to-top {
        color: <?php echo $options['color-scheme'] ?>;
    }

    /*------------------------------------*\
    $HEADER
    \*------------------------------------*/

    #header .navbar-default .navbar-toggle:hover, #header .navbar-default .navbar-toggle:focus {
        border-color: <?php echo $options['color-scheme'] ?>;
        background-color: <?php echo $options['color-scheme'] ?>;
    }

    #header .navbar-default .navbar-nav > .open .dropdown-menu > li > a {
        color: #FFFFFF;
    }

    #header .navbar-default .navbar-nav > li .dropdown-menu > .active > a {
        background-color: rgba(0, 0, 0, 0.1);
    }

    #header .navbar-default .navbar-nav > li.active > a,
    #header .navbar-default .navbar-nav > li.active > a:hover,
    #header .navbar-default .navbar-nav > li.active > a:focus {
        background-color: <?php echo $options['color-scheme'] ?>;
    }

    *, *:before, *:after {
        -moz-box-sizing: border-box;
    }

    *, *:before, *:after {
        -moz-box-sizing: border-box;
    }

    #header .navbar-default .navbar-nav > li.open > a,
    #header .navbar-default .navbar-nav > li.open > a:hover,
    #header .navbar-default .navbar-nav > li.open > a:focus,
    #header .navbar-default .navbar-nav > li.active > a,
    #header .navbar-default .navbar-nav > li.active > a:hover,
    #header .navbar-default .navbar-nav > li.active > a:focus {
        background-color: rgba(0, 0, 0, 0);
        color: #FFFFFF;
    }

    #header .navbar-default .navbar-nav > .open .dropdown-menu > li > a:hover, #header .navbar-default .navbar-nav > .open .dropdown-menu > li > a:focus {
        background-color: rgba(0, 0, 0, 0.1);
    }

    /*------------------------------------*\
    $FOOTER
    \*------------------------------------*/

    #footer #footer-main [class^="fa-icon-"], #footer #footer-main [class*=" fa-icon-"] {
        color: <?php echo $options['color-scheme'] ?>;
    }

    p.dropcap-contrast span.first-letter {
        background-color: <?php echo $options['color-scheme'] ?>;
    }

    p.dropcap span.first-letter {
        color: <?php echo $options['color-scheme'] ?>;
    }

    blockquote {
        border-left-color: <?php echo $options['color-scheme'] ?>;
    }

    blockquote.pull-right {
        border-right-color: <?php echo $options['color-scheme'] ?>;
    }

    blockquote small {
        color: <?php echo $options['color-scheme'] ?>;
    }

    .page-header.page-header-with-icon > [class^="fa-icon-"], .page-header.page-header-with-icon > [class*=" fa-icon-"], .page-header.page-header-with-icon > [class^="fa-"], .page-header.page-header-with-icon > [class*=" fa-icon-"] {
        color: <?php echo $options['color-scheme'] ?>;
    }

    /*------------------------------------*\
    $QUOTES
    \*------------------------------------*/
    .no-touch .quotes .quote:hover .author-wrapper .author {
        border-top-color: <?php echo $options['color-scheme'] ?>;
    }

    .no-touch .quotes .quote:hover .author-wrapper .author:before {
        border-top-color: <?php echo $options['color-scheme'] ?>;
    }

    /*------------------------------------*\
    $PRICING TABLES
    \*------------------------------------*/
    .panel.pricing-table .panel-body .price {
        color: <?php echo $options['color-scheme'] ?>;
    }

    .panel.pricing-table.pricing-table-solid.panel-contrast .panel-heading {
        background-color: <?php echo $options['color-scheme'] ?>;
    }

    /*------------------------------------*\
    $PANELS
    \*------------------------------------*/
    .panel.panel-image .panel-icon .icon {
        background-color: <?php echo $options['color-scheme'] ?>;
    }

    .panel.panel-big .panel-icon .icon {
        color: <?php echo $options['color-scheme'] ?>;
    }

    .panel.panel-circle-big-contrast .panel-icon .icon {
        background-color: <?php echo $options['color-scheme'] ?>;
    }

    .panel.panel-circle-contrast .panel-icon .icon {
        background-color: <?php echo $options['color-scheme'] ?>;
    }

    .panel.panel-circle-contrast.panel-contrast .panel-icon [class^="fa-icon-"], .panel.panel-circle-contrast.panel-contrast > [class*=" fa-icon-"] {
        background-color: <?php echo $options['color-scheme'] ?>;
    }

    .panel-contrast {
        border-color: <?php echo $options['color-scheme'] ?>;
    }

    .panel-contrast > .panel-heading {
        background-color: <?php echo $options['color-scheme'] ?>;
        border-color: <?php echo $options['color-scheme'] ?>;
    }

    .panel-contrast > .panel-heading + .panel-collapse .panel-body {
        border-top-color: <?php echo $options['color-scheme'] ?>;
    }

    .panel-contrast > .panel-footer {
        background-color: <?php echo $options['color-scheme'] ?>;
        border-color: <?php echo $options['color-scheme'] ?>;
    }

    .panel-contrast > .panel-footer + .panel-collapse .panel-body {
        border-bottom-color: <?php echo $options['color-scheme'] ?>;
    }

    /*------------------------------------*\
    $BOXES
    \*------------------------------------*/
    .text-boxes .text-box .toolbar .btn-link [class^="fa-icon-"], .text-boxes .text-box .toolbar .btn-link > [class*=" fa-icon-"] {
        color: <?php echo $options['color-scheme'] ?>;
    }

    .portfolio-boxes .portfolio-box a:hover .title {
        color: <?php echo $options['color-scheme'] ?>;
    }

    .author-box .title {
        color: <?php echo $options['color-scheme'] ?>;
    }

    /*------------------------------------*\
    $CAROUSELS
    \*------------------------------------*/
    .carousel-indicators .active {
        background-color: <?php echo $options['color-scheme'] ?>;
    }

    .carousel-indicators .active:hover {
        background-color: <?php echo $options['color-scheme'] ?>;
    }

    .carousel-contrast .flex-control-paging a:hover, .carousel-contrast .flex-control-paging a.flex-active {
        background-color: <?php echo $options['color-scheme'] ?>;
    }

    .carousel-image .flex-control-paging a:hover, .carousel-image .flex-control-paging a.flex-active {
        background-color: <?php echo $options['color-scheme'] ?>;
    }

    /*------------------------------------*\
    $TIMELINE
    \*------------------------------------*/
    ol.timeline li:hover .image {
        border-color: <?php echo $options['color-scheme'] ?>;
    }

    ol.timeline li:hover .time {
        color: <?php echo $options['color-scheme'] ?>;
    }

    ol.timeline li:nth-child(odd):hover .image:before {
        border-left-color: <?php echo $options['color-scheme'] ?>;
    }

    ol.timeline li.nth-child-odd:hover .image:before {
        border-left-color: <?php echo $options['color-scheme'] ?>;
    }

    ol.timeline li:nth-child(even):hover .image:before {
        border-right-color: <?php echo $options['color-scheme'] ?>;
    }

    ol.timeline li.nth-child-even:hover .image:before {
        border-right-color: <?php echo $options['color-scheme'] ?>;
    }

    /*------------------------------------*\
    $COMMENTS
    \*------------------------------------*/
    .media .content.staff {
        border-color: <?php echo $options['color-scheme'] ?>;
    }

    .media .content.staff:before {
        border-right-color: <?php echo $options['color-scheme'] ?>;
    }

    /*------------------------------------*\
    $BUTTONS
    \*------------------------------------*/
    p.form-submit {
        background-color: <?php echo $options['color-scheme'] ?>;
    }

    .button-wrapper {
        background-color: <?php echo $options['color-scheme'] ?>;
        border-color: <?php echo $options['color-scheme'] ?>;
    }

    .countdown-page .button-wrapper .btn:hover, .countdown-page .button-wrapper .btn:focus {
        color: #fff;
    }

    .btn {
        background-color: rgba(0, 0, 0, 0);
        color: #fff;
    }

    .button-wrapper .btn:hover, .button-wrapper .btn:focus {
        background-color: rgba(0, 0, 0, 0.1);
    }

    .button-wrapper.btn-bordered .btn:hover, .button-wrapper.btn-bordered .btn:focus {
        background-color: rgba(0, 0, 0, 0);
    }

    .btn.btn-bordered {
        border-color: <?php echo $options['color-scheme'] ?>;
        color: <?php echo $options['color-scheme'] ?>;
        background-color: transparent;
    }

    .btn.btn-bordered:hover, .btn.btn-bordered:focus {
        background-color: <?php echo $transparent_color_scheme ?>;
    }

    /*------------------------------------*\
    $ACCORDIONS
    \*------------------------------------*/
    .accordion .accordion-toggle {
        background-color: <?php echo $options['color-scheme'] ?>;
    }

    .accordion.accordion-bordered .accordion-toggle {
        color: <?php echo $options['color-scheme'] ?>;
    }

    .accordion.accordion-bordered .accordion-toggle.collapsed:hover {
        color: <?php echo $options['color-scheme'] ?>;
    }

    /*------------------------------------*\
    $TABS
    \*------------------------------------*/
    .tabbable .nav-tabs > li > a:hover {
        color: <?php echo $options['color-scheme'] ?>;
    }

    .tabbable .nav-tabs > li.active > a, .tabbable .nav-tabs > li.active > a:hover, .tabbable .nav-tabs > li.active > a:focus {
        border-top: 4px solid <?php echo $options['color-scheme'] ?>;
    }

    .tabbable.tabs-below .nav-tabs > li.active > a, .tabbable.tabs-below .nav-tabs > li.active > a:hover, .tabbable.tabs-below .nav-tabs > li.active > a:focus {
        border-bottom: 4px solid <?php echo $options['color-scheme'] ?>;
    }

    .tabbable.tabs-left .nav-tabs > li.active > a, .tabbable.tabs-left .nav-tabs > li.active > a:hover, .tabbable.tabs-left .nav-tabs > li.active > a:focus {
        border-left: 4px solid <?php echo $options['color-scheme'] ?>;
    }

    .tabbable.tabs-right .nav-tabs > li.active > a, .tabbable.tabs-right .nav-tabs > li.active > a:hover, .tabbable.tabs-right .nav-tabs > li.active > a:focus {
        border-right: 4px solid <?php echo $options['color-scheme'] ?>;
    }

    /*------------------------------------*\
    $PAGINATION
    \*------------------------------------*/
    .pagination > .active > a, .pagination > .active > a:hover, .pagination > .active > a:focus,
    .pagination > .active > span,
    .pagination > .active > span:hover,
    .pagination > .active > span:focus {
        border-color: <?php echo $options['color-scheme'] ?>;
    }

    /*------------------------------------*\
    $NAVS
    \*------------------------------------*/
    .list-group .list-group-item [class^="fa-icon-"], .list-group .list-group-item [class*=" fa-icon-"] {
        color: <?php echo $options['color-scheme'] ?>;
    }

    .list-group .list-group-item.active, .list-group .list-group-item.active:hover, .list-group .list-group-item.active:focus {
        background-color: <?php echo $options['color-scheme'] ?>;
        border-color: <?php echo $options['color-scheme'] ?>;
    }

    .nav-pills > li.active > a {
        background-color: <?php echo $options['color-scheme'] ?>;
    }

    .nav-pills > li.active > a:hover, .nav-pills > li.active > a:focus {
        background-color: <?php echo $options['color-scheme'] ?>;
    }

    /*------------------------------------*\
    $ERROR PAGE
    \*------------------------------------*/
    body.error-page .code [class^="fa-icon-"], body.error-page .code [class*=" fa-icon-"] {
        color: <?php echo $options['color-scheme'] ?>;
    }

    /*------------------------------------*\
    $COUNTDOWN PAGE
    \*------------------------------------*/
    body.countdown-page .countdown .countdown-item {
        background-color: <?php echo $options['color-scheme'] ?>;
    }

    @media (max-width: 767px) {
    #header .navbar-default .navbar-nav .open .dropdown-menu > .active > a, #header .navbar-default .navbar-nav .open .dropdown-menu > .active > a:hover, #header .navbar-default .navbar-nav .open .dropdown-menu > .active > a:focus {
        background-color: <?php echo $options['color-scheme'] ?>;
    } }

    @media (min-width: 768px) {
        #header .navbar-default .navbar-nav > li .dropdown-menu {
            margin-top: 15px;
            background-color: <?php echo $options['color-scheme'] ?>;
        }

        #header .navbar-default .navbar-nav > li > a:hover span, #header .navbar-default .navbar-nav > li > a:focus span {
            border-bottom-color: <?php echo $options['color-scheme'] ?>;
        }

        #header .navbar-default .navbar-nav > li.open > a span, #header .navbar-default .navbar-nav > li.open > a:hover span, #header .navbar-default .navbar-nav > li.open > a:focus span, #header .navbar-default .navbar-nav > li.active > a span, #header .navbar-default .navbar-nav > li.active > a:hover span, #header .navbar-default .navbar-nav > li.active > a:focus span {
            border-bottom-color: <?php echo $options['color-scheme'] ?>;
        }
    }

</style>