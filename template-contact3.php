<?php

// Template Name: Contact 3

global $NHP_Options;
$options = $NHP_Options->options;

?>

<?php get_template_part('templates/header') ?>

<div id='main' class="contact-page" role='main'>

    <?php get_template_part('templates/top_section') ?>

    <div id='main-content'>
        <div class='container'>
            <div class='row'>
                <div class='col-sm-12'>
                    <?php
                    if ( have_posts() ) {
                        while ( have_posts() ) {
                            the_post();

                            the_content();
                        }
                    }
                    ?>
                </div>
            </div>

        <?php if (isset($options['show-contact-map'])) : ?>

            <div class='row'>
                <div class='col-sm-12'>
                    <div class='page-header page-header-with-icon'>
                        <i class='fa-icon-map-marker'></i>
                        <h2>
                            <?php _e('Our location', THEME_TEXT_DOMAIN) ?>
                        </h2>
                    </div>
                    <div class='map-container'>
                        <div id='map-canvas'></div>
                        <script>
                            var initializeMap;

                            initializeMap = function() {
                                var iw1, latlng, map, marker, options;

                                latlng = new google.maps.LatLng(<?php echo $options['contact-map'] ?>);
                                options = {
                                    scrollwheel: false,
                                    zoom: 16,
                                    center: latlng,
                                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                                    mapTypeControl: true
                                };

                                map = new google.maps.Map(document.getElementById("map-canvas"), options);

                                marker = new google.maps.Marker({
                                    position: latlng,
                                    map: map
                                });

                                iw1 = new google.maps.InfoWindow({
                                    content: "Here we are!"
                                });

                                return google.maps.event.addListener(marker, "click", function(e) {
                                    return iw1.open(map, this);
                                });
                            };

                            google.maps.event.addDomListener(window, 'load', initializeMap);
                        </script>
                    </div>
                </div>
            </div>

        <?php endif; ?>

            <?php get_template_part('templates/contact_info') ?>

            <div class='row'>
                <div class='col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2'>

                    <?php get_template_part('templates/contact_form') ?>

                </div>
            </div>
        </div>
    </div>

<?php get_template_part('templates/footer') ?>