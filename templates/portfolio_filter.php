<div class='row' id='portfolio-filter'>

    <?php

    $taxonomies = 'portfolio_category';

    $portfolio_categories_object = get_terms($taxonomies, '');


    $portfolio_categories = array();

    foreach ($portfolio_categories_object as $portfolio_category) {
        $portfolio_categories[] = $portfolio_category->name;
    }

    ?>

    <div class='col-sm-12'>
        <ul class='nav nav-pills'>
            <li class='active'><a data-filter="*" href="#"><?php _e('All', THEME_TEXT_DOMAIN) ?></a></li>
            <?php

            foreach ( $portfolio_categories as $portfolio_category ) :
                $portfolio_category_slug = str_replace(' ', '_', $portfolio_category);

            ?>

                <li><a data-filter=".<?php echo $portfolio_category_slug ?>" href="#"><?php echo $portfolio_category ?></a></li>

            <?php endforeach; ?>
        </ul>
    </div>

</div>