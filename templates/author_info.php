<?php

global $NHP_Options;
$options = $NHP_Options->options;

?>

<?php if ($options['author-bio'] == 1) : ?>

<div class='row'>
    <div class='col-sm-12'>
        <div class='author-box'>
            <div class='row'>
                <div class='col-sm-3 col-lg-2'>
                    <?php echo get_avatar(get_the_author_meta('ID'), 120, '', ''); ?>
                </div>
                <div class='col-sm-9 col-lg-10'>
                    <h3 class='title'><?php _e('About author', THEME_TEXT_DOMAIN) ?></h3>
                    <p><?php the_author_meta('description') ?></p>
                </div>
            </div>
        </div>
    </div>
</div>

<?php endif; ?>