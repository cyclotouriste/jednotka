
<nav class='sidebar'>
    <div class="button-wrapper sidebar-toggle">
        <button class='btn btn-block btn-contrast' data-target='.sidebar-collapse' data-toggle='collapse' type='button'>
            <span class='sr-only'><?php _e('Toggle navigation', THEME_TEXT_DOMAIN) ?></span>
            <span class='icon-bar'></span>
            <span class='icon-bar'></span>
            <span class='icon-bar'></span>
        </button>
    </div>
    <div class='sidebar-collapse collapse'>
        <?php

        dynamic_sidebar( 'sidebar-widgets' );

        ?>
    </div>
</nav>