<?php

global $NHP_Options;
$options = $NHP_Options->options;

?>

    <div class='fade' id='scroll-to-top'>
        <i class='fa-icon-chevron-up'></i>
    </div>
</div> <!--<div id='main' role='main'>-->

<footer id='footer'>
    <div id='footer-main'>
        <div class='container'>
            <div class='row'>

                <?php dynamic_sidebar( 'footer-widgets' ); ?>

            </div>
        </div>
    </div>
    <div id='footer-copyright'>
        <div class='container'>
            <div class='row'>
                <div class='col-lg-12 clearfix'>
                    <p class='copyright'>
                        &copy;
                        <?php if (isset($options['copyright'])) { echo $options['copyright']; } ?>
                    </p>
                    <div class='links'>
                        <?php if ( !empty($options['facebook-link']) ) { ?>
                            <a href="<?php echo $options['facebook-link'] ?>" class="btn btn-circle btn-medium-light btn-sm"><i class='fa-icon-facebook text-dark'></i></a>
                        <?php } // endif ?>

                        <?php if ( !empty($options['twitter-link']) ) { ?>
                            <a href="<?php echo $options['twitter-link'] ?>" class="btn btn-circle btn-medium-light btn-sm"><i class='fa-icon-twitter text-dark'></i></a>
                        <?php } // endif ?>

                        <?php if ( !empty($options['pinterest-link']) ) { ?>
                            <a href="<?php echo $options['pinterest-link'] ?>" class="btn btn-circle btn-medium-light btn-sm"><i class='fa-icon-dribbble text-dark'></i></a>
                        <?php } // endif ?>

                        <?php if ( !empty($options['google-plus-link']) ) { ?>
                            <a href="<?php echo $options['google-plus-link'] ?>" class="btn btn-circle btn-medium-light btn-sm"><i class="fa-icon-pinterest text-dark"></i></a>
                        <?php } // endif ?>

                        <?php if ( !empty($options['tumblr-link']) ) { ?>
                            <a href="<?php echo $options['tumblr-link'] ?>" class="btn btn-circle btn-medium-light btn-sm"><i class="fa-icon-google-plus text-dark"></i></a>
                        <?php } // endif ?>

                        <?php if ( !empty($options['dribble-link']) ) { ?>
                            <a href="<?php echo $options['dribble-link'] ?>" class="btn btn-circle btn-medium-light btn-sm"><i class="fa-icon-tumblr text-dark"></i></a>
                        <?php } // endif ?>

                        <?php if ( !empty($options['instagram-link']) ) { ?>
                            <a href="<?php echo $options['instagram-link'] ?>" class="btn btn-circle btn-medium-light btn-sm"><i class="fa-icon-instagram text-dark"></i></a>
                        <?php } // endif ?>

                        <?php if ( !empty($options['rss-link']) ) { ?>
                            <a href="<?php echo $options['rss-link'] ?>" class="btn btn-circle btn-medium-light btn-sm"><i class="fa-icon-rss text-dark"></i></a>
                        <?php } // endif ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

<?php do_action('jed_below_footer') ?>

</div>

<?php wp_footer(); ?>
</body>
</html>