<?php

global $NHP_Options;
$options = $NHP_Options->options;

function holo_get_breadcrumb_navigation($options) {
    $delimiter = '';
    $home = '<i class="fa-icon-home"></i>';
    $before = '<li>';
    $after = '</li>';
    $before_active = '<li class="active">';
    $after_acive = '';

    echo '<ol class="breadcrumb">';

    global $post;
    $homeLink = home_url();
    echo $before . '<a href="' . $homeLink . '">' . $home . '</a> ' . $delimiter . ' ' . $after;
    if ( is_category() ) { ?>

   <?php } elseif ( is_day() ) {
        echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
        echo '<a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a> ' . $delimiter . ' ';
        echo $before . '' . get_the_time('d') . '"' . $after;
    } elseif ( is_month() ) {
        echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
        echo $before . '' . get_the_time('F') . '"' . $after;
    } elseif ( is_year() ) {
        echo $before . '' . get_the_time('Y') . '"' . $after;
    } elseif ( is_single() && !is_attachment() ) {
        if ( get_post_type() != 'post' ) {
            $post_type = get_post_type_object(get_post_type());
            $slug = $post_type->rewrite;

            if ($slug == 'portfolio') {

                echo $before . '<a href="' . $options['portfolio-link'] . '/">' . $post_type->labels->singular_name . '</a>' . $delimiter . ' ' . $after;
                echo $before_active . get_the_title() . $after;
            }
            else {
                echo $before . '<a href="' . $homeLink . '/' . $slug['slug'] . '/">' . $post_type->labels->singular_name . '</a>' . $delimiter . ' ' . $after;
                echo $before_active . get_the_title() . $after;
            }
        } else {
            $cat = get_the_category(); $cat = $cat[0];
            echo $before . ' ' . get_category_parents($cat, TRUE, ' ' . $delimiter . ' ') . ' ' . $after;
            echo $before_active . '' . get_the_title() . '' . $after;
        }
    } elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
        $post_type = get_post_type_object(get_post_type());
        echo $before . $post_type . $after;
    } elseif ( is_attachment() ) {
        $parent_id  = $post->post_parent;
        $breadcrumbs = array();
        while ($parent_id) {
            $page = get_page($parent_id);
            $breadcrumbs[] = '<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a>';
            $parent_id    = $page->post_parent;
        }
        $breadcrumbs = array_reverse($breadcrumbs);
        foreach ($breadcrumbs as $crumb) echo ' ' . $crumb . ' ' . $delimiter . ' ';
        echo $before_active . '' . get_the_title() . '"' . $after;
    } elseif ( is_page() && !$post->post_parent ) {
        echo $before_active . get_the_title() . $after;
    } elseif ( is_page() && $post->post_parent ) {
        $parent_id  = $post->post_parent;
        $breadcrumbs = array();
        while ($parent_id) {
            $page = get_page($parent_id);
//            $breadcrumbs[] = '<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a>';
            $parent_id    = $page->post_parent;
        }
        $breadcrumbs = array_reverse($breadcrumbs);
        foreach ($breadcrumbs as $crumb) echo ' ' . $crumb . ' ' . $delimiter . ' ';
        echo $before_active . '' . get_the_title() . '' . $after;
    } elseif ( is_search() ) {
        echo $before . '' . get_search_query() . '' . $after;
    } elseif ( is_tag() ) {
        echo $before . '' . single_tag_title('', false) . '' . $after;
    } elseif ( is_author() ) {
        global $author;
        $userdata = get_userdata($author);
        echo $before . '' . $userdata->display_name . '' . $after;
    } elseif ( is_404() ) {
        echo $before . '' . '404 not Found' . '' . $after;
    }
//    if ( get_query_var('paged') ) {
//        if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';
//        echo ('Page') . ' ' . get_query_var('paged');
//        if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
//    }
    echo '</ol><!-- / Bloglow breadcrumb navigation without a plugin -->';
}

$page_id = get_queried_object_id();

$page_description = get_post_meta($page_id, 'page_description', true);

?>

<div id='main-content-header'>
    <div class='container'>
        <div class='row'>
            <div class='col-sm-12'>
                <h1 class='title'>
	                <?php echo get_the_title($page_id); ?>
                    <small><?php echo $page_description ?></small>
                </h1>

                <?php

                if ( is_page_template('template-blog-large.php') || is_page_template('template-blog-small.php') ||
                    is_singular('portfolio') || is_singular('post') || is_page_template('template-contact1.php')||
                    is_page_template('template-contact2.php') || is_page_template('template-contact3.php')) {

                    holo_get_breadcrumb_navigation($options);
                }

                ?>

<?php if(is_category('aanbod')):?>

<h1 class="title">
Mijn aanbod
</h1>
<?php endif;?>

                <?php if ( is_singular('portfolio') ) : ?>

                    <?php

                    $next_link = get_permalink(get_adjacent_post(false,'',false));
                    $prev_link = get_permalink(get_adjacent_post(false,'',true));
                    ?>

                <div class='directional'>

                    <a class='has-tooltip left' data-placement='right' data-title='Previous project' href='<?php echo $next_link ?>'>
                        <i class='fa-icon-chevron-left'></i>
                    </a>
                    <a class='has-tooltip right' data-placement='left' data-title='Next project' href='<?php echo $prev_link ?>'>
                        <i class='fa-icon-chevron-right'></i>
                    </a>
                </div>

                <?php endif; ?>

            </div>
        </div>
    </div>
</div>