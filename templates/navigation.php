<?php
    wp_nav_menu(array(
        'theme_location' => 'jednotka_header_navigation',
        'container' => 'div',
        'container_class' => 'collapse navbar-collapse navbar-header-collapse',
        'menu_class' => 'nav navbar-nav navbar-right',
        'fallback_cb' => 'mytheme_nav_fallback',
        'walker' => new Main_Nav_Walker(),
        'echo' => true
    ));

function mytheme_nav_fallback() {
    wp_page_menu( 'show_home=Start&menu_class=pagemenu' );
}
