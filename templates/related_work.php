<?php

global $NHP_Options;
$options = $NHP_Options->options;

if ( isset($options['show-portfolio-related-work']) && $options['show-portfolio-related-work'] ) :
?>

<?php
$portfolio_category = $GLOBALS['portfolio_category'];

$related_items = array();

    $args = array(
        'post_type' => 'portfolio',
        'posts_per_page' => 6,
        'portfolio_category' => $portfolio_category->name,
        'post__not_in' => array($post->ID)
    );

    global $post;

    $portfolio = new WP_Query($args);

    while ($portfolio->have_posts()) {
        $portfolio->the_post();

        $portfolio_image_src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'portfolio-4columns');
        $full_portfolio_image_src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full');

        $portfolio_category = wp_get_post_terms($post->ID, 'portfolio_category');

        $related_item = array();

        $related_item['permalink'] = get_permalink($post->ID);
        $related_item['image'] = $portfolio_image_src[0];
        $related_item['full_image'] = $full_portfolio_image_src[0];
        $related_item['title'] = get_the_title();

        $related_items[] = $related_item;
    }

    $first_three = array();
    $second_three = array();

    $index = 0;
    foreach($related_items as $related_item) {

        if ($index < 3) {
            $first_three[] = $related_item;
        }
        else {
            $second_three[] = $related_item;
        }

        $index++;
    }

//    print_r($first_three);
?>

<div class='row'>
    <div class='col-sm-12'>
        <div class='page-header page-header-with-icon'>
            <i class='fa-icon-leaf'></i>
            <h2>
                <?php _e('Related work', THEME_TEXT_DOMAIN) ?>
            </h2>
        </div>

        <div class='row portfolio-boxes'>
            <div class='carousel carousel-default slide carousel-auto' id='carousel-related-work'>
                <div class='carousel-inner'>
                    <div class='item active'>

                        <?php foreach($first_three as $item) : ?>

                        <div class='col-sm-4 portfolio-box'>
                            <a class="thumbnail-hover" href='<?php echo $item['full_image'] ?>'>
                                <div class='image-link'>
                                    <i class='fa-icon-search'></i>
                                    <img class="img-responsive img-rounded center-block" width="262" height="171" src="<?php echo $item['image'] ?>" />
                                </div>
                            </a>

                            <a href='<?php echo $item['permalink'] ?>'><h3 class='title'><?php echo $item['title'] ?></h3></a>
                            <p class='category'><?php echo $portfolio_category[0]->name ?></p>
                        </div>

                        <?php endforeach; ?>

                    </div>

                    <?php if (!empty($second_three)) : ?>

                    <div class='item'>

                        <?php foreach($second_three as $item) : ?>

                            <div class='col-sm-4 portfolio-box'>
                                <a class="thumbnail-hover" href='<?php echo $item['full_image'] ?>'>
                                    <div class='image-link'>
                                        <i class='fa-icon-search'></i>
                                        <img class="img-responsive img-rounded center-block" width="262" height="171" src="<?php echo $item['image'] ?>" />
                                    </div>
                                </a>

                                <a href='<?php echo $item['permalink'] ?>'><h3 class='title'><?php echo $item['title'] ?></h3></a>
                                <p class='category'><?php echo $portfolio_category[0]->name ?></p>
                            </div>

                        <?php endforeach; ?>

                    </div>

                    <?php endif; ?>
                </div>
                <?php if (!empty($second_three)) : ?>
                <ol class='carousel-indicators'>
                    <li class='active' data-slide-to='0' data-target='#carousel-related-work'></li>
                    <li data-slide-to='1' data-target='#carousel-related-work'></li>
                </ol>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<?php
endif;