<?php

global $NHP_Options;
$options = $NHP_Options->options;

$msg = '';
$emailSent = false;

if(isset($_POST['submit'])) {

    if ( isset($_POST['contact']) ) {
        //Check to make sure that the name field is not empty
        if(trim($_POST['contact']['name']) === '') {
            $nameError = __("U bent vergeten uw naam in te vullen.", "site5framework");
            $hasError = true;
        } else {
            $name = trim($_POST['contact']['name']);
        }

        //Check to make sure sure that a valid email address is submitted
        if(trim($_POST['contact']['email']) === '')  {
            $emailError = __("U bent vergeten uw email adres in te vullen.", "site5framework");
            $hasError = true;
//    } else if (!preg_match("^[A-Z0-9._%-]+@[A-Z0-9._%-]+\.[A-Z]{2,4}$^", trim($_POST['email']))) {
//        $emailError = __("You entered an invalid email address.", "site5framework");
//        $hasError = true;
        } else {
            $email = trim($_POST['contact']['email']);
        }

        //Check to make sure message were entered
        if(trim($_POST['contact']['message']) === '') {
            $messageError = __("You forgot to enter your message.", "site5framework");
            $hasError = true;
        } else {
            if(function_exists('stripslashes')) {
                $message = stripslashes(trim($_POST['contact']['message']));
            } else {
                $message = trim($_POST['contact']['message']);
            }
        }
    }

    //If there is no error, send the email
    if(!isset($hasError)) {

        $msg .= "------------User Info------------ \r\n"; //Title
        $msg .= "User IP : ".$_SERVER["REMOTE_ADDR"]."\r\n"; //Sender's IP
        $msg .= "Browser Info : ".$_SERVER["HTTP_USER_AGENT"]."\r\n"; //User agent
        $msg .= "User Come From : ".$_SERVER["HTTP_REFERER"]; //Referrer

        $emailTo = $options['contact-email'];

        $headers = 'From: ' . get_bloginfo('name') . ' <' . $emailTo . '>' . "\r\n";
        $headers .= 'Reply-To: ' . $email . "\r\n";
        $headers .= 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=ISO-8859-1';

        $subject = 'Contact Form Submission From ' . $name;

        $body = '<p><b>Naam:</b> ' . $name . '</p><p><b>Email:</b> ' . $email . '</p><p><b>Message:</b> </p><p>' . nl2br($message) . '</p>';

        if( mail($emailTo, $subject, $body, $headers) ) { $emailSent = true; };
    }

}

?>

<div class='page-header page-header-with-icon'>
    <i class='fa-icon-envelope-alt'></i>
    <h2><?php _e('Contact formulier', THEME_TEXT_DOMAIN) ?></h2>
</div>

<?php

if ($emailSent) :

    ?>

    <div class='alert alert-success fade in'>
        <button aria-hidden='true' class='close' data-dismiss='alert' type='button'>×</button>
        <strong><?php _e('Email Sent Successfully!', THEME_TEXT_DOMAIN) ?></strong>
    </div>

<?php

endif;

?>

<form action='' class='form form-validation' method='post'>
    <div class='row'>
        <div class='col-sm-6'>
            <div class='form-group control-group'>
                <input class='form-control' data-rule-required='true' id='contact_name' name='contact[name]' placeholder="<?php _e('Naam', THEME_TEXT_DOMAIN) ?>" type='text'>
            </div>
        </div>
        <div class='col-sm-6'>
            <div class='form-group control-group'>
                <input class='form-control' data-rule-email='true' data-rule-required='true' id='contact_email' name='contact[email]' placeholder="<?php _e('Email', THEME_TEXT_DOMAIN) ?>" type='text'>
            </div>
        </div>
    </div>
    <div class='row'>
        <div class='col-sm-12'>
            <div class='form-group control-group'>
                <textarea class='form-control' data-rule-required='true' id='contact_message' name='contact[message]' placeholder="<?php _e('Uw bericht', THEME_TEXT_DOMAIN) ?>..."></textarea>
            </div>
        </div>
    </div>
    <div class='row'>
        <div class='col-sm-12'>
            <div class="button-wrapper contact-form-button-wrapper">
                <input class='btn btn-block' name="submit" type='submit' value="<?php _e('Verstuur uw bericht', THEME_TEXT_DOMAIN) ?>" />
            </div>
        </div>
    </div>
</form>