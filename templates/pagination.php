
<div class="text-center">
<?php

if (function_exists("wpthemess_paginate")) {
    wpthemess_paginate();
}
else {
    wp_link_pages();
}

?>
</div>