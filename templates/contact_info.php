<?php

global $NHP_Options;
$options = $NHP_Options->options;

?>

<div class='row'>
    <div class='col-sm-12'>
        <div class='page-header page-header-with-icon'>
            <i class='fa-icon-info-sign'></i>
            <h2>
                <?php _e('Contact informatie', THEME_TEXT_DOMAIN) ?>
            </h2>
        </div>
        <div class='row text-center'>

            <?php if ( isset($options['contact-address']) && !empty($options['contact-address']) ) : ?>

            <div class='col-sm-3'>
                <div class='icon-wrap icon-circle contrast-bg icon-md'>
                    <i class='fa-icon-map-marker text-white'></i>
                </div>
                <h3><?php _e('Woonplaats', THEME_TEXT_DOMAIN) ?></h3>
                <ul class='list-unstyled'>
                    <li><?php echo $options['contact-address'] ?></li>
                </ul>
            </div>

            <?php endif; ?>

            <?php if ( isset($options['contact-phone']) && !empty($options['contact-phone']) ) : ?>

            <div class='col-sm-3'>
                <div class='icon-wrap icon-circle contrast-bg icon-md'>
                    <i class='fa-icon-phone text-white'></i>
                </div>
                <h3><?php _e('Telefoonnummer', THEME_TEXT_DOMAIN) ?></h3>
                <ul class='list-unstyled'>
                    <li><?php echo $options['contact-phone'] ?></li>
                </ul>
            </div>

            <?php endif; ?>

            <?php if ( isset($options['contact-email']) && !empty($options['contact-email']) ) : ?>

            <div class='col-sm-3'>
                <div class='icon-wrap icon-circle contrast-bg icon-md'>
                    <i class='fa-icon-envelope-alt text-white'></i>
                </div>
                <h3><?php _e('EMail', THEME_TEXT_DOMAIN) ?></h3>
                <ul class='list-unstyled'>
                    <li><a href="mailto:<?php echo $options['contact-email'] ?>"><?php echo $options['contact-email'] ?></a></li>
                </ul>
            </div>

            <?php endif; ?>

            <?php if ( isset($options['contact-schedule']) && !empty($options['contact-schedule']) ) : ?>

            <div class="col-sm-3">
                <div class="icon-wrap icon-circle contrast-bg icon-md">
                    <i class="fa-icon-time text-white"></i>
                </div>
                <h3><?php _e('Openingstijden', THEME_TEXT_DOMAIN) ?></h3>
                <ul class="list-unstyled">
                    <?php echo $options['contact-schedule'] ?>
                </ul>
            </div>

            <?php endif; ?>
        </div>
    </div>
</div>