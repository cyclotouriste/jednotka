<?php

function custom_comments($comment, $args, $depth) {
    $GLOBALS['comment'] = $comment;

    $is_admin = false;

    if ( holo_check_user_role('administrator', $comment->user_id) ) {
        $is_admin = true;
    }

    ?>

    <div class='media' id="li-comment-<?php comment_ID() ?>">
        <div class='pull-left'>
            <?php echo get_avatar($comment, '60'); ?>
        </div>
        <div class='media-body'>
            <div class='content <?php echo $is_admin ? 'staff' : ''; ?>'>
                <div class='toolbar'>
                    <h4 class='title'><?php echo get_comment_author_link() ?></h4>

                    <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'reply_text' => '<i class=\'fa-icon-reply\'></i><span>' . __('Reply', THEME_TEXT_DOMAIN) . '</span>', 'max_depth' => $args['max_depth']))) ?>
                </div>
                <p><?php comment_text(); ?></p>

                <?php if ($comment->comment_approved == '0') : ?>
                    <em><?php _e('Your comment is awaiting moderation.', THEME_TEXT_DOMAIN) ?></em>
                <?php endif; ?>

                <p class='time'>
                    <a><?php echo get_comment_date() ?> <?php _e('at', THEME_TEXT_DOMAIN) ?> <?php echo get_comment_time() ?></a>
                </p>
            </div>
        </div>
    </div>

<?php

}

?>

<div class='row'>
    <?php

    if ( 'open' == $post->comment_status ) {

    ?>

    <div class='col-sm-12' id='comments'>
        <h3><?php comments_number( __('No comments', THEME_TEXT_DOMAIN), __('Comments', THEME_TEXT_DOMAIN) . ' (1)', __('Comments', THEME_TEXT_DOMAIN) . ' (%)') ?></h3>

        <?php wp_list_comments( array( 'type'=> 'comment', 'callback' => 'custom_comments' ) ); ?>
    </div>

    <?php

    }

    ?>
</div>

<div class='row'>
    <hr class='hr-invisible hr-half'>
    <div class='col-sm-12' id='add-new-comment'>
        <?php

        global $user_identity, $id, $post_id;

        if ( null === $post_id )
            $post_id = $id;
        else
            $id = $post_id;

        $req = get_option( 'require_name_email' );

        $commenter = wp_get_current_commenter();

        $aria_req = ( $req ? " aria-required='true'" : '' );

        $fields = array(
            'author' =>
                '<div class="row">
                    <div class="col-sm-4">
                        <div class="form-group control-group">
                            <input class="form-control"' . $aria_req . 'id="comment_name" name="author" placeholder="' . __('Name', THEME_TEXT_DOMAIN) . '*" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" />
                        </div>
                    </div>',
            'email' => '
                <div class="col-sm-4">
                    <div class="form-group control-group">
                        <input class="form-control"' . $aria_req . ' data-rule-email="true" data-rule-required="true" id="comment_email" name="email" placeholder="' . __('EMail', THEME_TEXT_DOMAIN)  . '*" type="text" value="' . esc_attr( $commenter['comment_author_email'] ) . '" />
                    </div>
                </div>',
            'url' => '
                <div class="col-sm-4">
                    <div class="form-group control-group">
                        <input class="form-control"' . $aria_req . ' id="comment_website" name="url" placeholder="' . __('Website', THEME_TEXT_DOMAIN) . '" type="text" value="' . esc_attr( $commenter['comment_author_url'] ) . '" />
                    </div>
                </div>
                </div>'
        );

        $comment_field = '
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group control-group">
                        <textarea class="form-control"' . $aria_req . ' data-rule-required="true" id="comment_message" name="comment" placeholder="' . __('Comment', THEME_TEXT_DOMAIN) . '*" rows="5"></textarea>
                    </div>
                </div>
            </div>';

        $args = array(
            'fields' => apply_filters( 'comment_form_default_fields', $fields ),
            'comment_field' => $comment_field,
            'logged_in_as' => '
                <p id="login">
                    <span class="loggedin">' . sprintf( __('Logged in as %s', THEME_TEXT_DOMAIN ), sprintf( ' <a href="%1$s" title="%2$s">%3$s</a>', admin_url( 'profile.php' ), sprintf( esc_attr__('Logged in as %s', THEME_TEXT_DOMAIN), $user_identity ) , $user_identity ) ) . '. </span>
                    <span class="logout">' . sprintf('<a href="%s" title="%s">%s</a>' , esc_attr( wp_logout_url( apply_filters( 'the_permalink', get_permalink( $post_id ) ) ) ), esc_attr__('Log out?', THEME_TEXT_DOMAIN ) , __('Log out?', THEME_TEXT_DOMAIN ) ) . '</span>
                </p>',
            'id-submit' => '',
            'comment_notes_after' => '',
            'title_reply'          => __('Leave a reply', THEME_TEXT_DOMAIN),
            'title_reply_to'       => __('Post a reply to %s', THEME_TEXT_DOMAIN),
            'cancel_reply_link'    => __('Cancel reply', THEME_TEXT_DOMAIN),
            'label_submit'         => __('Post Comment', THEME_TEXT_DOMAIN),
        );

        comment_form($args);

        ?>

    </div>
</div>