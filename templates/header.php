<?php

global $NHP_Options;
$options = $NHP_Options->options;

$logo_width = 117;
$logo_height = 39;

if ( !empty($options['logo-width']) ) {
    $logo_width = (int)$options['logo-width'];
}

if (!empty($options['logo-height']) ) {
    $logo_height = (int)$options['logo-height'];
}

?>

<!DOCTYPE html>

<!--[if lt IE 9]> <html <?php language_attributes(); ?> class="lt-ie9"> <![endif]-->
<!--[if !IE] -->
<html <?php language_attributes(); ?>>
<!-- <![endif] -->
<head>

    <meta content='text/html; charset=utf-8' http-equiv='Content-Type'>
    <meta content='width=device-width, initial-scale=1.0' name='viewport'>

    <?php if ( $options['activate-meta'] ) : ?>

    <meta name="description" content='<?php if ( $options['meta-description'] ) { echo $options['meta-description']; } ?>' />
    <meta name="keywords" content='<?php if ( $options['meta-keywords'] ) { echo $options['meta-keywords']; } ?>' />
    <meta name="author" content='<?php if ( $options['meta-author'] ) { echo $options['meta-author']; } ?>' />

        <?php if ( $options['robots'] ) : ?>

            <meta name="robots" content="<?php echo $options['general-bot']  ?>" />
            <meta name="googlebot" content="<?php echo $options['google-bot']  ?>" />

        <?php endif; ?>

    <?php endif; ?>

    <!--[if IE]> <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'> <![endif]-->

    <link href='<?php echo $options['favicon'] ?>' rel='shortcut icon' type='image/x-icon'>

    <title><?php echo wp_title( '', true, 'right' ) ?></title>

    <?php echo $options['analytics'] ?>
    <?php wp_head(); ?>
</head>

<?php

$body_class = 'homepage' . ' ' . ( $options['layout-style'] == 'boxed' ? 'boxed' : '' );
$body_background = (isset($options['background-pattern']) && !empty($options['background-pattern']) ? 'style="background-image: url(' . $options['background-pattern'] . ');"' : '');

?>

<body <?php body_class($body_class); ?> <?php echo $body_background; ?>>

<?php get_template_part('styles/custom_styles') ?>

<style type="text/css">
    <?php echo $options['custom-css'] ?>
</style>

<?php do_action('jed_before_wrapper') ?>

<div id='wrapper'
    <?php
    if ( is_page_template('template-home.php') ||
        is_page_template('template-home-slider1.php') ||
        is_page_template('template-home-slider3.php') ) {

        echo ' class="homepage-wrapper"';

    }

    if ( is_page_template('template-home-slider2.php') ) {

        echo ' style="background-image: none !important;"';

    }
    ?>
>
    <header id='header'>
        <div class='container'>
            <nav class='navbar navbar-default' id='nav' role='navigation'>
                <div class='navbar-header'>
                    <button class='navbar-toggle' data-target='.navbar-header-collapse' data-toggle='collapse' type='button'>
                        <span class='sr-only'><?php _e('Toggle navigation', THEME_TEXT_DOMAIN) ?></span>
                        <span class='icon-bar'></span>
                        <span class='icon-bar'></span>
                        <span class='icon-bar'></span>
                    </button>

                    <a class='navbar-brand <?php if (isset($options['logo-animate']) && $options['logo-animate']) { echo 'logo-animate'; } ?>' href="<?php echo get_home_url() ?>">
                        <?php if ( !empty($options['logo']) ) { ?>

                            <img src="<?php echo $options['logo'] ?>" width="<?php echo $logo_width  ?>" height="<?php echo $logo_height ?>" />


                        <?php } else {

                            echo $options['logo-text'];

                        } ?>
                    </a>
                </div>

                <?php get_template_part('templates/navigation') ?>

            </nav>
        </div>
    </header>